/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.lib.markup;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 * TODO [javadoc]: type HtmlParse
 *
 * @author zisch
 */
public class HtmlParse {

  private static final String FACTORY_CLASS = "org.ccil.cowan.tagsoup.jaxp.SAXFactoryImpl";

  /**
   * TODO [javadoc]: method parseDom
   *
   * @param source TODO [javadoc]
   *
   * @return TODO [javadoc]
   *
   * @throws IOException TODO [javadoc]
   */
  public static Document parseDom (final File source) throws IOException {
    return parseDom(new FileSource(source));
  }

  /**
   * TODO [javadoc]: method parseDom
   *
   * @param source TODO [javadoc]
   *
   * @return TODO [javadoc]
   *
   * @throws IOException TODO [javadoc]
   */
  public static Document parseDom (final InputSource source) throws IOException {
    return parseDom(new InputSourceSource(source));
  }

  /**
   * TODO [javadoc]: method parseDom
   *
   * @param source TODO [javadoc]
   *
   * @return TODO [javadoc]
   *
   * @throws IOException TODO [javadoc]
   */
  public static Document parseDom (final URI source) throws IOException {
    return parseDom(source.toString());
  }

  /**
   * TODO [javadoc]: method parseDom
   *
   * @param source TODO [javadoc]
   *
   * @return TODO [javadoc]
   *
   * @throws IOException TODO [javadoc]
   */
  public static Document parseDom (final URL source) throws IOException {
    return parseDom(source.toString());
  }

  /**
   * TODO [javadoc]: method parseDom
   *
   * @param source TODO [javadoc]
   *
   * @return TODO [javadoc]
   *
   * @throws IOException TODO [javadoc]
   */
  public static Document parseDom (final String source) throws IOException {
    return parseDom(new StringSource(source));
  }

  /**
   * TODO [javadoc]: method parseDom
   *
   * @param source TODO [javadoc]
   *
   * @return TODO [javadoc]
   *
   * @throws IOException TODO [javadoc]
   */
  public static Document parseDom (final InputStream source) throws IOException {
    return parseDom(source, null);
  }

  /**
   * TODO [javadoc]: method parseDom
   *
   * @param source TODO [javadoc]
   * @param systemId TODO [javadoc]
   *
   * @return TODO [javadoc]
   *
   * @throws IOException TODO [javadoc]
   */
  public static Document parseDom (final InputStream source, final String systemId) throws IOException {
    return parseDom(new InputStreamSource(source, systemId));
  }

  private static Document parseDom (final ParseSource source) throws IOException {
    try {
      final SAXParserFactory spf = createSaxParserFactory();
      final Sax2DomHandler s2d = new Sax2DomHandler();
      final SAXParser sp = spf.newSAXParser();
      sp.getXMLReader().setProperty("http://xml.org/sax/properties/lexical-handler", s2d);
      source.parse(sp, s2d);
      final Document doc = s2d.getDocument();
      return doc;
    } catch (final SAXException | ParserConfigurationException exc) {
      // FIXME: tell _what_ document!
      throw new IOException("Failed to parse HTML document: " + exc, exc);
    }
  }

  /**
   * TODO [javadoc]: method createSaxParserFactory
   *
   * @return TODO [javadoc]
   */
  public static SAXParserFactory createSaxParserFactory () {
    return createSaxParserFactory(null);
  }

  /**
   * TODO [javadoc]: method createSaxParserFactory
   *
   * @param classLoader TODO [javadoc]
   *
   * @return TODO [javadoc]
   */
  public static SAXParserFactory createSaxParserFactory (final ClassLoader classLoader) {
    return SAXParserFactory.newInstance(FACTORY_CLASS, classLoader);
  }

  // ---- Internal Helpers

  private static abstract class ParseSource {
    abstract void parse (SAXParser parser, Sax2DomHandler s2d) throws SAXException, IOException;
  }

  private static final class FileSource extends ParseSource {
    private final File mSource;

    private FileSource (final File source) {
      mSource = source;
    }

    @Override
    void parse (final SAXParser parser, final Sax2DomHandler s2d) throws SAXException, IOException {
      parser.parse(mSource, s2d);
    }
  }

  private static final class InputSourceSource extends ParseSource {
    private final InputSource mSource;

    private InputSourceSource (final InputSource source) {
      mSource = source;
    }

    @Override
    void parse (final SAXParser parser, final Sax2DomHandler s2d) throws SAXException, IOException {
      parser.parse(mSource, s2d);
    }
  }

  private static final class StringSource extends ParseSource {
    private final String mSource;

    private StringSource (final String source) {
      mSource = source;
    }

    @Override
    void parse (final SAXParser parser, final Sax2DomHandler s2d) throws SAXException, IOException {
      parser.parse(mSource, s2d);
    }
  }

  private static final class InputStreamSource extends ParseSource {
    private final InputStream mSource;

    private final String mSystemId;

    private InputStreamSource (final InputStream source, final String systemId) {
      mSource = source;
      mSystemId = systemId;
    }

    @Override
    void parse (final SAXParser parser, final Sax2DomHandler s2d) throws SAXException, IOException {
      if (mSystemId == null) {
        parser.parse(mSource, s2d);
      } else {
        parser.parse(mSource, s2d, mSystemId);
      }
    }
  }

  private HtmlParse () {
    throw new AssertionError("not allowed");
  }
}
