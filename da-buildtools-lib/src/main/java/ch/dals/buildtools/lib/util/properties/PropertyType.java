/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.lib.util.properties;


import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import ch.dals.buildtools.lib.util.CheckArg;


/**
 * Abstract superclass for all classes representing types of properties.
 *
 * @param <T> the Java type of the property
 *
 * @author zisch
 */
public abstract class PropertyType <T> {

  /**
   * Returns the {@link PropertyType} for the specified {@code javaType}.
   *
   * @param <T> the Java type
   * @param javaType the Java type (not {@code null})
   *
   * @return the {@link PropertyType} for the specified {@code javaType}
   *
   * @throws IllegalArgumentException if no {@link PropertyType} corresponding to the {@code javaType} exists
   */
  @SuppressWarnings("unchecked")
  public static <T> PropertyType<T> forJavaType (final Class<T> javaType) {
    CheckArg.notNull("javaType", javaType);
    if (javaType.equals(String.class)) {
      return (PropertyType<T>) StringImpl.sInstance;
    } else if (javaType.equals(Path.class)) {
      return (PropertyType<T>) PathImpl.sInstance;
    } else if (javaType.equals(URI.class)) {
      return (PropertyType<T>) UriImpl.sInstance;
    } else if (javaType.isEnum()) {
      final Class<? extends Enum<?>> enumType = (Class<? extends Enum<?>>) javaType;
      return (PropertyType<T>) new EnumImpl<>(enumType);
    } else if (javaType.equals(Integer.class) || javaType.equals(int.class)) {
      return (PropertyType<T>) IntImpl.sInstance;
    } else {
      throw new IllegalArgumentException("Java type '" + javaType.getName() + "' not supported.");
    }
  }

  private final Class<T> mJavaType;

  PropertyType (final Class<T> javaType) {
    mJavaType = CheckArg.notNull("javaType", javaType);
  }

  /**
   * @return the Java type corresponding to this {@code PropertyType}
   */
  public Class<T> getJavaType () {
    return mJavaType;
  }

  /**
   * Parses the string representation of an property.
   *
   * @param str the string representation to parse
   *
   * @return the parsed property value
   *
   * @throws Exception in case of parse errors
   */
  public abstract T parseString (String str) throws Exception;

  /**
   * All property types implement a &ldquo;sensible&rdquo; equals method which will return {@code true} if and only if
   * the specified object is not {@code null} and logically represents the same property type as this instance.
   */
  /*
   * Implementation Notes:
   * 
   * Every property type needs a sensible equals method.
   * 
   * This default implementation returns {@code true} if and only if the specified object is not {@code null} and this
   * property type instance and the specified object have the same {@link #getClass() class}. This is suitable for
   * singleton implementations where all instances of some concrete implementation class have the same behaviour.
   * Concrete implementations which allow configuration of concrete instances must override this implementation (as well
   * as {@link #hashCode()})
   */
  @Override
  public boolean equals (final Object o) {
    return o == this || (o != null && getClass().equals(o.getClass()));
  }

  /**
   * All property types implement a {@code hashCode()} method consistent with {@link #equals(Object)}.
   */
  /*
   * Implementation Notes:
   * 
   * Every property type needs a sensible hashCode method.
   * 
   * This default implementations just returns the hash code of the concrete implementation {@link #getClass() class}.
   * This conforms with the default implementation of {@link #equals(Object)}; see there for more information.
   */
  @Override
  public int hashCode () {
    return getClass().hashCode();
  }

  /**
   * Formats the specified value to a string representation.
   *
   * @param value the value to format
   *
   * @return the string representation of {@code value}
   */
  public String formatString (final T value) {
    CheckArg.notNull("value", value);
    return value.toString();
  }

  private static final class StringImpl extends PropertyType<String> {
    private static final StringImpl sInstance = new StringImpl();

    private StringImpl () {
      super(String.class);
    }

    @Override
    public String parseString (final String str) {
      CheckArg.notNull("str", str);
      return str;
    }
  }

  private static final class PathImpl extends PropertyType<Path> {
    private static final PathImpl sInstance = new PathImpl();

    private PathImpl () {
      super(Path.class);
    }

    @Override
    public Path parseString (final String str) {
      CheckArg.notNull("str", str);
      return Paths.get(str);
    }
  }

  private static final class UriImpl extends PropertyType<URI> {
    private static final UriImpl sInstance = new UriImpl();

    private UriImpl () {
      super(URI.class);
    }

    @Override
    public URI parseString (final String str) throws URISyntaxException {
      CheckArg.notNull("str", str);
      return new URI(str);
    }
  }

  // private static final class DaLicenseInfoImpl extends PropertyType<DaLicenseInfo> {
  // private static final DaLicenseInfoImpl sInstance = new DaLicenseInfoImpl();
  //
  // private static final EnumImpl<LicenseType> sLicenseTypeEnum = new EnumImpl<>(LicenseType.class);
  //
  // private DaLicenseInfoImpl () {
  // super(DaLicenseInfo.class);
  // }
  //
  // @Override
  // public DaLicenseInfo parseString (final String str) throws Exception {
  // final LicenseType t = sLicenseTypeEnum.parseString(str);
  // return null;
  // }
  //
  // }

  private static final class EnumImpl <E extends Enum<?>> extends PropertyType<E> {
    private EnumImpl (final Class<E> enumType) {
      super(enumType);
      if (!enumType.isEnum()) {
        throw new IllegalArgumentException("enumType '" + enumType.getName() + "' does not denote a Java enum type.");
      }
    }

    @Override
    public E parseString (final String str) {
      return parseString(getJavaType(), str);
    }

    @SuppressWarnings("unchecked")
    private static <E extends Enum<?>, T extends Enum<T>> E parseString (final Class<E> javaType, final String str) {
      CheckArg.notNull("str", str);
      final Class<T> enumType = (Class<T>) javaType;
      return (E) Enum.valueOf(enumType, str);
    }

    @Override
    public boolean equals (final Object o) {
      return super.equals(o) && getJavaType().equals(((PropertyType<?>) o).getJavaType());
    }

    @Override
    public int hashCode () {
      return super.hashCode() * 31 + getJavaType().hashCode();
    }
  }

  private static final class IntImpl extends PropertyType<Integer> {
    private static final IntImpl sInstance = new IntImpl();

    private IntImpl () {
      super(Integer.class);
    }

    @Override
    public Integer parseString (final String str) {
      return new Integer(str);
    }
  }
}
