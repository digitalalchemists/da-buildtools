/*
 * This file is part of the DA Secure Communication Framework (SCF) project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA SCF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA SCF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA SCF.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.lib.util;


/**
 * A utility to help calculating Java {@linkplain Object#hashCode() hash code} values.
 * 
 * @author zisch
 */
public final class HashUtil {

  private static final int PRIME = 31;

  /**
   * Build the combined hash code for the five specified hash codes.
   * <p>
   * This method is equivalent to the following pseudo code (though the actual implementation might differ):
   * 
   * <pre>
   * int result = v0;
   * for (int i = 1; i &lt; 5; i++) {
   *   result = hash(result, v$i);
   * }
   * return result;
   * </pre>
   * <p>
   * 
   * @param v0 hash code value
   * @param v1 hash code value
   * @param v2 hash code value
   * @param v3 hash code value
   * @param v4 hash code value
   * 
   * @return the combined hash code for the specified hash codes
   */
  public static int hash (final int v0, final int v1, final int v2, final int v3, final int v4) {
    return hash(hash(v0, v1, v2, v3), v4);
  }

  /**
   * Build the combined hash code for the four specified hash codes.
   * <p>
   * This method is equivalent to the following pseudo code (though the actual implementation might differ):
   * 
   * <pre>
   * int result = v0;
   * for (int i = 1; i &lt; 4; i++) {
   *   result = hash(result, v$i);
   * }
   * return result;
   * </pre>
   * <p>
   * 
   * @param v0 hash code value
   * @param v1 hash code value
   * @param v2 hash code value
   * @param v3 hash code value
   * 
   * @return the combined hash code for the specified hash codes
   */
  public static int hash (final int v0, final int v1, final int v2, final int v3) {
    return hash(hash(v0, v1, v2), v3);
  }

  /**
   * Build the combined hash code for the three specified hash codes.
   * <p>
   * This method is equivalent to the following pseudo code (though the actual implementation might differ):
   * 
   * <pre>
   * int result = v0;
   * for (int i = 1; i &lt; 3; i++) {
   *   result = hash(result, v$i);
   * }
   * return result;
   * </pre>
   * <p>
   * 
   * @param v0 hash code value
   * @param v1 hash code value
   * @param v2 hash code value
   * 
   * @return the combined hash code for the specified hash codes
   */
  public static int hash (final int v0, final int v1, final int v2) {
    return hash(hash(v0, v1), v2);
  }

  /**
   * Build the combined hash code for the two specified hash codes. The hash code is calculated by multiplying
   * {@code v0} with a prime number and adding {@code v1}.
   * 
   * @param v0 hash code value
   * @param v1 hash code value
   * 
   * @return the combined hash code for the specified hash codes
   */
  public static int hash (final int v0, final int v1) {
    return v0 * PRIME + v1;
  }

  /**
   * Returns the hash code of the specified {@code long} primitive. This is equivalent to
   * {@code Long.valueOf(value).hashCode()} but more efficient.
   * 
   * @param value the value to calculate the hash code for
   * 
   * @return the calculated hash code {@code (int)(value ^ (value >>> 32))}
   * 
   * @see Long#hashCode()
   */
  public static int hash (final long value) {
    return (int) (value ^ (value >>> 32));
  }

  /**
   * Returns the hash code of the specified {@code float} primitive. This is equivalent to
   * {@code Float.valueOf(value).hashCode()} but more efficient.
   * 
   * @param value the value to calculate the hash code for
   * 
   * @return the calculated hash code {@code Float.floatToIntBits(value)}
   * 
   * @see Float#hashCode()
   * @see Float#floatToIntBits(float)
   */
  public static int hash (final float value) {
    return Float.floatToIntBits(value);
  }

  /**
   * Returns the hash code of the specified {@code double} primitive. This is equivalent to
   * {@code Double.valueOf(value).hashCode()} but more efficient.
   * 
   * @param value the value to calculate the hash code for
   * 
   * @return the calculated hash code which is the ({@linkplain #hash(long) hashed} result of
   *         {@code Double.doubleToLongBits(value)}
   * 
   * @see Double#hashCode()
   * @see Double#doubleToLongBits(double)
   * @see #hash(long)
   */
  public static int hash (final double value) {
    return hash(Double.doubleToLongBits(value));
  }

  /**
   * Returns the hash code of the specified {@code boolean} primitive. This is equivalent to
   * {@code Boolean.valueOf(value).hashCode()} but (slightly) more efficient.
   * 
   * @param value the value to calculate the hash code for
   * 
   * @return the calculated hash code which is {@code 1231} if {@code value == true} or {@code 1237} otherwise
   * 
   * @see Boolean#hashCode()
   */
  public static int hash (final boolean value) {
    return value ? 1231 : 1237;
  }

  /**
   * Returns the hash code of the specified {@code Object} or 0 for {@code null}.
   * <p>
   * This is the same as {@code value.hashCode()} but handles {@code null} values.
   * 
   * @param value the {@code Object} reference
   * 
   * @return the hash code of the specified {@code Object} or 0 for {@code null}
   */
  public static int hash (final Object value) {
    return value == null ? 0 : value.hashCode();
  }

  /**
   * 
   * 
   * @param value the {@code CharSequence} to hash
   * 
   * @return the hash code for the specified {@code CharSequence}
   */
  public static int hash (final CharSequence value) {
    int h = 0;
    if (value != null) {
      final int len = value.length();
      if (len > 0) {
        h = value.charAt(0);
        for (int i = 1; i < len; i++) {
          h = 31 * h + value.charAt(i);
        }
      }
    }
    return h;
  }

  // TODO: hashIgnoreCase(CharSequence)

  private HashUtil () {
    throw new AssertionError("not allowed");
  }
}
