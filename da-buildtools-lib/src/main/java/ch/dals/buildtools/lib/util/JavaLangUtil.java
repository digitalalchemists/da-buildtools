/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.lib.util;


import java.io.IOException;


/**
 * TODO [javadoc]: type StringUtil
 *
 * @author zisch
 */
public final class JavaLangUtil {

  public static <T extends Appendable> T appendUnicodeEscaped (final T output, final CharPredicate predicate,
          final CharSequence cs) throws IOException {
    try {
      return appendUnicodeEscaped(output, predicate, cs, 0, cs.length());
    } catch (final NullPointerException npe) {
      CheckArg.notNull(npe, "cs", cs);
      throw npe;
    }
  }

  public static <T extends Appendable> T appendUnicodeEscaped (final T output, final CharPredicate predicate,
          final CharSequence cs, final int off, final int len) throws IOException {
    CheckArg.notNull("output", output);
    CheckArg.notNull("predicate", predicate);
    CheckArg.notNull("cs", cs);
    CheckArg.indexValidOffsetAndLength("off", off, "len", len, "cs.length", cs.length());
    final int end = off + len;
    for (int i = off; i < end; i++) {
      appendUnicodeEscaped(output, predicate, cs.charAt(i));
    }
    return output;
  }

  public static <T extends Appendable> T appendUnicodeEscaped (final T output, final CharPredicate predicate,
          final char c) throws IOException {
    try {
      if (predicate.test(c)) {
        output.append(c);
      } else {
        output.append(String.format("\\u%04x", Integer.valueOf(c)));
      }
      return output;
    } catch (final NullPointerException npe) {
      CheckArg.notNull(npe, "output", output);
      CheckArg.notNull(npe, "predicate", predicate);
      throw npe;
    }
  }

  /**
   * If the specified {@code clazz} represents a primitive type (like {@code int.class}) the corresponding wrapper type
   * (like {@code java.lang.Integer.class} is returned, otherwise the specified {@code clazz} instance is returned
   * unchanged.
   *
   * @param clazz the {@code Class} to convert (not {@code  null})
   *
   * @return the corresponding wrapper class if the specified {@code clazz} represents a primitive type, otherwise the
   *         unchanged {@code clazz} instance
   */
  @SuppressWarnings("unchecked")
  static <T> Class<T> primitiveToWrapper (final Class<T> clazz) {
    // Note: The (Class<T>) casts work because the class objects for primitive and wrapper
    // types are both of the same type Class<T>; for example long.class and java.lang.Long.class
    // are both of type Class<Long>. (Therefore we can safely suppress the unchecked warnings.)
    switch (clazz.getName()) {
      case "boolean":
        return (Class<T>) Boolean.class;
      case "byte":
        return (Class<T>) Byte.class;
      case "char":
        return (Class<T>) Character.class;
      case "double":
        return (Class<T>) Double.class;
      case "float":
        return (Class<T>) Float.class;
      case "int":
        return (Class<T>) Integer.class;
      case "long":
        return (Class<T>) Long.class;
      case "short":
        return (Class<T>) Short.class;
      case "void":
        return (Class<T>) Void.class;
      default:
        return clazz;
    }
  }

  private JavaLangUtil () {
    throw new AssertionError("not allowed");
  }
}
