/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.lib.util;


import java.io.IOException;
import java.nio.file.DirectoryStream.Filter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


/**
 * TODO [javadoc]: type DirectoryStreamFilters
 *
 * @author zisch
 */
public class DirectoryStreamFilters {

  private static final Path[] EMPTY_PATHS = {};

  public static Filter<Path> sameFile (final CharSequence... files) {
    return sameFile(Arrays.asList(CheckArg.notNull("files", files)));
  }

  public static Filter<Path> sameFile (final Collection<? extends CharSequence> files) {
    CheckArg.noNulls("files", files);
    final Path[] filePaths = new Path[files.size()];
    int i = 0;
    for (final CharSequence f : files) {
      filePaths[i] = Paths.get(f.toString());
    }
    return new SameFileFilter(filePaths);
  }

  public static Filter<Path> sameFilePath (final Path... files) {
    return sameFilePath(Arrays.asList(CheckArg.notNull("files", files)));
  }

  public static Filter<Path> sameFilePath (final Collection<? extends Path> files) {
    CheckArg.noNulls("files", files);
    return new SameFileFilter(files.toArray(EMPTY_PATHS));
  }

  private static final class SameFileFilter implements Filter<Path> {
    private final List<Path> mFiles;

    private SameFileFilter (final Path[] files) {
      mFiles = Collections.unmodifiableList(Arrays.asList(files));
    }

    @Override
    public boolean accept (final Path entry) {
      CheckArg.notNull("entry", entry);
      for (int i = 0; i < mFiles.size(); i++) {
        if (entry.equals(mFiles.get(i))) {
          return true;
        }
      }
      return false;
    }
  }

  public static Filter<Path> fileName (final CharSequence name) {
    CheckArg.notNull("name", name);
    return fileName(Paths.get(name.toString()));
  }

  public static Filter<Path> fileName (final Path name) {
    CheckArg.notNull("name", name);
    return new FileNameFilter(name);
  }

  private static final class FileNameFilter implements Filter<Path> {
    private final Path mName;

    private FileNameFilter (final Path name) {
      mName = name;
    }

    @Override
    public boolean accept (final Path entry) {
      CheckArg.notNull("entry", entry);
      return entry.getFileName().equals(mName);
    }
  }

  @SafeVarargs
  public static <T> Filter<? super T> allOf (final Filter<? super T>... filters) {
    CheckArg.notNull("filters", filters);
    return allOf(Arrays.asList(filters));
  }

  public static <T> Filter<? super T> allOf (final Collection<? extends Filter<? super T>> filters) {
    CheckArg.noNulls("filters", filters);
    return new AllOfFilter<>(filters);
  }

  private static final class AllOfFilter <T> implements Filter<T> {
    private final List<Filter<? super T>> mFilters;

    private AllOfFilter (final Collection<? extends Filter<? super T>> filters) {
      mFilters = Collections.unmodifiableList(new ArrayList<>(filters));
    }

    @Override
    public boolean accept (final T entry) throws IOException {
      for (int i = 0; i < mFilters.size(); i++) {
        if (!mFilters.get(i).accept(entry)) {
          return false;
        }
      }
      return true;
    }
  }

  public static <T> Filter<T> not (final Filter<T> filter) {
    CheckArg.notNull("filter", filter);
    return new NotFilter<>(filter);
  }

  private static final class NotFilter <T> implements Filter<T> {
    private final Filter<T> mFilter;

    private NotFilter (final Filter<T> filter) {
      mFilter = filter;
    }

    @Override
    public boolean accept (final T entry) throws IOException {
      return !mFilter.accept(entry);
    }
  }

  @SuppressWarnings("unchecked")
  public static <T> Filter<T> acceptAll () {
    return (Filter<T>) AcceptAllFilter.INSTANCE;
  }

  private static final class AcceptAllFilter implements Filter<Object> {
    private static final AcceptAllFilter INSTANCE = new AcceptAllFilter();

    @Override
    public boolean accept (final Object entry) {
      return true;
    }
  }

  private DirectoryStreamFilters () {
    throw new AssertionError("not allowed");
  }
}
