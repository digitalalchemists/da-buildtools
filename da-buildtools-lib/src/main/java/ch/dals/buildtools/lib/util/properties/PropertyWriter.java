/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.lib.util.properties;


import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.regex.Pattern;

import ch.dals.buildtools.lib.util.CharPredicateUtil;
import ch.dals.buildtools.lib.util.CheckArg;
import ch.dals.buildtools.lib.util.JavaLangUtil;


/**
 * TODO [javadoc]: type PropertyUtil
 *
 * @author zisch
 */
public final class PropertyWriter {
  private static final Charset LATIN1 = Charset.forName("ISO-8859-1");

  private static final Pattern sLineSeparatorPattern = Pattern.compile("\r\n|\r|\n");

  public static void writeProperties (final String comments, final Map<?, ?> properties, final Path file)
          throws IOException {
    CheckArg.notNull("properties", properties);
    CheckArg.notNull("file", file);
    try (final Writer w = Files.newBufferedWriter(file, LATIN1)) {
      final PropertyWriter pw = new PropertyWriter(w, true);
      pw.writeComments(comments);
      pw.writeProperties(properties);
    }
  }

  private final Appendable mOutput;

  private final boolean mUnicodeEscape;

  private final String mLineSeparator;

  public PropertyWriter (final OutputStream output) {
    this(output, null);
  }

  public PropertyWriter (final OutputStream output, final String lineSeparator) {
    this(new OutputStreamWriter(output, LATIN1), true, lineSeparator);
  }

  public PropertyWriter (final Appendable output, final boolean unicodeEscape) {
    this(output, unicodeEscape, null);
  }

  public PropertyWriter (final Appendable output, final boolean unicodeEscape, final String lineSeparator) {
    mOutput = CheckArg.notNull("output", output);
    mUnicodeEscape = unicodeEscape;
    mLineSeparator = lineSeparator == null ? System.getProperty("line.separator") : lineSeparator;
  }

  public Appendable getOutput () {
    return mOutput;
  }

  public boolean getUnicodeEscape () {
    return mUnicodeEscape;
  }

  public String getLineSeparator () {
    return mLineSeparator;
  }

  public PropertyWriter writeComments (final CharSequence comments) throws IOException {
    if (comments != null) {
      final String[] lines = sLineSeparatorPattern.split(comments, -1);
      for (int i = 0; i < lines.length; i++) {
        if (!lines[i].startsWith("#")) {
          mOutput.append('#');
        }
        if (mUnicodeEscape) {
          JavaLangUtil.appendUnicodeEscaped(mOutput, CharPredicateUtil.forLatin1(), lines[i]);
        } else {
          mOutput.append(lines[i]);
        }
        mOutput.append(mLineSeparator);
      }
    }
    return this;
  }

  public PropertyWriter writeLineSeparator () throws IOException {
    mOutput.append(mLineSeparator);
    return this;
  }

  public PropertyWriter writeProperties (final Map<?, ?> properties) throws IOException {
    CheckArg.notNull("properties", properties);
    for (final Map.Entry<?, ?> e : properties.entrySet()) {
      writeProperty(e.getKey(), e.getValue());
    }
    return this;
  }

  public PropertyWriter writeProperty (final Object key, final Object value) throws IOException {
    CheckArg.notNull("key", key);
    writeKeyOrValue(true, key);
    if (value != null) {
      mOutput.append('=');
      writeKeyOrValue(false, value);
    }
    mOutput.append(mLineSeparator);
    return this;
  }

  private void writeKeyOrValue (final boolean key, final Object v) throws IOException {
    // TODO: for values it would be nice to split the CharSequence at line separators into multiple natural lines
    if (v == null) {
      return;
    }
    final CharSequence cs = v instanceof CharSequence ? (CharSequence) v : v.toString();
    final int len = cs.length();
    boolean sawNonSpace = false;
    for (int i = 0; i < len; i++) {
      final char c = cs.charAt(i);
      switch (c) {
        case '#':
        case '!':
        case '=':
        case ':':
        case ' ':
          if (c != ' ' || key || !sawNonSpace) {
            mOutput.append('\\');
          }
          mOutput.append(c);
          break;

        case '\t':
          mOutput.append("\\t");
          break;

        case '\n':
          mOutput.append("\\n");
          break;

        case '\r':
          mOutput.append("\\r");
          break;

        default:
          if (mUnicodeEscape) {
            JavaLangUtil.appendUnicodeEscaped(mOutput, CharPredicateUtil.forAsciiWithoutIsoControl(), c);
          } else {
            mOutput.append(c);
          }
      }
      sawNonSpace = sawNonSpace || c != ' ';
    }
  }
}
