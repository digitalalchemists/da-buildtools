/*
 * This file is part of the DA Secure Communication Framework (SCF) project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA SCF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA SCF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA SCF.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.lib.util;


import java.util.concurrent.atomic.AtomicReference;


/**
 * Abstraction of a value which is created when it is first requested.
 * <p>
 * Internally an {@link AtomicReference} is used
 *
 * @param <E> type of produced
 *
 * @author zisch
 */
public abstract class AtomicLazyFactory <E> {
  private final AtomicReference<E> mRef = new AtomicReference<>();

  /**
   * Default constructor.
   */
  protected AtomicLazyFactory () {
    super();
  }

  /**
   * TODO [javadoc]: method get
   *
   * @return TODO [javadoc]
   */
  public E get () {
    E result = mRef.get();
    while (result == null) {
      final E created = produce();
      if (created == null) {
        throw new IllegalStateException("Implementation of '" + getClass().getName() + ".produce()' returned null.");
      }
      if (mRef.compareAndSet(null, created)) {
        result = created;
        break;
      }
      result = mRef.get();
    }
    return result;
  }

  /**
   * TODO [javadoc]: method produce
   *
   * @return TODO [javadoc]
   */
  protected abstract E produce ();
}
