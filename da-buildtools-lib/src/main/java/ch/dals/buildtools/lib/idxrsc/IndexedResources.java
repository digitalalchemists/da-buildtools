/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.lib.idxrsc;


import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import ch.dals.buildtools.lib.util.CheckArg;


/**
 * TODO [javadoc]: type IndexedResources
 *
 * @author zisch
 */
public abstract class IndexedResources {

  /**
   * TODO [javadoc]
   */
  public static final String DEFAULT_INDEX_NAME = "index.properties";

  private final List<String> mIndexEntries;

  /**
   * TODO [javadoc]: constructor IndexedResources
   */
  protected IndexedResources () {
    this(DEFAULT_INDEX_NAME);
  }

  /**
   * TODO [javadoc]: constructor IndexedResources
   *
   * @param indexFileName TODO [javadoc]
   */
  protected IndexedResources (final String indexFileName) {
    CheckArg.notEmpty("indexFileName", indexFileName);
    if (!indexFileName.endsWith(".properties")) {
      throw new IllegalArgumentException("indexFileName must end with '.properties': '" + indexFileName + "'");
    }
    mIndexEntries = loadIndex(indexFileName);
  }

  private List<String> loadIndex (final String indexFileName) {
    final Properties props = new Properties();
    try (final InputStream is = openIndexFile(indexFileName)) {
      props.load(is);
    } catch (final Exception exc) {
      throw new IllegalStateException("Failed to load index resource '" + indexFileName + "' of '"
              + getClass().getName() + "': " + exc, exc);
    }
    final String[] indexEntries = new String[props.size()];
    int i = 0;
    for (final Object k : props.keySet()) {
      indexEntries[i++] = (String) k;
    }
    Arrays.sort(indexEntries);
    return Collections.unmodifiableList(Arrays.asList(indexEntries));
  }

  private InputStream openIndexFile (final String indexFileName) throws FileNotFoundException {
    final InputStream is = getClass().getResourceAsStream(indexFileName);
    if (is == null) {
      throw new FileNotFoundException("Cannot find resource on classpath.");
    }
    return is;
  }

  /**
   * @return TODO [javadoc]
   */
  public List<String> getEntries () {
    return mIndexEntries;
  }

  /**
   * @return TODO [javadoc]
   */
  public Iterable<URL> asUrls () {
    return new Iterable<URL>() {
      @Override
      public Iterator<URL> iterator () {
        return new AbstractIterator<URL>() {
          @Override
          URL resourceForName (final String name) {
            return mLoadingClass.getResource(name);
          }
        };
      }
    };
  }

  /**
   * @return TODO [javadoc]
   */
  public Iterable<InputStream> asInputStreams () {
    return new Iterable<InputStream>() {
      @Override
      public Iterator<InputStream> iterator () {
        return new AbstractIterator<InputStream>() {
          @Override
          InputStream resourceForName (final String name) {
            return mLoadingClass.getResourceAsStream(name);
          }
        };
      }
    };
  }

  private abstract class AbstractIterator <E> implements Iterator<E> {
    final Class<?> mLoadingClass = IndexedResources.this.getClass();

    private final Iterator<String> mNames = mIndexEntries.iterator();

    @Override
    public boolean hasNext () {
      return mNames.hasNext();
    }

    @Override
    public E next () {
      final String n = mNames.next();
      final E rsc = resourceForName(n);
      if (rsc == null) {
        throw new IllegalStateException("Cannot find resource with name '" + n + "' for class '"
                + mLoadingClass.getName() + "'.");
      }
      return null;
    }

    @Override
    public void remove () {
      throw new UnsupportedOperationException();
    }

    abstract E resourceForName (final String name);
  }
}
