/*
 * This file is part of the DA Secure Communication Framework (SCF) project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA SCF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA SCF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA SCF.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.lib.util;


import java.io.Serializable;
import java.lang.reflect.Member;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;


/**
 * Utility functions and comparator implementations to compare primitives and objects.
 *
 * @author zisch
 */
public final class Compare {

  // ---- Public Comparator Factory Methods

  /**
   * TODO [javadoc]: method chain
   *
   * @param <T> TODO [javadoc]
   * @param cmps TODO [javadoc]
   *
   * @return TODO [javadoc]
   */
  @SafeVarargs
  public static <T> Comparator<T> chain (final Comparator<? super T>... cmps) {
    try {
      if (cmps.length == 1) {
        /*
         * Note: Every Comparator<? super T> can also be used as a Comparator<T> (because compare(X o1, X o2) for some X
         * super T will accept any T as compare(T o1, T o2) would), so we can safely do this cast:
         */
        @SuppressWarnings("unchecked")
        final Comparator<T> result = (Comparator<T>) cmps[0];
        return result;
      } else {
        return new ChainComparator<>(Arrays.copyOf(cmps, cmps.length));
      }
    } catch (final NullPointerException exc) {
      CheckArg.notNull("cmps", cmps);
      throw exc;
    }
  }

  /**
   * TODO [javadoc]: method chain
   *
   * @param <T> TODO [javadoc]
   * @param cmps TODO [javadoc]
   *
   * @return TODO [javadoc]
   */
  public static <T> Comparator<T> chain (final Collection<? extends Comparator<? super T>> cmps) {
    try {
      if (cmps.size() == 1) {
        /*
         * Note: Every Comparator<? super T> can also be used as a Comparator<T> (because compare(X o1, X o2) for some X
         * super T will accept any T as compare(T o1, T o2) would), so we can safely do this cast:
         */
        @SuppressWarnings("unchecked")
        final Comparator<T> result = (Comparator<T>) cmps.iterator().next();
        return result;
      } else {
        return new ChainComparator<>(cmps.toArray(emptyComparators()));
      }
    } catch (final NullPointerException exc) {
      CheckArg.notNull("cmps", cmps);
      throw exc;
    }
  }

  /**
   * TODO [javadoc]: method byProperty
   *
   * @param <T> TODO [javadoc]
   * @param <P> TODO [javadoc]
   * @param propertyClass TODO [javadoc]
   * @param propertyName TODO [javadoc]
   *
   * @return TODO [javadoc]
   */
  public static <T, P extends Comparable<? super P>> Comparator<T> byProperty (final Class<P> propertyClass,
          final String propertyName) {
    final Comparator<P> cmp = natural();
    return byProperty(propertyClass, propertyName, cmp);
  }

  /**
   * TODO [javadoc]: method byProperty
   *
   * @param <T> TODO [javadoc]
   * @param <P> TODO [javadoc]
   * @param propertyClass TODO [javadoc]
   * @param propertyName TODO [javadoc]
   * @param propertyComparator TODO [javadoc]
   *
   * @return TODO [javadoc]
   */
  public static <T, P> Comparator<T> byProperty (final Class<P> propertyClass, final String propertyName,
          final Comparator<? super P> propertyComparator) {
    return new ByPropertyComparator<>(propertyClass, propertyName, propertyComparator);
  }

  /**
   * TODO [javadoc]: method nullFirst
   *
   * @param <T> TODO [javadoc]
   * @param cmp TODO [javadoc]
   *
   * @return TODO [javadoc]
   */
  public static <T> Comparator<T> nullFirst (final Comparator<? super T> cmp) {
    return new NullComparator<>(true, cmp);
  }

  /**
   * TODO [javadoc]: method nullLast
   *
   * @param <T> TODO [javadoc]
   * @param cmp TODO [javadoc]
   *
   * @return TODO [javadoc]
   */
  public static <T> Comparator<T> nullLast (final Comparator<? super T> cmp) {
    return new NullComparator<>(false, cmp);
  }

  /**
   * TODO [javadoc]: method reverse
   *
   * @param <T> TODO [javadoc]
   * @param cmp TODO [javadoc]
   *
   * @return TODO [javadoc]
   */
  public static <T> Comparator<T> reverse (final Comparator<? super T> cmp) {
    if (cmp instanceof ReverseComparator<?>) {
      /*
       * Reasoning why we can cast cmp.mCmp to Comparator<T>:
       * 
       * 'cmp' is (according to the method signature) actually ReverseComparator<X super T> for some superclass X of T
       * (including X == T).
       * 
       * ==> Therefore 'cmp.mCmp' is Comparator<Y super X> for some superclass Y of X (including Y == X), which means
       * that Y is also a superclass of T (including Y == T).
       * 
       * ==> Because the Comparator interface only uses T for method parameters (as opposed to return values) in
       * 'compare(T o1, T o2)' and Y is a superclass of T, we can use every Comparator<Y> in place of a Comparator<T>,
       * which means we can safely cast the Comparator<Y> 'cmp.mCmp' and return it as a Comparator<T>.
       */
      @SuppressWarnings("unchecked")
      final Comparator<T> original = (Comparator<T>) ((ReverseComparator<?>) cmp).mCmp;
      return original;
    } else {
      return new ReverseComparator<>(cmp);
    }
  }

  /**
   * TODO [javadoc]: method natural
   *
   * @param <T> TODO [javadoc]
   *
   * @return TODO [javadoc]
   */
  public static <T extends Comparable<? super T>> Comparator<T> natural () {
    return castUnchecked(NaturalComparator.INSTANCE);
  }

  /**
   * TODO [javadoc]: method byIdentity
   *
   * @param <T> TODO [javadoc]
   *
   * @return TODO [javadoc]
   */
  public static <T> Comparator<T> byIdentity () {
    return castUnchecked(ByIdentityComparator.INSTANCE);
  }

  /**
   * Returns a comparator which compares the classes of the compared instances. Comparison is done using the compared
   * objects {@linkplain Object#getClass() classes}, first on the fully qualified {@linkplain Class#getName() class
   * name} and second on the {@linkplain System#identityHashCode(Object) identity hash code} of the class objects.
   * <p>
   * The returned comparator will be consistent with {@link Class#equals(Object)} and {@link Class#hashCode()} for the
   * classes of the compared instances.
   * <p>
   * The returned comparator will throw a {@linkplain NullPointerException} if one of the compared objects is
   * {@code null}.
   *
   * @return a comparator which compares the classes of the compared instances
   *
   * @see #classByName()
   */
  public static Comparator<Object> byClass () {
    return ByClassComparator.INSTANCE;
  }

  /**
   * Returns a comparator which compares class instances first on the fully qualified {@linkplain Class#getName() class
   * name} and second on the {@linkplain System#identityHashCode(Object) identity hash code} of the class objects.
   * <p>
   * The returned comparator will be consistent with {@link Class#equals(Object)} and {@link Class#hashCode()}.
   * <p>
   * The returned comparator will throw a {@linkplain NullPointerException} if one of the compared objects is
   * {@code null}.
   *
   * @return a comparator which compares the classes of the compared instances
   *
   * @see #byClass()
   */
  public static Comparator<Class<?>> classByName () {
    return ClassByNameComparator.INSTANCE;
  }

  /**
   * Returns a comparator which compares arbitrary {@link CharSequence}s lexicographically. The order imposed by the
   * returned comparator is identical to the natural order of {@link String}s as defined by
   * {@link String#compareTo(String)} (assuming that all compared {@code CharSequence}s are converted to strings using
   * {@link CharSequence#toString()} before comparison).
   * <p>
   * One intended use case is to be able to check for equality of {@code CharSequence}s, which is not possible using
   * {@code CharSequence.equals(Object)}. ({@code CharSequence} does not specify the behaviour of {@code equals(Object)}
   * and {@code hashCode()}.) Note: If you need to calculate the hash code of a {@code CharSequence} you might want to
   * use {@link HashUtil#hash(CharSequence)}.
   *
   * @return a comparator which compares arbitrary {@code CharSequence}s lexicographically
   *
   * @see HashUtil#hash(CharSequence)
   */
  public static Comparator<CharSequence> charSequence () {
    return CharSequenceComparator.INSTANCE;
  }

  // TODO: charSequenceIgnoreCase()!

  /**
   * TODO [javadoc]: method classMember
   *
   * @return TODO [javadoc]
   */
  public static Comparator<Member> classMember () {
    return ClassMemberComparator.INSTANCE;
  }

  // ---- Compare Methods for Primitive Values

  /**
   * Compares the specified {@code int} values and returns a negative integer, zero, or a positive integer as the first
   * argument is less than, equal to, or greater than the second. This is equivalent to (but more efficient than)
   * {@code Integer.valueOf(a).compareTo(Integer.valueOf(b))}.
   * <p>
   * Note: In Java 1.7+ this is implemented by {@code Integer.compare(int, int)}.
   *
   * @param a first argument
   * @param b second argument
   *
   * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater
   *         than the second
   */
  public static int compare (final int a, final int b) {
    return a < b ? -1 : (a > b ? 1 : 0);
  }

  /**
   * Compares the specified {@code long} values and returns a negative integer, zero, or a positive integer as the first
   * argument is less than, equal to, or greater than the second. This is equivalent to (but more efficient than)
   * {@code Long.valueOf(a).compareTo(Long.valueOf(b))}.
   * <p>
   * Note: In Java 1.7+ this is implemented by {@code Long.compare(long, long)}.
   *
   * @param a first argument
   * @param b second argument
   *
   * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater
   *         than the second
   */
  public static int compare (final long a, final long b) {
    return a < b ? -1 : (a > b ? 1 : 0);
  }

  /**
   * Compares the specified {@code boolean} values and returns a negative integer, zero, or a positive integer as the
   * first argument is less than, equal to, or greater than the second. This is equivalent to (but more efficient than)
   * {@code Boolean.valueOf(a).compareTo(Boolean.valueOf(b))}.
   * <p>
   * Note: In Java 1.7+ this is implemented by {@code Boolean.compare(int, int)}.
   *
   * @param a first argument
   * @param b second argument
   *
   * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater
   *         than the second
   */
  public static int compare (final boolean a, final boolean b) {
    return (a == b) ? 0 : (a ? 1 : -1);
  }

  /**
   * TODO [javadoc]: method compareUnsigned
   *
   * @param a TODO [javadoc]
   * @param b TODO [javadoc]
   *
   * @return TODO [javadoc]
   */
  public static int compareUnsigned (final int a, final int b) {
    if (a < 0) {
      if (b >= 0) {
        return 1;
      } else {
        return compare(a, b);
      }
    } else {
      if (b < 0) {
        return -1;
      } else {
        return compare(a, b);
      }
    }
  }

  /**
   * TODO [javadoc]: method compareUnsigned
   *
   * @param a TODO [javadoc]
   * @param b TODO [javadoc]
   *
   * @return TODO [javadoc]
   */
  public static int compareUnsigned (final long a, final long b) {
    if (a < 0) {
      if (b >= 0) {
        return 1;
      } else {
        return compare(a, b);
      }
    } else {
      if (b < 0) {
        return -1;
      } else {
        return compare(a, b);
      }
    }
  }

  // ---- Equals Methods

  /**
   * TODO [javadoc]: method equals
   *
   * @param o1 TODO [javadoc]
   * @param o2 TODO [javadoc]
   *
   * @return TODO [javadoc]
   */
  public static boolean equals (final Object o1, final Object o2) {
    return o1 == null ? o2 == null : o2 != null && o1.equals(o2);
  }

  // ---- Private Helper Methods

  @SuppressWarnings("unchecked")
  private static <T> Comparator<T> castUnchecked (final Comparator<?> cmp) {
    return (Comparator<T>) cmp;
  }

  private static final Comparator<?>[] EMPTY_COMPARATORS = {};

  @SuppressWarnings("unchecked")
  private static <T> Comparator<T>[] emptyComparators () {
    return (Comparator<T>[]) EMPTY_COMPARATORS;
  }

  // ---- Abstract Comparator Base Classes

  private static abstract class AbstractSingletonComparator <T> implements Comparator<T>, Serializable {
    private static final long serialVersionUID = -1329492243771970641L;

    private Object readResolve () {
      return singletonInstance();
    }

    @Override
    public boolean equals (final Object o) {
      return o != null && getClass().equals(o.getClass());
    }

    @Override
    public int hashCode () {
      return getClass().hashCode();
    }

    @Override
    public String toString () {
      return getClass().getSimpleName();
    }

    abstract AbstractSingletonComparator<?> singletonInstance ();
  }

  private static final class NaturalComparator <T extends Comparable<? super T>> extends AbstractSingletonComparator<T> {
    private static final long serialVersionUID = -2462137871021782501L;

    private static final NaturalComparator<?> INSTANCE = new NaturalComparator<Integer>();

    @Override
    public int compare (final T o1, final T o2) {
      CheckArg.notNull("o2", o2);
      try {
        return o1.compareTo(o2);
      } catch (final NullPointerException exc) {
        CheckArg.notNull(exc, "o1", o1);
        throw exc;
      }
    }

    @Override
    NaturalComparator<?> singletonInstance () {
      return INSTANCE;
    }
  }

  private static final class ByIdentityComparator <T> extends AbstractSingletonComparator<T> {
    private static final long serialVersionUID = -4271326312922576497L;

    private static final ByIdentityComparator<?> INSTANCE = new ByIdentityComparator<>();

    @Override
    public int compare (final T o1, final T o2) {
      CheckArg.notNull("o1", o1);
      CheckArg.notNull("o2", o2);
      return Compare.compare(System.identityHashCode(o1), System.identityHashCode(o2));
    }

    @Override
    ByIdentityComparator<?> singletonInstance () {
      return INSTANCE;
    }
  }

  private static final class NullComparator <T> implements Comparator<T>, Serializable {
    private static final long serialVersionUID = 2445725040891440177L;

    private final int mDir;

    private final Comparator<? super T> mCmp;

    private NullComparator (final boolean nullFirst, final Comparator<? super T> cmp) {
      mDir = nullFirst ? -1 : 1;
      mCmp = CheckArg.notNull("cmp", cmp);
    }

    @Override
    public int compare (final T o1, final T o2) {
      return o1 == null ? (o2 == null ? 0 : mDir) : (o2 == null ? -mDir : mCmp.compare(o1, o2));
    }

    @Override
    public boolean equals (final Object o) {
      if (o instanceof NullComparator) {
        final NullComparator<?> nc = (NullComparator<?>) o;
        return mDir == nc.mDir && mCmp.equals(nc.mCmp);
      } else {
        return false;
      }
    }

    @Override
    public int hashCode () {
      return HashUtil.hash(getClass().hashCode(), mDir, mCmp.hashCode());
    }

    @Override
    public String toString () {
      return getClass().getSimpleName() + "[" + (mDir < 0 ? "nullFirst" : "nullLast") + ", " + mCmp + "]";
    }
  }

  private static final class ReverseComparator <T> implements Comparator<T>, Serializable {
    private static final long serialVersionUID = -496555113279369866L;

    private final Comparator<? super T> mCmp;

    private ReverseComparator (final Comparator<? super T> cmp) {
      mCmp = CheckArg.notNull("cmp", cmp);
    }

    @Override
    public int compare (final T o1, final T o2) {
      return -mCmp.compare(o1, o2);
    }

    @Override
    public boolean equals (final Object o) {
      return o instanceof ReverseComparator && mCmp.equals(((ReverseComparator<?>) o).mCmp);
    }

    @Override
    public int hashCode () {
      return HashUtil.hash(getClass().hashCode(), mCmp.hashCode());
    }

    @Override
    public String toString () {
      return getClass().getSimpleName() + "[" + mCmp + "]";
    }
  }

  private static final class ByClassComparator extends AbstractSingletonComparator<Object> {
    private static final long serialVersionUID = 2279520727649420942L;

    private static final ByClassComparator INSTANCE = new ByClassComparator();

    @Override
    public int compare (final Object o1, final Object o2) {
      try {
        return ClassByNameComparator.INSTANCE.compare(o1.getClass(), o2.getClass());
      } catch (final NullPointerException npe) {
        CheckArg.notNull(npe, "o1", o1);
        CheckArg.notNull(npe, "o2", o2);
        throw npe;
      }
    }

    @Override
    ByClassComparator singletonInstance () {
      return INSTANCE;
    }
  }

  private static final class ClassByNameComparator extends AbstractSingletonComparator<Class<?>> {
    private static final long serialVersionUID = 3458449524913623286L;

    private static final ClassByNameComparator INSTANCE = new ClassByNameComparator();

    @Override
    public int compare (final Class<?> o1, final Class<?> o2) {
      try {
        int result = o1.getName().compareTo(o2.getName());
        if (result == 0) {
          result = byIdentity().compare(o1, o2);
        }
        return result;
      } catch (final NullPointerException npe) {
        CheckArg.notNull(npe, "o1", o1);
        CheckArg.notNull(npe, "o2", o2);
        throw npe;
      }
    }

    @Override
    ClassByNameComparator singletonInstance () {
      return INSTANCE;
    }
  }

  private static final class ChainComparator <T> implements Comparator<T>, Serializable {
    private static final long serialVersionUID = -4658599268580758059L;

    private final Comparator<? super T>[] mCmps;

    /**
     * NOTE: This constructor assumes that the specified array is already a defensive copy of the array/collection and
     * will use the referenced array object without further copying!
     */
    private ChainComparator (final Comparator<? super T>[] cmps) {
      CheckArg.notEmpty("cmps", cmps);
      CheckArg.noNulls("cmps", cmps);
      mCmps = cmps;
    }

    @Override
    public int compare (final T o1, final T o2) {
      for (int i = 0; i < mCmps.length; i++) {
        final int result = mCmps[i].compare(o1, o2);
        if (result != 0) {
          return result;
        }
      }
      return 0;
    }

    @Override
    public boolean equals (final Object o) {
      return o instanceof ChainComparator && Arrays.equals(mCmps, ((ChainComparator<?>) o).mCmps);
    }

    @Override
    public int hashCode () {
      return HashUtil.hash(getClass().hashCode(), Arrays.hashCode(mCmps));
    }

    @Override
    public String toString () {
      final StringBuilder sb = new StringBuilder(64);
      sb.append(getClass().getSimpleName()).append('[');
      for (int i = 0; i < mCmps.length; i++) {
        if (i > 0) {
          sb.append(", ");
        }
        sb.append(mCmps[i]);
      }
      sb.append(']');
      return sb.toString();
    }
  }

  private static final class ByPropertyComparator <T, P> implements Comparator<T>, Serializable {
    private static final long serialVersionUID = 5902919794827850050L;

    private final Class<P> mPropertyClass;

    private final String mPropertyName;

    private final Comparator<? super P> mPropertyComparator;

    private ByPropertyComparator (final Class<P> propertyClass, final String propertyName,
            final Comparator<? super P> propertyComparator) {
      mPropertyClass = CheckArg.notNull("propertyClass", propertyClass);
      mPropertyName = CheckArg.isJavaIdentifier("propertyName", propertyName);
      mPropertyComparator = CheckArg.notNull("propertyComparator", propertyComparator);
    }

    @Override
    public int compare (final T o1, final T o2) {

      return 0;
    }

    @Override
    public boolean equals (final Object o) {
      if (o instanceof ByPropertyComparator<?, ?>) {
        final ByPropertyComparator<?, ?> bpc = (ByPropertyComparator<?, ?>) o;
        return mPropertyClass.equals(bpc.mPropertyClass) && mPropertyName.equals(bpc.mPropertyName)
                && mPropertyComparator.equals(bpc.mPropertyComparator);
      } else {
        return false;
      }
    }

    @Override
    public int hashCode () {
      return HashUtil.hash(mPropertyClass.hashCode(), mPropertyName.hashCode(), mPropertyComparator.hashCode());
    }

    @Override
    public String toString () {
      return getClass().getSimpleName() + "[" + mPropertyClass.getName() + " " + mPropertyName + ", "
              + mPropertyComparator + "]";
    }
  }

  private static final class CharSequenceComparator extends AbstractSingletonComparator<CharSequence> {
    private static final long serialVersionUID = 2942312326881625454L;

    private static final CharSequenceComparator INSTANCE = new CharSequenceComparator();

    private CharSequenceComparator () {
      super();
    }

    @Override
    public int compare (final CharSequence o1, final CharSequence o2) {
      CheckArg.notNull("o1", o1);
      if (o1 == o2) {
        return 0;
      } else {
        try {
          final int l1 = o1.length();
          final int l2 = o2.length();
          final int l = Math.min(l1, l2);
          for (int i = 0; i < l; i++) {
            final int c1 = o1.charAt(i);
            final int c2 = o2.charAt(i);
            if (c1 != c2) {
              return c1 - c2;
            }
          }
          return l1 - l2;
        } catch (final NullPointerException npe) {
          CheckArg.notNull(npe, "o2", o2);
          throw npe;
        }
      }
    }

    @Override
    CharSequenceComparator singletonInstance () {
      return INSTANCE;
    }
  }

  private static final class ClassMemberComparator extends AbstractSingletonComparator<Member> {
    private static final long serialVersionUID = -7527211360817278058L;

    private static final ClassMemberComparator INSTANCE = new ClassMemberComparator();

    @Override
    public int compare (final Member o1, final Member o2) {
      try {
        return o1.getName().compareTo(o2.getName());
      } catch (final NullPointerException npe) {
        CheckArg.notNull(npe, "o1", o1);
        CheckArg.notNull(npe, "o2", o2);
        throw npe;
      }
    }

    @Override
    ClassMemberComparator singletonInstance () {
      return INSTANCE;
    }
  }

  private Compare () {
    throw new AssertionError("not allowed");
  }
}
