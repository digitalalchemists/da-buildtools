/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.lib.idxrsc;


import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

import ch.dals.buildtools.lib.util.CheckArg;
import ch.dals.buildtools.lib.util.DirectoryStreamFilters;
import ch.dals.buildtools.lib.util.properties.PropertyReader;
import ch.dals.buildtools.lib.util.properties.PropertyWriter;


/**
 * TODO [javadoc]: type IndexedResourceRootProcessor
 *
 * @author zisch
 */
public class IndexedResourcesGenerator {

  private static final Path[] EMPTY_PATHS = {};

  private final List<Path> mRootDirectories;

  private final boolean mIncludeSubdirectories;

  private final DirectoryStream.Filter<? super Path> mFilter;

  private final Path mIndexFile;

  /**
   * Creates a generator indexing the specified root directory and writing the index to a file in the same directory
   * with the {@linkplain IndexedResources#DEFAULT_INDEX_NAME default name}.
   *
   * @param rootDirectory the root directory to index
   * @param includeSubdirectories if subdirectories should be indexed recursively
   * @param filter a custom filter to exclude arbitrary files from being indexed
   */
  public IndexedResourcesGenerator (final Path rootDirectory, final boolean includeSubdirectories,
          final DirectoryStream.Filter<? super Path> filter) {
    this(Collections.singleton(CheckArg.notNull("rootDirectory", rootDirectory)), includeSubdirectories, filter);
  }

  /**
   * Creates a generator indexing the specified root directory and writing the index to the specified {@code indexFile}.
   *
   * @param rootDirectory the root directory to index
   * @param includeSubdirectories if subdirectories should be indexed recursively
   * @param filter a custom filter to exclude arbitrary files from being indexed
   * @param indexFile the path of the index file; this may be a filename or path relative to the specified
   *          {@code rootDirectory}, or an absolute path to file (which may or may not exist) in an existing directory;
   *          an existing file is overwritten with the new index
   */
  public IndexedResourcesGenerator (final Path rootDirectory, final boolean includeSubdirectories,
          final DirectoryStream.Filter<? super Path> filter, final CharSequence indexFile) {
    this(Collections.singleton(CheckArg.notNull("rootDirectory", rootDirectory)), includeSubdirectories, filter,
            indexFile);
  }

  /**
   * Creates a generator indexing the specified root directory and writing the index to the specified {@code indexFile}.
   *
   * @param rootDirectory the root directory to index
   * @param includeSubdirectories if subdirectories should be indexed recursively
   * @param filter a custom filter to exclude arbitrary files from being indexed
   * @param indexFile the path of the index file; this may be a filename or path relative to the specified
   *          {@code rootDirectory}, or an absolute path to file (which may or may not exist) in an existing directory;
   *          an existing file is overwritten with the new index
   */
  public IndexedResourcesGenerator (final Path rootDirectory, final boolean includeSubdirectories,
          final DirectoryStream.Filter<? super Path> filter, final Path indexFile) {
    this(Collections.singleton(CheckArg.notNull("rootDirectory", rootDirectory)), includeSubdirectories, filter,
            indexFile);
  }

  /**
   * Creates a generator indexing all of the specified root directories and writing the index to a file in the same
   * directory with the {@linkplain IndexedResources#DEFAULT_INDEX_NAME default name}.
   *
   * @param rootDirectories the root directories to index
   * @param includeSubdirectories if subdirectories should be indexed recursively
   * @param filter a custom filter to exclude arbitrary files from being indexed
   */
  public IndexedResourcesGenerator (final Collection<? extends Path> rootDirectories,
          final boolean includeSubdirectories, final DirectoryStream.Filter<? super Path> filter) {
    this(rootDirectories, includeSubdirectories, filter, (Path) null);
  }

  /**
   * Creates a generator indexing all of the specified root directories and writing the index to the specified
   * {@code indexFile}.
   *
   * @param rootDirectories the root directories to index
   * @param includeSubdirectories if subdirectories should be indexed recursively
   * @param filter a custom filter to exclude arbitrary files from being indexed
   * @param indexFile the path of the index file; this may be a filename or path relative to the specified
   *          {@code rootDirectory}, or an absolute path to file (which may or may not exist) in an existing directory;
   *          an existing file is overwritten with the new index
   */
  public IndexedResourcesGenerator (final Collection<? extends Path> rootDirectories,
          final boolean includeSubdirectories, final DirectoryStream.Filter<? super Path> filter,
          final CharSequence indexFile) {
    this(rootDirectories, includeSubdirectories, filter, indexFile == null ? null : Paths.get(CheckArg.notEmpty(
            "indexFile", indexFile).toString()));
  }

  /**
   * Creates a generator indexing all of the specified root directories and writing the index to the specified
   * {@code indexFile}.
   *
   * @param rootDirectories the root directories to index
   * @param includeSubdirectories if subdirectories should be indexed recursively
   * @param filter a custom filter to exclude arbitrary files from being indexed
   * @param indexFile the path of the index file; this may be a filename or path relative to the specified
   *          {@code rootDirectory}, or an absolute path to file (which may or may not exist) in an existing directory;
   *          an existing file is overwritten with the new index
   */
  public IndexedResourcesGenerator (final Collection<? extends Path> rootDirectories,
          final boolean includeSubdirectories, final DirectoryStream.Filter<? super Path> filter, final Path indexFile) {
    CheckArg.notEmpty("rootDirectories", rootDirectories);
    CheckArg.noNulls("rootDirectories", rootDirectories);
    mRootDirectories = Collections.unmodifiableList(Arrays.asList(rootDirectories.toArray(EMPTY_PATHS)));
    mIncludeSubdirectories = includeSubdirectories;
    if (indexFile == null) {
      mIndexFile = mRootDirectories.get(0).resolve(IndexedResources.DEFAULT_INDEX_NAME);
    } else {
      mIndexFile = mRootDirectories.get(0).resolve(indexFile);
    }
    final DirectoryStream.Filter<? super Path> exclIndexFile = DirectoryStreamFilters.not(DirectoryStreamFilters
            .sameFilePath(mIndexFile));
    if (filter == null) {
      mFilter = exclIndexFile;
    } else {
      mFilter = DirectoryStreamFilters.allOf(filter, exclIndexFile);
    }
  }

  /**
   * Checks if the index generated by this generator is up to date.
   *
   * @return {@code true} if the index generated by this generator is up to date, {@code false} otherwise
   *
   * @throws IOException in case of I/O errors
   */
  public boolean indexUpToDate () throws IOException {
    if (Files.exists(mIndexFile)) {
      final TreeMap<String, Void> currentIndex = readIndexForDirectories();
      final TreeMap<String, String> storedIndex = PropertyReader.readProperties(mIndexFile);
      return currentIndex.keySet().equals(storedIndex.keySet());
    } else {
      return false;
    }
  }

  /**
   * (Re)generates the index.
   *
   * @return the number of indexed files
   *
   * @throws IOException in case of I/O errors
   */
  public int generateIndex () throws IOException {
    final TreeMap<String, Void> indexProperties = readIndexForDirectories();
    PropertyWriter.writeProperties(null, indexProperties, mIndexFile);
    return indexProperties.size();
  }

  private TreeMap<String, Void> readIndexForDirectories () throws IOException {
    final TreeMap<String, Void> result = new TreeMap<>();
    for (final Path dir : mRootDirectories) {
      readIndexForDirectory("", dir, mIncludeSubdirectories, mFilter, result);
    }
    return result;
  }

  private static void readIndexForDirectory (final String prefix, final Path dir, final boolean includeSubdirectories,
          final DirectoryStream.Filter<? super Path> filter, final TreeMap<String, Void> result) throws IOException {
    try (final DirectoryStream<Path> ds = Files.newDirectoryStream(dir, filter)) {
      for (final Path p : ds) {
        final String fn = p.getFileName().toString();
        if (includeSubdirectories && Files.isDirectory(p)) {
          readIndexForDirectory(prefix + p.getFileName() + "/", p, includeSubdirectories, filter, result);
        }
        result.put(prefix + fn, null);
      }
    }
  }
}
