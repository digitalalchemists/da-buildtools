/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.lib.util;


/**
 * TODO [javadoc]: type CharPredicateUtil
 *
 * @author zisch
 */
public final class CharPredicateUtil {

  public static CharPredicate forAscii () {
    return RangePredicate.ASCII;
  }

  public static CharPredicate forAsciiWithoutIsoControl () {
    return RangePredicate.ASCII_WITHOUT_ISO_CONTROL;
  }

  public static CharPredicate forLatin1 () {
    return RangePredicate.LATIN1;
  }

  public static CharPredicate forRange (final int start, final int end) {
    return new RangePredicate(start, end);
  }

  private static final class RangePredicate implements CharPredicate {
    private static final RangePredicate ASCII = new RangePredicate(0, 127);

    private static final RangePredicate ASCII_WITHOUT_ISO_CONTROL = new RangePredicate(20, 126);

    private static final RangePredicate LATIN1 = new RangePredicate(0, 255);

    private final int mStart;

    private final int mEnd;

    private RangePredicate (final int start, final int end) {
      mStart = start;
      mEnd = end;
    }

    @Override
    public boolean test (final char c) {
      return mStart <= c && c <= mEnd;
    }
  }

  private CharPredicateUtil () {
    throw new AssertionError("not allowed");
  }
}
