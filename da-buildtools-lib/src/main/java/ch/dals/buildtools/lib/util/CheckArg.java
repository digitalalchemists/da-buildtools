/*
 * This file is part of the DA Secure Communication Framework (SCF) project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA SCF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA SCF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA SCF.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.lib.util;


import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * A utility class which helps to conveniently assert characteristics of method arguments, supporting a defensive coding
 * style.
 * <p>
 * Note that the term "assert" as it is used in the documentation of this class has no relation to the Java language
 * {@code assert} statement. The Java {@code assert} statement is used for <strong>optional</strong> assertions of
 * internal pre- and postconditions in private or package scope. If such an assertion fails, the {@code assert}
 * statement will throw an {@link AssertionError}. At runtime assertions are disabled by default and can be enabled
 * using the {@code -ea} option (either globally or just for specific packages or classes).
 * <p>
 * The "assertion" methods in this class however cannot (and are not intended to) ever be disabled, rather they will be
 * executed whenever the calling code is executed. The intended use is to check the validity of Java method arguments,
 * typically such of protected or public methods.
 * <p>
 * Most of the methods will throw an {@link IllegalArgumentException} if an assertion fails, though there are
 * exceptions. For example, checks for {@code null} values will throw {@link NullPointerException}s while there are
 * dedicated <code>index&hellip;</code> methods to check for indizes (typically into array-like structures) which throw
 * {@link IndexOutOfBoundsException}s.
 * <p>
 * It is very important to understand that failed assertion are always caused by a bug in the application code,
 * typically (though not always) in the code calling the method which executes the check. Consequentially the exceptions
 * thrown by {@code CheckArg} upon failed assertions are normally subclasses {@link RuntimeException}, though there
 * might be special cases throwing checked exceptions.
 * <p>
 * One of the main goals of this class is to generate consistent, meaningfull, non-localized exception messages, which
 * should provide debugging developers (the intended audience of these messages) with all the avaible information about
 * the "offending" values at the point of the assertion. Apart from the value(s) itself, most methods will expect the
 * specification of one (or multiple) "label(s)" which shold "describe" the involved (method) parameter(s). Normally
 * such a "label" will simply be the variable name used for the corresponding parameter in the (public) method
 * signature.
 * <p>
 * <h3>How to Use this Class</h3>
 * <p>
 * A typical usage of this class would look like this:
 *
 * <pre>
 * /**
 *  * Describes the specified value.
 *  *
 *  * {@code @param} value the value to describe (not null)
 *  *
 *  * {@code @return} a description of the specified value
 *  *
 *  * {@code @throws} NullPointerException in case the specified value is null
 *  &#42;/
 * public String describeValue (final Object value) {
 *   CheckArg.notNull(&quot;value&quot;, value);
 *   return &quot;We got the value '&quot; + value + &quot;'.&quot;;
 * }
 * </pre>
 * <p>
 * In the previous example, the use of {@code CheckArg} (assuming that {@code someParam == null} is supposed to be
 * illegal) or a similar "manual" check is mandatory, since the {@code +} operator would happily accept {@code null}
 * values (and convert these to {@code "null"} strings). While not strictly necessary, it is in general recommended to
 * also use {@code CheckArg} in cases when an invalid value would trigger an exception later on anyway:
 *
 * <pre>
 * /**
 *  * Describes the specified value.
 *  *
 *  * {@code @param} value the value to describe (not null)
 *  *
 *  * {@code @return} a description of the specified value
 *  *
 *  * {@code @throws} NullPointerException in case the specified value is null
 *  &#42;/
 * public String describeValue (final Object value) {
 *   CheckArg.notNull(&quot;value&quot;, value);
 *   return &quot;We got the value '&quot; + value.toString() + &quot;'.&quot;;
 * }
 * </pre>
 * <p>
 * This clearly reflects the Javadoc specification of the method in the implementation code and as a bonus enforces
 * consistent exception types and messages throughout all code using the {@code CheckArg} class. Only in cases where the
 * additional and unconditional (though very cheap) checks prove to actually have a negative impact on the performance
 * of an application, one should consider the following pattern, which I call "exception triggered assertion":
 *
 * <pre>
 * /**
 *  * Describes the specified value.
 *  *
 *  * {@code @param} value the value to describe (not null)
 *  *
 *  * {@code @return} a description of the specified value
 *  *
 *  * {@code @throws} NullPointerException in case the specified value is null
 *  &#42;/
 * public String describeValue (final Object value) {
 *   try {
 *     return &quot;We got the value '&quot; + value.toString() + &quot;'.&quot;;
 *   } catch (final NullPointerException exc) {
 *     CheckArg.notNull(exc, &quot;value&quot;, value);
 *     throw exc;
 *   }
 * }
 * </pre>
 * <p>
 * This pattern does not impose any performance penalties (except for the very rare cases of a failed assertion).
 * However, the code is much less readable than the "simple" use presented first, which is therefore preferable in
 * general.
 * <p>
 * Another notable API detail can be seen in the "exception triggered assertion" shown above: All {@code CheckArg}
 * assertion methods will come overloaded with a variant which take a {@link Throwable}
 * {@linkplain Throwable#getCause() "root cause"} parameter to use Javas exception chaining algorithm. Under all normal
 * circumstances this should be used to chain the causing {@code Throwable} whenever applying the
 * "exception triggered assertion" pattern!
 * <p>
 * Most {@code CheckArg} methods will return some value (typically the original input value) which can be used to
 * initialize some variable. This comes especially handy when initializing final (typically constant) fields in
 * constructors because the resulting code is very simple and readable:
 *
 * <pre>
 * private final String mName;
 * 
 * /**
 *  * Constructs a {@code FooBar} instance with a name.
 *  *
 *  * {@code @param} name the name of this instance (not null or empty)
 *  *
 *  * {@code @throws} NullPointerException in case the specified name is null
 *  *
 *  * {@code @throws} IllegalArgumentException in case the specified name is empty
 *  &#42;/
 * public FooBar (final String name) {
 *   mName = CheckArg.notEmpty(&quot;name&quot;, name);
 * }
 * </pre>
 *
 * @author zisch
 */
public final class CheckArg {

  // ---- Null Tests

  /**
   * Asserts that the specified reference is not {@code null}.
   * <p>
   * This is equivalent to {@link #notNull(Throwable, String, Object) notNull(null, label, o)}.
   *
   * @param <T> type of the reference to test
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param o the actual reference to test
   *
   * @return the specified object instance (not {@code null})
   *
   * @throws NullPointerException if and only if {@code o == null}; the exception message will only contain the
   *           specified {@code label}
   */
  public static <T> T notNull (final String label, final T o) {
    return notNull(null, label, o);
  }

  /**
   * Asserts that the specified reference is not {@code null}.
   *
   * @param <T> type of the reference to test
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param o the actual reference to test
   *
   * @return the specified object instance {@code o} (never {@code null})
   *
   * @throws NullPointerException if and only if {@code o == null}
   */
  public static <T> T notNull (final Throwable cause, final String label, final T o) {
    if (o == null) {
      throw createNPE(l(label), cause);
    }
    return o;
  }

  /**
   * Asserts that the specified reference is not {@code null} and that an iteration returned by the {@link Iterable}
   * contains no {@code null} elements.
   * <p>
   * This is equivalent to {@link #noNulls(Throwable, String, Iterable) noNulls(null, label, c)}.
   *
   * @param <T> type of the reference to test
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param c the {@code Iterable} to test (usually some collection)
   *
   * @return the specified {@code Iterable} instance (not {@code null} and not containing any {@code null} elements)
   *
   * @throws NullPointerException if and only if {@code c == null} or for the {@code i}'th element {@code e = c[i]} of
   *           {@code c.iterator()} {@code e == null}; the exception message will only contain the specified
   *           {@code label} (for {@code c == null}) or the string {@code "label[i]"} where {@code label} is the
   *           specified {@code label} and {@code i} is the index of the offending element in {@code c.iterator()}
   */
  public static <T extends Iterable<?>> T noNulls (final String label, final T c) {
    return noNulls(null, label, c);
  }

  /**
   * Asserts that the specified reference is not {@code null} and that an iteration returned by the {@link Iterable}
   * contains no {@code null} elements.
   *
   * @param <T> type of the reference to test
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param c the {@code Iterable} to test (usually some collection)
   *
   * @return the specified {@code Iterable} instance (not {@code null} and not containing any {@code null} elements)
   *
   * @throws NullPointerException if and only if {@code c == null} or for the {@code i}'th element {@code e = c[i]} of
   *           {@code c.iterator()} {@code e == null}; the exception message will only contain the specified
   *           {@code label} (for {@code c == null}) or the string {@code "label[i]"} where {@code label} is the
   *           specified {@code label} and {@code i} is the index of the offending element in {@code c.iterator()}
   */
  public static <T extends Iterable<?>> T noNulls (final Throwable cause, final String label, final T c) {
    notNull(cause, label, c);
    internalNoNulls(cause, label, c);
    return c;
  }

  private static void internalNoNulls (final Throwable cause, final String label, final Iterable<?> c) {
    int i = 0;
    for (final Object o : c) {
      if (o == null) {
        final String msg = l(label) + "[" + i + "]";
        throw createNPE(msg, cause);
      }
      i++;
    }
  }

  /**
   * Asserts that the specified reference is not {@code null} and that the array does not contain any {@code null}
   * elements.
   * <p>
   * This is equivalent to {@link #noNulls(Throwable, String, Object[]) noNulls(null, label, arr)}.
   *
   * @param <T> type of the reference to test
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param arr the array to test
   *
   * @return the specified array instance (not {@code null} and not containing any {@code null} elements)
   *
   * @throws NullPointerException if and only if {@code arr == null} or {@code arr[i] == null} for any valid array index
   *           {@code i} of {@code arr}; the exception message will only contain the specified {@code label} (for
   *           {@code arr == null}) or the string {@code "label[i]"} where {@code label} is the specified {@code label}
   *           and {@code i} is the index of the offending element in {@code arr}
   */
  public static <T> T[] noNulls (final String label, final T[] arr) {
    return noNulls(null, label, arr);
  }

  /**
   * Asserts that the specified reference is not {@code null} and that the array does not contain any {@code null}
   * elements.
   *
   * @param <T> type of the reference to test
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param arr the array to test
   *
   * @return the specified array instance (not {@code null} and not containing any {@code null} elements)
   *
   * @throws NullPointerException if and only if {@code arr == null} or {@code arr[i] == null} for any valid array index
   *           {@code i} of {@code arr}; the exception message will only contain the specified {@code label} (for
   *           {@code arr == null}) or the string {@code "label[i]"} where {@code label} is the specified {@code label}
   *           and {@code i} is the index of the offending element in {@code arr}
   */
  public static <T> T[] noNulls (final Throwable cause, final String label, final T[] arr) {
    notNull(cause, label, arr);
    internalNoNulls(cause, label, arr);
    return arr;
  }

  private static void internalNoNulls (final Throwable cause, final String label, final Object[] c) {
    int i = 0;
    for (final Object o : c) {
      if (o == null) {
        final String msg = l(label) + "[" + i + "]";
        throw createNPE(msg, cause);
      }
      i++;
    }
  }

  // TODO: deepNoNulls(...)

  // ---- Empty Tests

  /**
   * Asserts that the specified {@link CharSequence} is neither {@code null} nor {@linkplain String#isEmpty() empty}.
   * <p>
   * This is equivalent to {@link #notEmpty(Throwable, String, CharSequence) notEmpty(null, label, cs)}.
   *
   * @param <T> type of the reference to test
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param cs the {@code CharSequence} reference to test
   *
   * @return the specified {@code CharSequence} (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code cs == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code cs} is {@linkplain String#isEmpty() empty}, that is <code>
   *           {@linkplain CharSequence#length() cs.length()} == 0</code>; the message will be
   *           {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static <T extends CharSequence> T notEmpty (final String label, final T cs) {
    return notEmpty(null, label, cs);
  }

  /**
   * Asserts that the specified {@link CharSequence} is neither {@code null} nor {@linkplain String#isEmpty() empty}.
   *
   * @param <T> type of the reference to test
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param cs the {@code CharSequence} reference to test
   *
   * @return the specified {@code CharSequence} (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code cs == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code cs} is {@linkplain String#isEmpty() empty}, that is <code>
   *           {@linkplain CharSequence#length() cs.length()} == 0</code>; the message will be
   *           {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static <T extends CharSequence> T notEmpty (final Throwable cause, final String label, final T cs) {
    notNull(cause, label, cs);
    if (cs.length() == 0) {
      final String msg = l(label) + " must not be empty";
      throw createIAE(msg, cause);
    }
    return cs;
  }

  /**
   * Asserts that the specified {@link Collection} is neither {@code null} nor {@linkplain Collection#isEmpty() empty}.
   * <p>
   * This is equivalent to {@link #notEmpty(Throwable, String, Collection) notEmpty(null, label, c)}.
   *
   * @param <T> type of the reference to test
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param c the {@code Collection} reference to test
   *
   * @return the specified {@code Collection} (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code c == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code c} is {@linkplain Collection#isEmpty() empty}, that is
   *           <code> {@linkplain Collection#size() c.size()} == 0</code>; the message will be
   *           {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static <T extends Collection<?>> T notEmpty (final String label, final T c) {
    return notEmpty(null, label, c);
  }

  /**
   * Asserts that the specified {@link Collection} is neither {@code null} nor {@linkplain Collection#isEmpty() empty}.
   *
   * @param <T> type of the reference to test
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param c the {@code Collection} reference to test
   *
   * @return the specified {@code Collection} (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code c == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code c} is {@linkplain Collection#isEmpty() empty}, that is
   *           <code>
   *           {@linkplain Collection#size() c.size()} == 0</code>; the message will be
   *           {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static <T extends Collection<?>> T notEmpty (final Throwable cause, final String label, final T c) {
    notNull(cause, label, c);
    internalNotEmpty(cause, label, c);
    return c;
  }

  private static void internalNotEmpty (final Throwable cause, final String label, final Collection<?> c) {
    if (c.isEmpty()) {
      final String msg = l(label) + " must not be empty";
      throw createIAE(msg, cause);
    }
  }

  /**
   * Asserts that the specified {@link Map} is neither {@code null} nor {@linkplain Map#isEmpty() empty}.
   * <p>
   * This is equivalent to {@link #notEmpty(Throwable, String, Map) notEmpty(null, label, m)}.
   *
   * @param <T> type of the reference to test
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param m the {@code Map} reference to test
   *
   * @return the specified {@code Map} (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code m == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code m} is {@linkplain Map#isEmpty() empty}, that is <code>
   *           {@linkplain Map#size() m.size()} == 0</code>; the message will be {@code "label must not be empty"} where
   *           {@code label} is the specified {@code label}
   */
  public static <T extends Map<?, ?>> T notEmpty (final String label, final T m) {
    return notEmpty(null, label, m);
  }

  /**
   * Asserts that the specified {@link Map} is neither {@code null} nor {@linkplain Map#isEmpty() empty}.
   *
   * @param <T> type of the reference to test
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param m the {@code Map} reference to test
   *
   * @return the specified {@code Map} (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code m == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code m} is {@linkplain Map#isEmpty() empty}, that is
   *           <code> {@linkplain Map#size() m.size()} == 0</code>; the message will be
   *           {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static <T extends Map<?, ?>> T notEmpty (final Throwable cause, final String label, final T m) {
    notNull(cause, label, m);
    internalNotEmpty(cause, label, m);
    return m;
  }

  private static void internalNotEmpty (final Throwable cause, final String label, final Map<?, ?> m) {
    if (m.isEmpty()) {
      final String msg = l(label) + " must not be empty";
      throw createIAE(msg, cause);
    }
  }

  /**
   * Asserts that the specified array is neither {@code null} nor empty (that is {@code arr.length == 0}).
   * <p>
   * This is equivalent to {@link #notEmpty(Throwable, String, byte[]) notEmpty(null, label, arr)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param arr the array reference to test
   *
   * @return the specified array (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code arr == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code arr} is empty, that is <code>arr.length == 0</code>; the
   *           message will be {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static byte[] notEmpty (final String label, final byte[] arr) {
    return notEmpty(null, label, arr);
  }

  /**
   * Asserts that the specified array is neither {@code null} nor empty (that is {@code arr.length == 0}).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param arr the array reference to test
   *
   * @return the specified array (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code arr == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code arr} is empty, that is <code>arr.length == 0</code>; the
   *           message will be {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static byte[] notEmpty (final Throwable cause, final String label, final byte[] arr) {
    notNull(cause, label, arr);
    if (arr.length == 0) {
      final String msg = l(label) + " must not be empty";
      throw createIAE(msg, cause);
    }
    return arr;
  }

  /**
   * Asserts that the specified array is neither {@code null} nor empty (that is {@code arr.length == 0}).
   * <p>
   * This is equivalent to {@link #notEmpty(Throwable, String, short[]) notEmpty(null, label, arr)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param arr the array reference to test
   *
   * @return the specified array (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code arr == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code arr} is empty, that is <code>arr.length == 0</code>; the
   *           message will be {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static short[] notEmpty (final String label, final short[] arr) {
    return notEmpty(null, label, arr);
  }

  /**
   * Asserts that the specified array is neither {@code null} nor empty (that is {@code arr.length == 0}).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param arr the array reference to test
   *
   * @return the specified array (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code arr == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code arr} is empty, that is <code>arr.length == 0</code>; the
   *           message will be {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static short[] notEmpty (final Throwable cause, final String label, final short[] arr) {
    notNull(cause, label, arr);
    if (arr.length == 0) {
      final String msg = l(label) + " must not be empty";
      throw createIAE(msg, cause);
    }
    return arr;
  }

  /**
   * Asserts that the specified array is neither {@code null} nor empty (that is {@code arr.length == 0}).
   * <p>
   * This is equivalent to {@link #notEmpty(Throwable, String, int[]) notEmpty(null, label, arr)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param arr the array reference to test
   *
   * @return the specified array (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code arr == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code arr} is empty, that is <code>arr.length == 0</code>; the
   *           message will be {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static int[] notEmpty (final String label, final int[] arr) {
    return notEmpty(null, label, arr);
  }

  /**
   * Asserts that the specified array is neither {@code null} nor empty (that is {@code arr.length == 0}).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param arr the array reference to test
   *
   * @return the specified array (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code arr == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code arr} is empty, that is <code>arr.length == 0</code>; the
   *           message will be {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static int[] notEmpty (final Throwable cause, final String label, final int[] arr) {
    notNull(cause, label, arr);
    if (arr.length == 0) {
      final String msg = l(label) + " must not be empty";
      throw createIAE(msg, cause);
    }
    return arr;
  }

  /**
   * Asserts that the specified array is neither {@code null} nor empty (that is {@code arr.length == 0}).
   * <p>
   * This is equivalent to {@link #notEmpty(Throwable, String, long[]) notEmpty(null, label, arr)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param arr the array reference to test
   *
   * @return the specified array (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code arr == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code arr} is empty, that is <code>arr.length == 0</code>; the
   *           message will be {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static long[] notEmpty (final String label, final long[] arr) {
    return notEmpty(null, label, arr);
  }

  /**
   * Asserts that the specified array is neither {@code null} nor empty (that is {@code arr.length == 0}).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param arr the array reference to test
   *
   * @return the specified array (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code arr == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code arr} is empty, that is <code>arr.length == 0</code>; the
   *           message will be {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static long[] notEmpty (final Throwable cause, final String label, final long[] arr) {
    notNull(cause, label, arr);
    if (arr.length == 0) {
      final String msg = l(label) + " must not be empty";
      throw createIAE(msg, cause);
    }
    return arr;
  }

  /**
   * Asserts that the specified array is neither {@code null} nor empty (that is {@code arr.length == 0}).
   * <p>
   * This is equivalent to {@link #notEmpty(Throwable, String, float[]) notEmpty(null, label, arr)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param arr the array reference to test
   *
   * @return the specified array (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code arr == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code arr} is empty, that is <code>arr.length == 0</code>; the
   *           message will be {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static float[] notEmpty (final String label, final float[] arr) {
    return notEmpty(null, label, arr);
  }

  /**
   * Asserts that the specified array is neither {@code null} nor empty (that is {@code arr.length == 0}).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param arr the array reference to test
   *
   * @return the specified array (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code arr == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code arr} is empty, that is <code>arr.length == 0</code>; the
   *           message will be {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static float[] notEmpty (final Throwable cause, final String label, final float[] arr) {
    notNull(cause, label, arr);
    if (arr.length == 0) {
      final String msg = l(label) + " must not be empty";
      throw createIAE(msg, cause);
    }
    return arr;
  }

  /**
   * Asserts that the specified array is neither {@code null} nor empty (that is {@code arr.length == 0}).
   * <p>
   * This is equivalent to {@link #notEmpty(Throwable, String, double[]) notEmpty(null, label, arr)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param arr the array reference to test
   *
   * @return the specified array (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code arr == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code arr} is empty, that is <code>arr.length == 0</code>; the
   *           message will be {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static double[] notEmpty (final String label, final double[] arr) {
    return notEmpty(null, label, arr);
  }

  /**
   * Asserts that the specified array is neither {@code null} nor empty (that is {@code arr.length == 0}).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param arr the array reference to test
   *
   * @return the specified array (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code arr == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code arr} is empty, that is <code>arr.length == 0</code>; the
   *           message will be {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static double[] notEmpty (final Throwable cause, final String label, final double[] arr) {
    notNull(cause, label, arr);
    if (arr.length == 0) {
      final String msg = l(label) + " must not be empty";
      throw createIAE(msg, cause);
    }
    return arr;
  }

  /**
   * Asserts that the specified array is neither {@code null} nor empty (that is {@code arr.length == 0}).
   * <p>
   * This is equivalent to {@link #notEmpty(Throwable, String, char[]) notEmpty(null, label, arr)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param arr the array reference to test
   *
   * @return the specified array (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code arr == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code arr} is empty, that is <code>arr.length == 0</code>; the
   *           message will be {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static char[] notEmpty (final String label, final char[] arr) {
    return notEmpty(null, label, arr);
  }

  /**
   * Asserts that the specified array is neither {@code null} nor empty (that is {@code arr.length == 0}).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param arr the array reference to test
   *
   * @return the specified array (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code arr == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code arr} is empty, that is <code>arr.length == 0</code>; the
   *           message will be {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static char[] notEmpty (final Throwable cause, final String label, final char[] arr) {
    notNull(cause, label, arr);
    if (arr.length == 0) {
      final String msg = l(label) + " must not be empty";
      throw createIAE(msg, cause);
    }
    return arr;
  }

  /**
   * Asserts that the specified array is neither {@code null} nor empty (that is {@code arr.length == 0}).
   * <p>
   * This is equivalent to {@link #notEmpty(Throwable, String, boolean[]) notEmpty(null, label, arr)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param arr the array reference to test
   *
   * @return the specified array (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code arr == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code arr} is empty, that is <code>arr.length == 0</code>; the
   *           message will be {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static boolean[] notEmpty (final String label, final boolean[] arr) {
    return notEmpty(null, label, arr);
  }

  /**
   * Asserts that the specified array is neither {@code null} nor empty (that is {@code arr.length == 0}).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param arr the array reference to test
   *
   * @return the specified array (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code arr == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code arr} is empty, that is <code>arr.length == 0</code>; the
   *           message will be {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static boolean[] notEmpty (final Throwable cause, final String label, final boolean[] arr) {
    notNull(cause, label, arr);
    if (arr.length == 0) {
      final String msg = l(label) + " must not be empty";
      throw createIAE(msg, cause);
    }
    return arr;
  }

  /**
   * Asserts that the specified array is neither {@code null} nor empty (that is {@code arr.length == 0}).
   * <p>
   * This is equivalent to {@link #notEmpty(Throwable, String, Object[]) notEmpty(null, label, arr)}.
   *
   * @param <T> the array element type
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param arr the array reference to test
   *
   * @return the specified array (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code arr == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code arr} is empty, that is <code>arr.length == 0</code>; the
   *           message will be {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static <T> T[] notEmpty (final String label, final T[] arr) {
    return notEmpty(null, label, arr);
  }

  /**
   * Asserts that the specified array is neither {@code null} nor empty (that is {@code arr.length == 0}).
   *
   * @param <T> the array element type
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param arr the array reference to test
   *
   * @return the specified array (which is neither {@code null} nor empty)
   *
   * @throws NullPointerException if and only if {@code arr == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code arr} is empty, that is <code>arr.length == 0</code>; the
   *           message will be {@code "label must not be empty"} where {@code label} is the specified {@code label}
   */
  public static <T> T[] notEmpty (final Throwable cause, final String label, final T[] arr) {
    notNull(cause, label, arr);
    internalNotEmpty(cause, label, arr);
    return arr;
  }

  private static void internalNotEmpty (final Throwable cause, final String label, final Object[] arr) {
    if (arr.length == 0) {
      final String msg = l(label) + " must not be empty";
      throw createIAE(msg, cause);
    }
  }

  // ---- Misc CharSequence Tests

  public static <T extends CharSequence> T isJavaName (final String label, final T cs) {
    return isJavaName(null, label, cs);
  }

  public static <T extends CharSequence> T isJavaName (final Throwable cause, final String label, final T cs) {
    internalCheckJavaIdentifierOrName(cause, label, cs, true);
    return cs;
  }

  /**
   * Asserts that the specified {@code CharSequence} represents a valid Java identifier according to the production
   * {@code IdentifierChars} in <a href="http://docs.oracle.com/javase/specs/jls/se7/html/jls-3.html#jls-3.8">JLS
   * 3.8</a>. A valid Java identifier is a non-null, non-empty string which starts with a character for which
   * {@link Character#isJavaIdentifierStart(char)} is {@code true} followed by any number of characters for which
   * {@link Character#isJavaIdentifierPart(char)} is {@code true}. (This method does not take keywords or reserved
   * literals into account, so it checks the {@code IdentifierChars} production rather than the {@code Identifier}
   * production of the JLS!)
   * <p>
   * This is equivalent to {@link #isJavaIdentifier(Throwable, String, CharSequence) isJavaIdentifier(null, label, cs)}.
   *
   * @param <T> the {@code CharSequence} type
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param cs the {@code CharSequence} to test
   *
   * @return the specified {@code CharSequence} (which is not {@code null} and contains a valid Java identifier)
   *
   * @throws NullPointerException if and only if {@code cs == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code cs} is empty (with the message
   *           {@code "label must not be empty"} where {@code label} is the specified {@code label}) or contains some
   *           invalid {@linkplain Character#isJavaIdentifierStart(char) start character} at index 0 or some invalid
   *           {@linkplain Character#isJavaIdentifierPart(char) part character} at some higher index (with the message
   *           {@code "label is not a valid Java identifier; invalid character 'c' at i: \"cs\""}, where {@code label}
   *           is the specified {@code label}, {@code c} is the offending character and {@code cs} is the specified
   *           {@code CharSequence})
   *
   * @see <a href="http://docs.oracle.com/javase/specs/jls/se7/html/jls-3.html#jls-3.8">Java Language Specification SE7,
   *      3.8 Identifiers</a>
   */
  public static <T extends CharSequence> T isJavaIdentifier (final String label, final T cs) {
    return isJavaIdentifier(null, label, cs);
  }

  /**
   * Asserts that the specified {@code CharSequence} represents a valid Java identifier according to the production
   * {@code IdentifierChars} in <a href="http://docs.oracle.com/javase/specs/jls/se7/html/jls-3.html#jls-3.8">JLS
   * 3.8</a>. A valid Java identifier is a non-null, non-empty string which starts with a character for which
   * {@link Character#isJavaIdentifierStart(char)} is {@code true} followed by any number of characters for which
   * {@link Character#isJavaIdentifierPart(char)} is {@code true}. (This method does not take keywords or reserved
   * literals into account, so it checks the {@code IdentifierChars} production rather than the {@code Identifier}
   * production of the JLS!)
   *
   * @param <T> the {@code CharSequence} type
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param cs the {@code CharSequence} to test
   *
   * @return the specified {@code CharSequence} (which is not {@code null} and contains a valid Java identifier)
   *
   * @throws NullPointerException if and only if {@code cs == null}; the exception message will only contain the
   *           specified {@code label}
   * @throws IllegalArgumentException if and only if {@code cs} is empty (with the message
   *           {@code "label must not be empty"} where {@code label} is the specified {@code label}) or contains some
   *           invalid {@linkplain Character#isJavaIdentifierStart(char) start character} at index 0 or some invalid
   *           {@linkplain Character#isJavaIdentifierPart(char) part character} at some higher index (with the message
   *           {@code "label is not a valid Java identifier; invalid character 'c' at i: \"cs\""}, where {@code label}
   *           is the specified {@code label}, {@code c} is the offending character and {@code cs} is the specified
   *           {@code CharSequence})
   *
   * @see <a href="http://docs.oracle.com/javase/specs/jls/se7/html/jls-3.html#jls-3.8">Java Language Specification SE7,
   *      3.8 Identifiers</a>
   */
  public static <T extends CharSequence> T isJavaIdentifier (final Throwable cause, final String label, final T cs) {
    internalCheckJavaIdentifierOrName(cause, label, cs, false);
    return cs;
  }

  private static void internalCheckJavaIdentifierOrName (final Throwable cause, final String label,
          final CharSequence cs, final boolean checkName) {
    notEmpty(cause, label, cs);
    final int len = cs.length();
    boolean startNamePart = true;
    for (int i = 0; i < len; i++) {
      final int cp = Character.codePointAt(cs, i);
      if (checkName && cp == '.') {
        if (startNamePart) {
          final String msg;
          if (i > 0) {
            msg = String.format("%s is not a valid Java name; empty name part at %d: \"%s\"", l(label), i);
          } else {
            msg = String.format("%s is not a valid Java name; invalid leading dot: \"%s\"", l(label), i);
          }
          throw createIAE(msg, cause);
        } else {
          startNamePart = true;
        }
      } else {
        final boolean ok = startNamePart ? Character.isJavaIdentifierStart(cp) : Character.isJavaIdentifierPart(cp);
        if (!ok) {
          final String msg = String.format("%s is not a valid Java %s; invalid code point U+%x at index %d: \"%s\"",
                  l(label), checkName ? "name" : "identifier", cp, i, cs);
          throw createIAE(msg, cause);
        }
        startNamePart = false;
      }
    }
    if (startNamePart) {
      assert checkName;
      final String msg = String.format("%s is not a valid Java name; invalid trailing dot: \"%s\"", l(label));
      throw createIAE(msg, cause);
    }
  }

  /**
   * Asserts that the specified {@code CharSequence} is not {@code null} and {@linkplain Matcher#matches() matches} the
   * specified regular expression pattern, returning the {@code CharSequence} itself.
   * <p>
   * This is equivalent to {@link #matches(Throwable, Pattern, String, CharSequence) matches(null, pattern, label, cs)}.
   *
   * @param <T> the {@code CharSequence} type
   * @param pattern the regexp pattern to test against
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param cs the {@code CharSequence} to test
   *
   * @return the specified {@code CharSequence} (which is not {@code null} and matches the specified regexp pattern)
   *
   * @throws NullPointerException if and only if {@code cs == null} (where the exception message will only contain the
   *           specified {@code label}) or {@code pattern == null} (where the exception message will be
   *           {@code "pattern"})
   * @throws IllegalArgumentException if and only if {@code cs} does not match the specified {@code pattern}; the
   *           message will be {@code "label does not match regular expression 'pattern': \"cs\""} where {@code label}
   *           is the specified {@code label}, {@code pattern} is the specified {@code Pattern} and {@code cs} is the
   *           specified {@code CharSequence}
   *
   * @see #matcher(Pattern, String, CharSequence)
   */
  public static <T extends CharSequence> T matches (final Pattern pattern, final String label, final T cs) {
    return matches(null, pattern, label, cs);
  }

  /**
   * Asserts that the specified {@code CharSequence} is not {@code null} and {@linkplain Matcher#matches() matches} the
   * specified regular expression pattern, returning the {@code CharSequence} itself.
   *
   * @param <T> the {@code CharSequence} type
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param pattern the regexp pattern to test against
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param cs the {@code CharSequence} to test
   *
   * @return the specified {@code CharSequence} (which is not {@code null} and matches the specified regexp pattern)
   *
   * @throws NullPointerException if and only if {@code cs == null} (where the exception message will only contain the
   *           specified {@code label}) or {@code pattern == null} (where the exception message will be
   *           {@code "pattern"})
   * @throws IllegalArgumentException if and only if {@code cs} does not match the specified {@code pattern}; the
   *           message will be {@code "label does not match regular expression 'pattern': \"cs\""} where {@code label}
   *           is the specified {@code label}, {@code pattern} is the specified {@code Pattern} and {@code cs} is the
   *           specified {@code CharSequence}
   *
   * @see #matcher(Throwable, Pattern, String, CharSequence)
   */
  public static <T extends CharSequence> T matches (final Throwable cause, final Pattern pattern, final String label,
          final T cs) {
    matcher(cause, pattern, label, cs);
    return cs;
  }

  /**
   * Asserts that the specified {@code CharSequence} is not {@code null} and {@linkplain Matcher#matches() matches} the
   * specified regular expression pattern, returning the {@link Matcher} which matched the sequence. The returned
   * {@code Matcher} instance can, for example, be used to access the matched groups.
   * <p>
   * This is equivalent to {@link #matcher(Throwable, Pattern, String, CharSequence) matches(null, pattern, label, cs)}.
   *
   * @param pattern the regexp pattern to test against
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param cs the {@code CharSequence} to test
   *
   * @return the {@link Matcher} instance which matched the specified {@code CharSequence}
   *
   * @throws NullPointerException if and only if {@code cs == null} (where the exception message will only contain the
   *           specified {@code label}) or {@code pattern == null} (where the exception message will be
   *           {@code "pattern"})
   * @throws IllegalArgumentException if and only if {@code cs} does not match the specified {@code pattern}; the
   *           message will be {@code "label does not match regular expression 'pattern': \"cs\""} where {@code label}
   *           is the specified {@code label}, {@code pattern} is the specified {@code Pattern} and {@code cs} is the
   *           specified {@code CharSequence}
   *
   * @see #matches(Pattern, String, CharSequence)
   */
  public static Matcher matcher (final Pattern pattern, final String label, final CharSequence cs) {
    return matcher(null, pattern, label, cs);
  }

  /**
   * Asserts that the specified {@code CharSequence} is not {@code null} and {@linkplain Matcher#matches() matches} the
   * specified regular expression pattern, returning the {@link Matcher} which matched the sequence. The returned
   * {@code Matcher} instance can, for example, be used to access the matched groups.
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param pattern the regexp pattern to test against
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param cs the {@code CharSequence} to test
   *
   * @return the {@link Matcher} instance which matched the specified {@code CharSequence}
   *
   * @throws NullPointerException if and only if {@code cs == null} (where the exception message will only contain the
   *           specified {@code label}) or {@code pattern == null} (where the exception message will be
   *           {@code "pattern"})
   * @throws IllegalArgumentException if and only if {@code cs} does not match the specified {@code pattern}; the
   *           message will be {@code "label does not match regular expression 'pattern': \"cs\""} where {@code label}
   *           is the specified {@code label}, {@code pattern} is the specified {@code Pattern} and {@code cs} is the
   *           specified {@code CharSequence}
   *
   * @see #matches(Throwable, Pattern, String, CharSequence)
   */
  public static Matcher matcher (final Throwable cause, final Pattern pattern, final String label, final CharSequence cs) {
    try {
      notNull(cause, label, cs);
      final java.util.regex.Matcher m = pattern.matcher(cs);
      if (!m.matches()) {
        // FIXME: Introduce and use JavaStringUtil to format values!
        throw createIAE(label + " does not match regular expression '" + pattern + "': \"" + cs + "\"", cause);
      }
      return m;
    } catch (final NullPointerException exc) {
      notNull(exc, "pattern", pattern);
      throw exc;
    }
  }

  // ---- Number Tests

  /**
   * Asserts that the specified value is positive ({@code val > 0}).
   * <p>
   * This is equivalent to {@link #isPositive(Throwable, String, int) isPositive(null, label, val)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than 0)
   *
   * @throws IllegalArgumentException if and only if {@code val <= 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static int isPositive (final String label, final int val) {
    return isPositive(null, label, val);
  }

  /**
   * Asserts that the specified value is positive ({@code val > 0}).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than 0)
   *
   * @throws IllegalArgumentException if and only if {@code val <= 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static int isPositive (final Throwable cause, final String label, final int val) {
    if (val <= 0) {
      throw createIAE(label + " must be positive: " + val, cause);
    }
    return val;
  }

  /**
   * Asserts that the specified value is positive ({@code val > 0}).
   * <p>
   * This is equivalent to {@link #isPositive(Throwable, String, long) isPositive(null, label, val)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than 0)
   *
   * @throws IllegalArgumentException if and only if {@code val <= 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static long isPositive (final String label, final long val) {
    return isPositive(null, label, val);
  }

  /**
   * Asserts that the specified value is positive ({@code val > 0}).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than 0)
   *
   * @throws IllegalArgumentException if and only if {@code val <= 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static long isPositive (final Throwable cause, final String label, final long val) {
    if (val <= 0) {
      throw createIAE(label + " must be positive: " + val, cause);
    }
    return val;
  }

  /**
   * Asserts that the specified value is not negative ({@code val >= 0}).
   * <p>
   * This is equivalent to {@link #notNegative(Throwable, String, int) notNegative(null, label, val)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static int notNegative (final String label, final int val) {
    return notNegative(null, label, val);
  }

  /**
   * Asserts that the specified value is not negative ({@code val >= 0}).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static int notNegative (final Throwable cause, final String label, final int val) {
    if (val < 0) {
      throw createIAE(label + " must not be negative: " + val, cause);
    }
    return val;
  }

  /**
   * Asserts that the specified value is not negative ({@code val >= 0}).
   * <p>
   * This is equivalent to {@link #notNegative(Throwable, String, int) notNegative(null, label, val)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static long notNegative (final String label, final long val) {
    return notNegative(null, label, val);
  }

  /**
   * Asserts that the specified value is not negative ({@code val >= 0}).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static long notNegative (final Throwable cause, final String label, final long val) {
    if (val < 0) {
      throw createIAE(label + " must not be negative: " + val, cause);
    }
    return val;
  }

  /**
   * Asserts that the specified value is not negative ({@code val >= 0}).
   * <p>
   * This is equivalent to {@link #notNegative(Throwable, String, int) notNegative(null, label, val)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static float notNegative (final String label, final float val) {
    return notNegative(null, label, val);
  }

  /**
   * Asserts that the specified value is not negative ({@code val >= 0}).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static float notNegative (final Throwable cause, final String label, final float val) {
    // We also consider NaN:
    if (!(val >= 0)) {
      throw createIAE(label + " must not be negative: " + val, cause);
    }
    return val;
  }

  /**
   * Asserts that the specified value is not negative ({@code val >= 0}).
   * <p>
   * This is equivalent to {@link #notNegative(Throwable, String, int) notNegative(null, label, val)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static double notNegative (final String label, final double val) {
    return notNegative(null, label, val);
  }

  /**
   * Asserts that the specified value is not negative ({@code val >= 0}).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static double notNegative (final Throwable cause, final String label, final double val) {
    // We also consider NaN:
    if (!(val >= 0)) {
      throw createIAE(label + " must not be negative: " + val, cause);
    }
    return val;
  }

  /**
   * Asserts that the specified value is not {@code null} or negative ({@code val >= 0}).
   * <p>
   * This is equivalent to {@link #notNegative(Throwable, String, int) notNegative(null, label, val)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static Byte notNegative (final String label, final Byte val) {
    return notNegative(null, label, val);
  }

  /**
   * Asserts that the specified value is not negative ({@code val >= 0}).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws NullPointerException if and only if {@code val == null}
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static Byte notNegative (final Throwable cause, final String label, final Byte val) {
    try {
      notNegative(cause, label, val.byteValue());
    } catch (final NullPointerException npe) {
      notNull(cause, label, val);
      throw npe;
    }
    return val;
  }

  /**
   * Asserts that the specified value is not {@code null} or negative ({@code val >= 0}).
   * <p>
   * This is equivalent to {@link #notNegative(Throwable, String, int) notNegative(null, label, val)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static Short notNegative (final String label, final Short val) {
    return notNegative(null, label, val);
  }

  /**
   * Asserts that the specified value is not negative ({@code val >= 0}).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws NullPointerException if and only if {@code val == null}
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static Short notNegative (final Throwable cause, final String label, final Short val) {
    try {
      notNegative(cause, label, val.shortValue());
    } catch (final NullPointerException npe) {
      notNull(cause, label, val);
      throw npe;
    }
    return val;
  }

  /**
   * Asserts that the specified value is not {@code null} or negative ({@code val >= 0}).
   * <p>
   * This is equivalent to {@link #notNegative(Throwable, String, int) notNegative(null, label, val)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static Integer notNegative (final String label, final Integer val) {
    return notNegative(null, label, val);
  }

  /**
   * Asserts that the specified value is not negative ({@code val >= 0}).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws NullPointerException if and only if {@code val == null}
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static Integer notNegative (final Throwable cause, final String label, final Integer val) {
    try {
      notNegative(cause, label, val.intValue());
    } catch (final NullPointerException npe) {
      notNull(cause, label, val);
      throw npe;
    }
    return val;
  }

  /**
   * Asserts that the specified value is not {@code null} or negative ({@code val >= 0}).
   * <p>
   * This is equivalent to {@link #notNegative(Throwable, String, int) notNegative(null, label, val)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static Long notNegative (final String label, final Long val) {
    return notNegative(null, label, val);
  }

  /**
   * Asserts that the specified value is not negative ({@code val >= 0}).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws NullPointerException if and only if {@code val == null}
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static Long notNegative (final Throwable cause, final String label, final Long val) {
    try {
      notNegative(cause, label, val.longValue());
    } catch (final NullPointerException npe) {
      notNull(cause, label, val);
      throw npe;
    }
    return val;
  }

  /**
   * Asserts that the specified value is not {@code null} or negative ({@code val >= 0}).
   * <p>
   * This is equivalent to {@link #notNegative(Throwable, String, int) notNegative(null, label, val)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static Float notNegative (final String label, final Float val) {
    return notNegative(null, label, val);
  }

  /**
   * Asserts that the specified value is not negative ({@code val >= 0}).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws NullPointerException if and only if {@code val == null}
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static Float notNegative (final Throwable cause, final String label, final Float val) {
    try {
      notNegative(cause, label, val.floatValue());
    } catch (final NullPointerException npe) {
      notNull(cause, label, val);
      throw npe;
    }
    return val;
  }

  /**
   * Asserts that the specified value is not {@code null} or negative ({@code val >= 0}).
   * <p>
   * This is equivalent to {@link #notNegative(Throwable, String, int) notNegative(null, label, val)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static Double notNegative (final String label, final Double val) {
    return notNegative(null, label, val);
  }

  /**
   * Asserts that the specified value is not negative ({@code val >= 0}).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws NullPointerException if and only if {@code val == null}
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static Double notNegative (final Throwable cause, final String label, final Double val) {
    try {
      notNegative(cause, label, val.doubleValue());
    } catch (final NullPointerException npe) {
      notNull(cause, label, val);
      throw npe;
    }
    return val;
  }

  /**
   * Asserts that the specified value is not {@code null} or negative ({@code val >= 0}).
   * <p>
   * This is equivalent to {@link #notNegative(Throwable, String, int) notNegative(null, label, val)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static BigInteger notNegative (final String label, final BigInteger val) {
    return notNegative(null, label, val);
  }

  /**
   * Asserts that the specified value is not negative ({@code val >= 0}).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws NullPointerException if and only if {@code val == null}
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static BigInteger notNegative (final Throwable cause, final String label, final BigInteger val) {
    try {
      if (val.signum() < 0) {
        throw createIAE(label + " must not be negative: " + val, cause);
      }
    } catch (final NullPointerException npe) {
      notNull(cause, label, val);
      throw npe;
    }
    return val;
  }

  /**
   * Asserts that the specified value is not {@code null} or negative ({@code val >= 0}).
   * <p>
   * This is equivalent to {@link #notNegative(Throwable, String, int) notNegative(null, label, val)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static BigDecimal notNegative (final String label, final BigDecimal val) {
    return notNegative(null, label, val);
  }

  /**
   * Asserts that the specified value is not negative ({@code val >= 0}).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   *
   * @return the specified {@code val} (which is greater than or equal to 0)
   *
   * @throws NullPointerException if and only if {@code val == null}
   * @throws IllegalArgumentException if and only if {@code val < 0}; the message will be
   *           {@code "label must not be negative: val"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static BigDecimal notNegative (final Throwable cause, final String label, final BigDecimal val) {
    try {
      if (val.signum() < 0) {
        throw createIAE(label + " must not be negative: " + val, cause);
      }
    } catch (final NullPointerException npe) {
      notNull(cause, label, val);
      throw npe;
    }
    return val;
  }

  /**
   * Asserts that the specified value is in the range of {@code min} to {@code max} (both inclusive).
   * <p>
   * This is equivalent to {@link #inRange(Throwable, String, int, int, int) inRange(null, label, val, min, max)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   * @param min the minimal accepted limit
   * @param max the maximal accepted limit
   *
   * @return the specified {@code val} for which {@code val >= min && val <= max}
   *
   * @throws IllegalArgumentException if and only if {@code val < min || val > max}; the message will be
   *           {@code "label == val is not in range [min .. max] (limits included)."} where {@code label} is the
   *           specified {@code label}, {@code val} is the specified value and {@code min} and {@code max} are the
   *           specified limits
   */
  public static int inRange (final String label, final int val, final int min, final int max) {
    return inRange(null, label, val, min, max);
  }

  /**
   * Asserts that the specified value is in the range of {@code min} to {@code max} (both inclusive).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   * @param min the minimal accepted limit
   * @param max the maximal accepted limit
   *
   * @return the specified {@code val} for which {@code val >= min && val <= max}
   *
   * @throws IllegalArgumentException if and only if {@code val < min || val > max}; the message will be
   *           {@code "label == val is not in range [min .. max] (limits included)."} where {@code label} is the
   *           specified {@code label}, {@code val} is the specified value and {@code min} and {@code max} are the
   *           specified limits
   */
  public static int inRange (final Throwable cause, final String label, final int val, final int min, final int max) {
    if (val < min || val > max) {
      throw createIAE(label + " == " + val + " is not in range [" + min + " .. " + max + "] (limits included).", cause);
    }
    return val;
  }

  /**
   * Asserts that the specified value is in the range of {@code min} to {@code max} (both inclusive).
   * <p>
   * This is equivalent to {@link #inRange(Throwable, String, long, long, long) inRange(null, label, val, min, max)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   * @param min the minimal accepted limit
   * @param max the maximal accepted limit
   *
   * @return the specified {@code val} for which {@code val >= min && val <= max}
   *
   * @throws IllegalArgumentException if and only if {@code val < min || val > max}; the message will be
   *           {@code "label == val is not in range [min .. max] (limits included)."} where {@code label} is the
   *           specified {@code label}, {@code val} is the specified value and {@code min} and {@code max} are the
   *           specified limits
   */
  public static long inRange (final String label, final long val, final long min, final long max) {
    return inRange(null, label, val, min, max);
  }

  /**
   * Asserts that the specified value is in the range of {@code min} to {@code max} (both inclusive).
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   * @param min the minimal accepted limit
   * @param max the maximal accepted limit
   *
   * @return the specified {@code val} for which {@code val >= min && val <= max}
   *
   * @throws IllegalArgumentException if and only if {@code val < min || val > max}; the message will be
   *           {@code "label == val is not in range [min .. max] (limits included)."} where {@code label} is the
   *           specified {@code label}, {@code val} is the specified value and {@code min} and {@code max} are the
   *           specified limits
   */
  public static long inRange (final Throwable cause, final String label, final long val, final long min, final long max) {
    if (val < min || val > max) {
      throw createIAE(label + " == " + val + " is not in range [" + min + " .. " + max + "] (limits included).", cause);
    }
    return val;
  }

  public static int inUnsignedRange (final String label, final int val, final int min, final int max) {
    return inUnsignedRange(null, label, val, min, max);
  }

  public static int inUnsignedRange (final Throwable cause, final String label, final int val, final int min,
          final int max) {
    if (Compare.compareUnsigned(val, min) < 0 || Compare.compareUnsigned(val, max) > 0) {
      throw createIAE(label + " == 0x" + Integer.toHexString(val) + " is not in range [0x" + Integer.toHexString(min)
              + " .. 0x" + Integer.toHexString(max) + "] (limits included).", cause);
    }
    return val;
  }

  public static long inUnsignedRange (final String label, final long val, final long min, final long max) {
    return inUnsignedRange(null, label, val, min, max);
  }

  public static long inUnsignedRange (final Throwable cause, final String label, final long val, final long min,
          final long max) {
    if (Compare.compareUnsigned(val, min) < 0 || Compare.compareUnsigned(val, max) > 0) {
      throw createIAE(label + " == 0x" + Long.toHexString(val) + " is not in range [0x" + Long.toHexString(min)
              + " .. 0x" + Long.toHexString(max) + "] (limits included).", cause);
    }
    return val;
  }

  /**
   * Asserts that the specified value is not {@code null} and in the range of {@code min} to {@code max} (both
   * inclusive).
   * <p>
   * This is equivalent to {@link #inRange(Throwable, String, Comparable, Comparable, Comparable) inRange(null, label,
   * val, min, max)}.
   *
   * @param <E> the type of {@link Comparable} to compare
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   * @param min the minimal accepted limit
   * @param max the maximal accepted limit
   *
   * @return the specified {@code val} which is not {@code null} and for which
   *         {@code val.compareTo(min) >= 0 && val.compareTo(max) <= 0}
   *
   * @throws NullPointerException if {@code val} is {@code null} (with a message containing just the specified
   *           {@code label}) or if either {@code min} or {@code max} are {@code null} (with the message {@code "min"}
   *           resp. {@code "max"})
   * @throws IllegalArgumentException if and only if {@code val < min || val > max}; the message will be
   *           {@code "label == val is not in range [min .. max] (limits included)."} where {@code label} is the
   *           specified {@code label}, {@code val} is the specified value and {@code min} and {@code max} are the
   *           specified limits
   */
  public static <E extends Comparable<? super E>> E inRange (final String label, final E val, final E min, final E max) {
    return inRange(null, label, val, min, max);
  }

  /**
   * Asserts that the specified value is not {@code null} and in the range of {@code min} to {@code max} (both
   * inclusive).
   *
   * @param <E> the type of {@link Comparable} to compare
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param val the value to test
   * @param min the minimal accepted limit
   * @param max the maximal accepted limit
   *
   * @return the specified {@code val} which is not {@code null} and for which
   *         {@code val.compareTo(min) >= 0 && val.compareTo(max) <= 0}
   *
   * @throws NullPointerException if {@code val} is {@code null} (with a message containing just the specified
   *           {@code label}) or if either {@code min} or {@code max} are {@code null} (with the message {@code "min"}
   * @throws IllegalArgumentException if and only if {@code val < min || val > max}; the message will be
   *           {@code "label == val is not in range [min .. max] (limits included)."} where {@code label} is the
   *           specified {@code label}, {@code val} is the specified value and {@code min} and {@code max} are the
   *           specified limits
   */
  public static <E extends Comparable<? super E>> E inRange (final Throwable cause, final String label, final E val,
          final E min, final E max) {
    notNull(cause, label, val);
    notNull(cause, "min", min);
    notNull(cause, "max", max);
    if (val.compareTo(min) < 0 || val.compareTo(max) > 0) {
      throw createIAE(label + " == " + val + " is not in range [" + min + " .. " + max + "] (limits included).", cause);
    }
    return val;
  }

  // ---- Index Bounds Tests

  /**
   * Checks if the specified offset {@code off} and the specified length {@code len} specify a valid (possibly empty)
   * slice of an (imaginary) array of length {@code limit} and throws an {@link IndexOutOfBoundsException} in case of
   * failure. This method is intended to be used for method arguments which specify a "sub-array" by specifying the
   * containing array, a starting offset and a length of the sub-array as in
   * {@link java.io.InputStream#read(byte[], int, int)} and many other methods in the Java APIs.
   * <p>
   * Example usage:
   *
   * <pre>
   * public int sum (final int[] arr, final int off, final int len) {
   *   CheckArg.notNull(&quot;arr&quot;, arr);
   *   CheckArg.indexValidOffsetAndLength(&quot;off&quot;, off, &quot;len&quot;, len, &quot;arr.length&quot;, arr.length);
   *   int sum = 0;
   *   for (int i = 0; i &lt; arr.length; i++) {
   *     sum += arr[i];
   *   }
   *   return sum;
   * }
   * </pre>
   * <p>
   * This is equivalent to {@link #indexValidOffsetAndLength(Throwable, String, int, String, int, String, int)
   * indexValidOffsetAndLength(null, offLabel, off, lenLabel, len, limitLabel, limit)}.
   *
   * @param offLabel a label for the offset value
   * @param off the offset to check
   * @param lenLabel a label for the length value
   * @param len the length to check
   * @param limitLabel a label for the limit value (typically {@code "ARRAY.length"} where {@code ARRAY} is a label for
   *          the array in question, may be {@code null} if the limit should not be labelled in the error message)
   * @param limit the limit (i.e. array length) assumed for the check
   *
   * @return the index of the first element after the specified slice, i.e. {@code (off + len)}
   *
   * @throws IndexOutOfBoundsException if and only if {@code off < 0} (with message
   *           {@code "offLabel must not be negative"}) or {@code len < 0} (with message {@code "lenLabel must not
   *           be negative"}) or {@code off + len > limit} (with message
   *           {@code "offLabel + lenLabel == off + len == end must not be greater than limitLabel limit."} where
   *           {@code end == off + len})
   */
  public static int indexValidOffsetAndLength (final String offLabel, final int off, final String lenLabel,
          final int len, final String limitLabel, final int limit) {
    return indexValidOffsetAndLength(null, offLabel, off, lenLabel, len, limitLabel, limit);
  }

  /**
   * Checks if the specified offset {@code off} and the specified length {@code len} specify a valid (possibly empty)
   * slice of an (imaginary) array of length {@code limit} and throws an {@link IndexOutOfBoundsException} in case of
   * failure. This method is intended to be used for method arguments which specify a "sub-array" by specifying the
   * containing array, a starting offset and a length of the sub-array as in
   * {@link java.io.InputStream#read(byte[], int, int)} and many other methods in the Java APIs.
   * <p>
   * Example usage:
   *
   * <pre>
   * public int sum (final int[] arr, final int off, final int len) {
   *   CheckArg.notNull(&quot;arr&quot;, arr);
   *   CheckArg.indexValidOffsetAndLength(&quot;off&quot;, off, &quot;len&quot;, len, &quot;arr.length&quot;, arr.length);
   *   int sum = 0;
   *   for (int i = 0; i &lt; arr.length; i++) {
   *     sum += arr[i];
   *   }
   *   return sum;
   * }
   * </pre>
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param offLabel a label for the offset value
   * @param off the offset to check
   * @param lenLabel a label for the length value
   * @param len the length to check
   * @param limitLabel a label for the limit value (typically {@code "ARRAY.length"} where {@code ARRAY} is a label for
   *          the array in question, may be {@code null} if the limit should not be labelled in the error message)
   * @param limit the limit (i.e. array length) assumed for the check
   *
   * @return the index of the first element after the specified slice, i.e. {@code (off + len)}
   *
   * @throws IndexOutOfBoundsException if and only if {@code off < 0} (with message
   *           {@code "offLabel must not be negative"}) or {@code len < 0} (with message {@code "lenLabel must not
   *           be negative"}) or {@code off + len > limit} (with message
   *           {@code "offLabel + lenLabel == off + len == end must not be greater than limitLabel limit."} where
   *           {@code end == off + len})
   */
  public static int indexValidOffsetAndLength (final Throwable cause, final String offLabel, final int off,
          final String lenLabel, final int len, final String limitLabel, final int limit) {
    indexNotNegative(cause, offLabel, off);
    indexNotNegative(cause, lenLabel, len);
    final int end = off + len;
    if (end > limit) {
      throw createIOOBE(offLabel + " + " + lenLabel + " == " + off + " + " + len + " == " + end
              + " must not be greater than " + (limitLabel == null ? "" : limitLabel + " ") + limit + ".", cause);
    }
    return end;
  }

  public static int indexValid (final String label, final int idx, final int len) {
    return indexValid(null, label, idx, null, len);
  }

  public static int indexValid (final String label, final int idx, final String limitLabel, final int len) {
    return indexValid(null, label, idx, limitLabel, len);
  }

  public static int indexValid (final Throwable cause, final String label, final int idx, final int len) {
    return indexValid(cause, label, idx, null, len);
  }

  public static int indexValid (final Throwable cause, final String label, final int idx, final String lenLabel,
          final int len) {
    indexNotNegative(cause, label, idx);
    indexLessThan(cause, label, idx, lenLabel, len);
    return idx;
  }

  public static long indexValid (final String label, final long idx, final long len) {
    return indexValid(null, label, idx, null, len);
  }

  public static long indexValid (final String label, final long idx, final String limitLabel, final long len) {
    return indexValid(null, label, idx, limitLabel, len);
  }

  public static long indexValid (final Throwable cause, final String label, final long idx, final long len) {
    return indexValid(cause, label, idx, null, len);
  }

  public static long indexValid (final Throwable cause, final String label, final long idx, final String lenLabel,
          final long len) {
    indexNotNegative(cause, label, idx);
    indexLessThan(cause, label, idx, lenLabel, len);
    return idx;
  }

  /**
   * Asserts that the specified index value is not negative ({@code idx >= 0}) and throws an
   * {@link IndexOutOfBoundsException} in case of failure.
   * <p>
   * This is equivalent to {@link #indexNotNegative(Throwable, String, int) indexNotNegative(null, label, idx)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param idx the index value to test
   *
   * @return the specified {@code idx} (which is greater than or equal to 0)
   *
   * @throws IndexOutOfBoundsException if and only if {@code idx < 0}; the message will be
   *           {@code "label must not be negative: idx"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static int indexNotNegative (final String label, final int idx) {
    return indexNotNegative(null, label, idx);
  }

  /**
   * Asserts that the specified index value is not negative ({@code idx >= 0}) and throws an
   * {@link IndexOutOfBoundsException} in case of failure.
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param idx the index value to test
   *
   * @return the specified {@code idx} (which is greater than or equal to 0)
   *
   * @throws IndexOutOfBoundsException if and only if {@code idx < 0}; the message will be
   *           {@code "label must not be negative: idx"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static int indexNotNegative (final Throwable cause, final String label, final int idx) {
    if (idx < 0) {
      throw createIOOBE(label + " must not be negative: " + idx, cause);
    }
    return idx;
  }

  /**
   * Asserts that the specified index value is not negative ({@code idx >= 0}) and throws an
   * {@link IndexOutOfBoundsException} in case of failure.
   * <p>
   * This is equivalent to {@link #indexNotNegative(Throwable, String, long) indexNotNegative(null, label, idx)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param idx the index value to test
   *
   * @return the specified {@code idx} (which is greater than or equal to 0)
   *
   * @throws IndexOutOfBoundsException if and only if {@code idx < 0}; the message will be
   *           {@code "label must not be negative: idx"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static long indexNotNegative (final String label, final long idx) {
    return indexNotNegative(null, label, idx);
  }

  /**
   * Asserts that the specified index value is not negative ({@code idx >= 0}) and throws an
   * {@link IndexOutOfBoundsException} in case of failure.
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param idx the index value to test
   *
   * @return the specified {@code idx} (which is greater than or equal to 0)
   *
   * @throws IndexOutOfBoundsException if and only if {@code idx < 0}; the message will be
   *           {@code "label must not be negative: idx"} where {@code label} is the specified {@code label} and
   *           {@code val} is the specified value
   */
  public static long indexNotNegative (final Throwable cause, final String label, final long idx) {
    if (idx < 0) {
      throw createIOOBE(label + " must not be negative: " + idx, cause);
    }
    return idx;
  }

  /**
   * Asserts that the specified index value is less than or equal to the specified limit ({@code idx <= limit}) and
   * throws an {@link IndexOutOfBoundsException} in case of failure.
   * <p>
   * This is equivalent to {@link #indexNotGreater(Throwable, String, int, String, int) indexNotGreater(null, label,
   * idx, limitLabel, limit)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param idx the index value to test
   * @param limitLabel a label for the limit value (may be {@code null} if the limit should not be labelled in the error
   *          message)
   * @param limit the limit (i.e. maximal allowed value) assumed for the check
   *
   * @return the specified {@code idx} (which is smaller than or equal to the specified {@code limit})
   *
   * @throws IndexOutOfBoundsException if and only if {@code idx > limit}; the message will be
   *           {@code "label must not be greater than limitLabel limit: idx"} where {@code label} is the specified
   *           {@code label}, {@code limitLabel} is the specified {@code limitLabel}, {@code limit} is the specified
   *           {@code limit} and {@code idx} is the specified index
   */
  public static int indexNotGreater (final String label, final int idx, final String limitLabel, final int limit) {
    return indexNotGreater(null, label, idx, limitLabel, limit);
  }

  /**
   * Asserts that the specified index value is less than or equal to the specified limit ({@code idx <= limit}) and
   * throws an {@link IndexOutOfBoundsException} in case of failure.
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param idx the index value to test
   * @param limitLabel a label for the limit value (may be {@code null} if the limit should not be labelled in the error
   *          message)
   * @param limit the limit (i.e. maximal allowed value) assumed for the check
   *
   * @return the specified {@code idx} (which is smaller than or equal to the specified {@code limit})
   *
   * @throws IndexOutOfBoundsException if and only if {@code idx > limit}; the message will be
   *           {@code "label must not be greater than limitLabel limit: idx"} where {@code label} is the specified
   *           {@code label}, {@code limitLabel} is the specified {@code limitLabel}, {@code limit} is the specified
   *           {@code limit} and {@code idx} is the specified index
   */
  public static int indexNotGreater (final Throwable cause, final String label, final int idx, final String limitLabel,
          final int limit) {
    if (idx > limit) {
      throw createIOOBE(label + " must not be greater than " + (limitLabel == null ? "" : limitLabel + " ") + limit
              + ": " + idx, cause);
    }
    return idx;
  }

  /**
   * Asserts that the specified index value is less than or equal to the specified limit ({@code idx <= limit}) and
   * throws an {@link IndexOutOfBoundsException} in case of failure.
   * <p>
   * This is equivalent to {@link #indexNotGreater(Throwable, String, long, String, long) indexNotGreater(null, label,
   * idx, limitLabel, limit)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param idx the index value to test
   * @param limitLabel a label for the limit value (may be {@code null} if the limit should not be labelled in the error
   *          message)
   * @param limit the limit (i.e. maximal allowed value) assumed for the check
   *
   * @return the specified {@code idx} (which is smaller than or equal to the specified {@code limit})
   *
   * @throws IndexOutOfBoundsException if and only if {@code idx > limit}; the message will be
   *           {@code "label must not be greater than limitLabel limit: idx"} where {@code label} is the specified
   *           {@code label}, {@code limitLabel} is the specified {@code limitLabel}, {@code limit} is the specified
   *           {@code limit} and {@code idx} is the specified index
   */
  public static long indexNotGreater (final String label, final long idx, final String limitLabel, final long limit) {
    return indexNotGreater(null, label, idx, limitLabel, limit);
  }

  /**
   * Asserts that the specified index value is less than or equal to the specified limit ({@code idx <= limit}) and
   * throws an {@link IndexOutOfBoundsException} in case of failure.
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param idx the index value to test
   * @param limitLabel a label for the limit value (may be {@code null} if the limit should not be labelled in the error
   *          message)
   * @param limit the limit (i.e. maximal allowed value) assumed for the check
   *
   * @return the specified {@code idx} (which is smaller than or equal to the specified {@code limit})
   *
   * @throws IndexOutOfBoundsException if and only if {@code idx > limit}; the message will be
   *           {@code "label must not be greater than limitLabel limit: idx"} where {@code label} is the specified
   *           {@code label}, {@code limitLabel} is the specified {@code limitLabel}, {@code limit} is the specified
   *           {@code limit} and {@code idx} is the specified index
   */
  public static long indexNotGreater (final Throwable cause, final String label, final long idx,
          final String limitLabel, final long limit) {
    if (idx > limit) {
      throw createIOOBE(label + " must not be greater than " + (limitLabel == null ? "" : limitLabel + " ") + limit
              + ": " + idx, cause);
    }
    return idx;
  }

  /**
   * Asserts that the specified index value is less than the specified limit ({@code idx <= limit}) and throws an
   * {@link IndexOutOfBoundsException} in case of failure.
   * <p>
   * This is equivalent to {@link #indexLessThan(Throwable, String, int, String, int) indexLessThan(null, label, idx,
   * limitLabel, limit)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param idx the index value to test
   * @param limitLabel a label for the limit value (may be {@code null} if the limit should not be labelled in the error
   *          message)
   * @param limit the limit (for example the array length or collections size) assumed for the check; should not be
   *          negative, though this is not enforced
   *
   * @return the specified {@code idx} (which is smaller than the specified {@code limit})
   *
   * @throws IndexOutOfBoundsException if and only if {@code idx >= limit}; the message will be
   *           {@code "label must be less than limitLabel limit: idx"} where {@code label} is the specified
   *           {@code label}, {@code limitLabel} is the specified {@code limitLabel}, {@code limit} is the specified
   *           {@code limit} and {@code idx} is the specified index
   */
  public static int indexLessThan (final String label, final int idx, final String limitLabel, final int limit) {
    return indexLessThan(null, label, idx, limitLabel, limit);
  }

  /**
   * Asserts that the specified index value is less than the specified limit ({@code idx <= limit}) and throws an
   * {@link IndexOutOfBoundsException} in case of failure.
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param idx the index value to test
   * @param limitLabel a label for the limit value (may be {@code null} if the limit should not be labelled in the error
   *          message)
   * @param limit the limit (for example the array length or collections size) assumed for the check; should not be
   *          negative, though this is not enforced
   *
   * @return the specified {@code idx} (which is smaller than the specified {@code limit})
   *
   * @throws IndexOutOfBoundsException if and only if {@code idx >= limit}; the message will be
   *           {@code "label must be less than limitLabel limit: idx"} where {@code label} is the specified
   *           {@code label}, {@code limitLabel} is the specified {@code limitLabel}, {@code limit} is the specified
   *           {@code limit} and {@code idx} is the specified index
   */
  public static int indexLessThan (final Throwable cause, final String label, final int idx, final String limitLabel,
          final int limit) {
    if (idx >= limit) {
      throw createIOOBE(label + " must be less than " + (limitLabel == null ? "" : limitLabel + " ") + limit + ": "
              + idx, cause);
    }
    return idx;
  }

  /**
   * Asserts that the specified index value is less than the specified limit ({@code idx < limit}) and throws an
   * {@link IndexOutOfBoundsException} in case of failure.
   * <p>
   * This is equivalent to {@link #indexLessThan(Throwable, String, long, String, long) indexLessThan(null, label, idx,
   * limitLabel, limit)}.
   *
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param idx the index value to test
   * @param limitLabel a label for the limit value (may be {@code null} if the limit should not be labelled in the error
   *          message)
   * @param limit the limit (for example the array length or collections size) assumed for the check; should not be
   *          negative, though this is not enforced
   *
   * @return the specified {@code idx} (which is smaller than the specified {@code limit})
   *
   * @throws IndexOutOfBoundsException if and only if {@code idx >= limit}; the message will be
   *           {@code "label must be less than limitLabel limit: idx"} where {@code label} is the specified
   *           {@code label}, {@code limitLabel} is the specified {@code limitLabel}, {@code limit} is the specified
   *           {@code limit} and {@code idx} is the specified index
   */
  public static long indexLessThan (final String label, final long idx, final String limitLabel, final long limit) {
    return indexLessThan(null, label, idx, limitLabel, limit);
  }

  /**
   * Asserts that the specified index value is less than the specified limit ({@code idx < limit}) and throws an
   * {@link IndexOutOfBoundsException} in case of failure.
   *
   * @param cause a {@linkplain Throwable#getCause() "root cause"} for the exception thrown in case of a failed
   *          assertion (or {@code null} if there is no such cause)
   * @param label a label describing the argument to test (may but should not be {@code null})
   * @param idx the index value to test
   * @param limitLabel a label for the limit value (may be {@code null} if the limit should not be labelled in the error
   *          message)
   * @param limit the limit (for example the array length or collections size) assumed for the check; should not be
   *          negative, though this is not enforced
   *
   * @return the specified {@code idx} (which is smaller than the specified {@code limit})
   *
   * @throws IndexOutOfBoundsException if and only if {@code idx >= limit}; the message will be
   *           {@code "label must be less than limitLabel limit: idx"} where {@code label} is the specified
   *           {@code label}, {@code limitLabel} is the specified {@code limitLabel}, {@code limit} is the specified
   *           {@code limit} and {@code idx} is the specified index
   */
  public static long indexLessThan (final Throwable cause, final String label, final long idx, final String limitLabel,
          final long limit) {
    if (idx >= limit) {
      throw createIOOBE(label + " must be less than " + (limitLabel == null ? "" : limitLabel + " ") + limit + ": "
              + idx, cause);
    }
    return idx;
  }

  // ---- Private Helper Methods

  private static String l (final String label) {
    return label == null ? "?" : label;
  }

  private static NullPointerException createNPE (final String msg, final Throwable cause) {
    if (cause == null) {
      return new NullPointerException(msg);
    } else {
      final NullPointerException npe = new NullPointerException(msg);
      npe.initCause(cause);
      return npe;
    }
  }

  private static IllegalArgumentException createIAE (final String msg, final Throwable cause) {
    if (cause == null) {
      return new IllegalArgumentException(msg);
    } else {
      return new IllegalArgumentException(msg, cause);
    }
  }

  private static IndexOutOfBoundsException createIOOBE (final String msg, final Throwable cause) {
    if (cause == null) {
      return new IndexOutOfBoundsException(msg);
    } else {
      final IndexOutOfBoundsException ioobe = new IndexOutOfBoundsException(msg);
      ioobe.initCause(cause);
      return ioobe;
    }
  }

  // ---- Constructors

  private CheckArg () {
    throw new AssertionError("not allowed");
  }
}
