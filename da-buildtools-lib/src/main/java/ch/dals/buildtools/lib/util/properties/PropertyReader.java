/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.lib.util.properties;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.AbstractMap;
import java.util.Map;
import java.util.TreeMap;

import ch.dals.buildtools.lib.util.CheckArg;


/**
 * TODO [javadoc]: type PropertyUtil
 *
 * @author zisch
 */
public final class PropertyReader {
  private static final Charset LATIN1 = Charset.forName("ISO-8859-1");

  /**
   * Reads the properties from the specified {@code file} and returns them in a newly created {@link TreeMap}.
   *
   * @param file path to the properties file (not {@code null}, pointing to an existing, readable file)
   *
   * @return the properties contained in the file
   *
   * @throws IOException in case of I/O errors when reading the file
   */
  public static TreeMap<String, String> readProperties (final Path file) throws IOException {
    return readProperties(new TreeMap<String, String>(), file);
  }

  /**
   * Reads the properties from the specified {@code file} and puts them into the specified {@link Map}.
   *
   * @param <T> the map type
   * @param properties the target map (not {@code null}, must be mutable)
   * @param file path to the properties file (not {@code null}, pointing to an existing, readable file)
   *
   * @return the specified target map {@code properties}
   *
   * @throws IOException in case of I/O errors when reading the file
   */
  public static <T extends Map<? super String, ? super String>> T readProperties (final T properties, final Path file)
          throws IOException {
    CheckArg.notNull("properties", properties);
    CheckArg.notNull("file", file);
    try (final Reader r = Files.newBufferedReader(file, LATIN1)) {
      final PropertyReader pr = new PropertyReader(r);
      pr.readRemainingProperties(properties);
    }
    return properties;
  }

  private final BufferedReader mInput;

  private final StringBuilder mLogicalLineBuffer = new StringBuilder();

  private int mLogicalLineBufferIndex = 0;

  private final StringBuilder mValueBuffer = new StringBuilder();

  /**
   * Creates a {@link PropertyReader} reading from the specified {@code input} stream.
   *
   * @param input the input stream to read from
   */
  public PropertyReader (final InputStream input) {
    this(new InputStreamReader(CheckArg.notNull("input", input), LATIN1));
  }

  /**
   * Creates a {@link PropertyReader} reading from the specified {@code input} reader.
   *
   * @param input the reader to read from
   */
  public PropertyReader (final Reader input) {
    CheckArg.notNull("input", input);
    mInput = input instanceof BufferedReader ? (BufferedReader) input : new BufferedReader(input);
  }

  /**
   * Reads the remaining properties from the input and puts them into the specified {@link Map}.
   *
   * @param <T> the map type
   * @param properties the target map (not {@code null}, must be mutable)
   *
   * @return the specified target map {@code properties}
   *
   * @throws IOException in case of I/O errors while reading the input
   */
  public <T extends Map<? super String, ? super String>> T readRemainingProperties (final T properties)
          throws IOException {
    while (readNextProperty(properties)) {
      // empty body
    }
    return properties;
  }

  /**
   * Reads the next single property from the input and puts it into the specified {@link Map}.
   *
   * @param properties the target map (not {@code null}, must be mutable)
   *
   * @return {@code true} if a property could has been read from the input and put into the target map, {@code false} if
   *         the end of the input has been reached before a property could be read
   *
   * @throws IOException in case of I/O errors while reading the input
   */
  public boolean readNextProperty (final Map<? super String, ? super String> properties) throws IOException {
    final String key = readKey();
    if (key == null) {
      return false;
    } else {
      final String value = readValue();
      properties.put(key, value);
      return true;
    }
  }

  /**
   * Reads the next single property from the input and returns it as a map entry.
   *
   * @return a map entry containing the name and value of the read property or {@code null} if the end of the input has
   *         been reached before a property could be read
   *
   * @throws IOException in case of I/O errors while reading the input
   */
  public AbstractMap.SimpleEntry<String, String> readNextProperty () throws IOException {
    final String key = readKey();
    if (key == null) {
      return null;
    } else {
      final String value = readValue();
      return new AbstractMap.SimpleEntry<>(key, value);
    }
  }

  private String readKey () throws IOException {
    if (!readNextLogicalLine()) {
      return null;
    }
    skipWhitespaceInLogicalLine();
    return decodeKeyOrValue(true);
  }

  private String readValue () throws IOException {
    skipWhitespaceInLogicalLine();
    final char c = mLogicalLineBuffer.charAt(mLogicalLineBufferIndex);
    if (c == ':' || c == '=') {
      mLogicalLineBufferIndex++;
      skipWhitespaceInLogicalLine();
    }
    return decodeKeyOrValue(false);
  }

  private String decodeKeyOrValue (final boolean key) throws IOException {
    mValueBuffer.setLength(0);
    final int len = mLogicalLineBuffer.length();
    boolean escaped = false;
    while (mLogicalLineBufferIndex < len) {
      if (escaped) {
        unescapeNextLogicalLineChar();
        escaped = false;

      } else {
        final char c = mLogicalLineBuffer.charAt(mLogicalLineBufferIndex);
        if (c == '\\') {
          escaped = true;

        } else if (key && isKeyTerminator(c)) {
          break;

        } else {
          mValueBuffer.append(c);
        }
      }
      mLogicalLineBufferIndex++;
    }
    return mValueBuffer.toString();
  }

  private char unescapeNextLogicalLineChar () throws IOException {
    final char c = mLogicalLineBuffer.charAt(mLogicalLineBufferIndex);
    switch (c) {
      case 'u':
        return decodeUnicodeEscape();

      case 't':
        return '\t';

      case 'f':
        return '\f';

      case 'n':
        return '\n';

      case 'r':
        return '\r';

      default:
        return c;
    }
  }

  private char decodeUnicodeEscape () throws IOException {
    // Note: The current index is supposed to point to the 'u' following the backslash!
    assert mLogicalLineBufferIndex > 0;
    final int start = mLogicalLineBufferIndex + 1;
    final int end = start + 4;
    if (mLogicalLineBuffer.length() < end) {
      throw new IOException("Incomplete unicode escape sequence at end of logical line: '"
              + mLogicalLineBuffer.substring(mLogicalLineBufferIndex - 1, end) + "'");
    }
    int value = 0;
    for (int i = start; i < end; i++) {
      final char c = mLogicalLineBuffer.charAt(i);
      switch (c) {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
          value = (value << 4) + c - '0';
          break;

        case 'a':
        case 'b':
        case 'c':
        case 'd':
        case 'e':
        case 'f':
          value = (value << 4) + 10 + c - 'a';
          break;

        case 'A':
        case 'B':
        case 'C':
        case 'D':
        case 'E':
        case 'F':
          value = (value << 4) + 10 + c - 'A';
          break;

        default:
          throw new IOException(String.format("Malformed unicode escape sequence \\uXXXX; "
                  + "invalid character U+%04x in place of hex digit at position %d: '%s'", Integer.valueOf(c),
                  Integer.valueOf(i - mLogicalLineBufferIndex),
                  mLogicalLineBuffer.substring(mLogicalLineBufferIndex - 1, end)));
      }
    }
    return (char) value;
  }

  private void skipWhitespaceInLogicalLine () {
    final int len = mLogicalLineBuffer.length();
    while (mLogicalLineBufferIndex < len && isWhitespace(mLogicalLineBuffer.charAt(mLogicalLineBufferIndex))) {
      mLogicalLineBufferIndex++;
    }
  }

  private boolean readNextLogicalLine () throws IOException {
    mLogicalLineBuffer.setLength(0);
    mLogicalLineBufferIndex = 0;
    String line = mInput.readLine();
    while (line != null && isBlankOrComment(line)) {
      line = mInput.readLine();
    }
    if (line == null) {
      return false;
    }
    while (isContinuedLine(line)) {
      // Append line without the trailing escape character:
      mLogicalLineBuffer.append(line.substring(0, line.length() - 1));

      // Read next continuation line, trimming leading whitespace:
      line = mInput.readLine();
      if (line == null) {
        //
        // Note: The specification in Properties#load(...) does not define what to do when the last character before EOF
        // is a backslash (i.e. a continued line is missing the continuation line), as in the following properties file:
        //
        // ----------
        // foo=bar\
        // ----------
        //
        // (Note that the EOF follows directly the backslash without any line termination characters!)
        //
        // The original Properties implementation will return a NUL character in place of the escape character; though
        // under certain circumstances, namely if the end of the internally used buffer happens to hit the last
        // character before the trailing escape character, an ArrayIndexOutOfBoundsException might result. (In the
        // example above, the loaded properties would therefore contain one key "foo" with the value "bar\u0000".)
        // Replacing the backslash with a NUL character does not seem the correct thing to do, and certainly it is not
        // correct to do this "most of the time" but "sometimes" throw an exception instead!
        //
        // The cleanest solution would be to always signal an error, i.e. throw an exception in this case. However, I
        // would prefer to be somewhat lenient on the input (at least as lenient as the original Properties). Therefore
        // we always ignore the trailing escape character; in other words: we view an escape character before EOF as a
        // line continuation followed by an empty line.
        //
        line = "";
      } else {
        int off = 0;
        while (off < line.length() && isWhitespace(line.charAt(off))) {
          off++;
        }
        line = line.substring(off);
      }
    }
    mLogicalLineBuffer.append(line);
    return true;
  }

  private static boolean isBlankOrComment (final String line) {
    for (int i = 0; i < line.length(); i++) {
      final char c = line.charAt(i);
      if (!isWhitespace(c)) {
        return c == '#' || c == '!';
      }
    }
    return true;
  }

  /**
   * A line is continued if the line terminator is preceded by an odd number of escape characters.
   */
  private static boolean isContinuedLine (final String line) {
    int escCount = 0;
    for (int i = line.length() - 1; i >= 0; i--) {
      if (line.charAt(i) != '\\') {
        break;
      }
      escCount++;
    }
    return (escCount & 1) == 1;
  }

  private static boolean isKeyTerminator (final char c) {
    return isWhitespace(c) || c == ':' || c == '=';
  }

  private static boolean isWhitespace (final char c) {
    switch (c) {
      case ' ':
      case '\t':
      case '\f':
        return true;

      default:
        return false;
    }
  }
}
