/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.main.init;


import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Map;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

import ch.dals.buildtools.gradle.core.util.RscUtil;
import ch.dals.buildtools.gradle.core.util.testing.files.FileTreeDiff;
import ch.dals.buildtools.gradle.core.util.testing.junit.ExtendedTemporaryFolder;


/**
 * Integration test for {@link DaInit#main(String...)}.
 *
 * @author zisch
 */
public class DaInitITest {
  /** Name of the resource ZIP file containing the expected output of the test. */
  public static final String EXPECTED_ZIP_NAME = "DaInitITest-expected.zip";

  private static final String PROJECT_NAME = "da-simsam";

  /** Temporary folder for test data. */
  @Rule
  public final ExtendedTemporaryFolder sTempFolder = (new ExtendedTemporaryFolder()).keepFolderOnFailure(true);

  /**
   * Integration test for {@link DaInit#main(String...)}.
   *
   * @throws IOException in case of I/O errors
   */
  @Test
  @Ignore
  // FIXME: currently fails
  public void testDaInit () throws IOException {
    final File expectedDir = sTempFolder.newFolder("expectedDir");
    RscUtil.unzipClasspathResource(DaInitITest.class, "testdata/" + EXPECTED_ZIP_NAME, expectedDir,
            Collections.emptyMap());

    final File targetDir = sTempFolder.newFolder("targetDir");
    DaInit.main(PROJECT_NAME, targetDir.toString());

    final Map<Path, FileTreeDiff.Result> diffResult = FileTreeDiff.diffRecursive(expectedDir.toPath(),
            targetDir.toPath());
    Assert.assertTrue("Unexpected init result: " + diffResult, diffResult.isEmpty());
    // Assert.fail("Want to fail!");
  }

}
