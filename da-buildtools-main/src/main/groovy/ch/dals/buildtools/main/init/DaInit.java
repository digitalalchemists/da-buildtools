/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.main.init;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.TreeMap;

import ch.dals.buildtools.gradle.core.ext.AbstractNamedNode;
import ch.dals.buildtools.lib.util.CheckArg;


/**
 * Main class for the {@code da-init} command line tool.
 *
 * @author zisch
 */
public final class DaInit {
  /**
   * Main method.
   *
   * @param args the command line arguments
   *
   * @throws IOException in case of I/O errors
   */
  public static void main (final String... args) throws IOException {
    if (args.length != 2) {
      usage("Unexpected number of command line arguments.");
    }
    final String projectNameArg = args[0];
    final Path targetDirectory = Paths.get(args[1]);
    final DaInit init = new DaInit(projectNameArg, targetDirectory);
    init.executeInstallation();
  }

  private static void usage (final String errorMessage) {
    final String msg = "FATAL: Invalid command line: " + errorMessage;
    System.out.println(msg);
    System.out.println("Usage: java -cp ... ch.dals.buildtools.gradle.core.init.DaInit PROJECTNAME TARGETDIR");
    throw new IllegalArgumentException(msg);
  }

  private final String mProjectName;

  private final Path mTargetDirectory;

  private final Path mRootProjectDirectory;

  private DaInit (final String projectName, final Path targetDirectory) {
    mProjectName = AbstractNamedNode.checkName(projectName);
    mTargetDirectory = CheckArg.notNull("targetDirectory", targetDirectory).toAbsolutePath();
    mRootProjectDirectory = mTargetDirectory.resolve(mProjectName + "-root");
  }

  private void executeInstallation () throws IOException {
    if (Files.exists(mTargetDirectory) && (!Files.isDirectory(mTargetDirectory) || hasChildren(mTargetDirectory))) {
      throw new FileNotFoundException(
              "Target directory already exists but is not empty (or not a directory); please remove/empty first: '"
                      + mTargetDirectory + "'");
    }
    if (!Files.exists(mTargetDirectory)) {
      if (mTargetDirectory.getParent() != null && !Files.isDirectory(mTargetDirectory.getParent())) {
        throw new FileNotFoundException("Parent directory of target directory '" + mTargetDirectory
                + "' does not exist or is not a directory.");
      }
      Files.createDirectories(mTargetDirectory);
    }
    final Map<String, String> tokens = new TreeMap<>();
    tokens.put("project.name", mProjectName);
    DaInitRsc.unzipRootTemplate(mTargetDirectory, tokens);
  }

  private static boolean hasChildren (final Path dir) throws IOException {
    try (final DirectoryStream<Path> ds = Files.newDirectoryStream(dir)) {
      return ds.iterator().hasNext();
    }
  }
}
