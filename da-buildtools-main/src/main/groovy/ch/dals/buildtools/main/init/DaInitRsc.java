/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.main.init;


import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;

import ch.dals.buildtools.gradle.core.util.FileUtil;
import ch.dals.buildtools.gradle.core.util.RscUtil;


/**
 * Manages resources for {@link DaInit}.
 *
 * @author zisch
 */
public final class DaInitRsc {
  /**
   * Name of the ZIP archive in the resources which contains the project root template.
   */
  public static final String ROOT_TEMPLATE_NAME = "DaInitRsc-root-template.zip";

  /**
   * Unzips the project root template to the specified {@code dest} path.
   *
   * @param dest the target destination path where the template should be unzipped
   * @param tokens tokens to interpolate in filenames and content
   *
   * @throws IOException in case of any I/O errors
   */
  public static void unzipRootTemplate (final Path dest, final Map<?, ?> tokens) throws IOException {
    // TODO: Ant unzip does not know how to handle file permissions, so we should actually replace the use of Ant with
    // our own unzip function (including mapping and filtering)!
    RscUtil.unzipClasspathResource(DaInitRsc.class, ROOT_TEMPLATE_NAME, dest, tokens);
    final String rootFolderName = tokens.get("project.name") + "-root";
    final Path gradlewPath = dest.resolve(rootFolderName).resolve("gradlew");
    FileUtil.makeExecutable(gradlewPath);
  }

  private DaInitRsc () {
    throw new AssertionError("not allowed");
  }
}
