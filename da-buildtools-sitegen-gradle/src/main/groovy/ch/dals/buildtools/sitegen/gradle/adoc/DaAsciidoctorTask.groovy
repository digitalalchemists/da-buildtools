/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.sitegen.gradle.adoc;


import org.asciidoctor.gradle.AsciidoctorTask


/**
 * TODO [javadoc]: type DaAsciidoctorTask
 *
 * @author zisch
 */
class DaAsciidoctorTask extends AsciidoctorTask {
  private static final String GROUP = "Documentation";

  private static final String DESCRIPTION = "Converts AsciiDoc files and copies the output files and related resources to the build directory.";

  /**
   * Default constructor.
   */
  public DaAsciidoctorTask () {
    setGroup(GROUP);
    setDescription(DESCRIPTION);
    resources {}
  }
}
