/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.git.core;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;

import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;


/**
 * TODO [javadoc]: type GitUtil
 *
 * @author zisch
 */
public final class GitUtil {

  public static String currentBranch (final File location) throws IOException {
    final FileRepositoryBuilder builder = new FileRepositoryBuilder();
    final File gitDir = builder.findGitDir(location).readEnvironment().getGitDir();
    if (gitDir == null) {
      throw new FileNotFoundException("Cannot find Git directory upwards from: '" + location + "'");
    }
    final Repository repository = builder.build();
    try {
      final String branchName = repository.getBranch();
      if (branchName == null) {
        throw new FileNotFoundException("Failed to find HEAD reference in Git directory: '" + gitDir + "'");
      }
      return branchName;
    } finally {
      repository.close();
    }
  }

  public static String currentBranch (final Path location) throws IOException {
    return currentBranch(location.toFile());
  }

  private GitUtil () {
    throw new AssertionError("not allowed");
  }

}
