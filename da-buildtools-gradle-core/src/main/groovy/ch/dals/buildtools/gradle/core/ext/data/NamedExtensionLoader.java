/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.ext.data;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import org.gradle.api.Project;

import ch.dals.buildtools.gradle.core.ext.AbstractNamedNode;
import ch.dals.buildtools.lib.util.CheckArg;


/**
 * Loads predefined extension data.
 *
 * @author zisch
 */
public final class NamedExtensionLoader {

  private final NamedExtensionCache mNamedExtensions;

  private final WriteablePropertiesCache mWriteableProperties = new WriteablePropertiesCache();

  /**
   * Constructor.
   *
   * @param project the Gradle project for which the predefined data should be loaded
   */
  public NamedExtensionLoader (final Project project) {
    mNamedExtensions = new NamedExtensionCache(project);
  }

  /**
   * Gets the data for the specified {@code nodeType} and {@code dataId} and sets the corresponding properties on the
   * specified {@code dataObject}.
   *
   * @param <E> the type of data object to populate
   * @param extensionType the type of data object to populate
   * @param extension the data object to populate
   * @param name the ID of the data entry defining the properties values
   *
   * @return the modified {@code dataObject}
   *
   * @throws IllegalArgumentException if no extension of the specified type and name could be found
   */
  public <E extends AbstractNamedNode> E loadNamedExtension (final Class<E> extensionType, final E extension,
          final CharSequence name) {
    CheckArg.notNull("extensionType", extensionType);
    CheckArg.notNull("extension", extension);
    CheckArg.notEmpty("name", name);
    final Map<String, NamedExtensionCache.PropertyEntry> data = mNamedExtensions.getData(extensionType, name);
    final Map<String, Method> props = mWriteableProperties.getProperties(extensionType);
    for (final Map.Entry<String, Method> e : props.entrySet()) {
      final Method m = e.getValue();
      final String value = data.get(e.getKey()).getValue();
      try {
        m.invoke(extension, value);
      } catch (final IllegalAccessException exc) {
        throw new IllegalStateException("Unexpected IllegalAccessException; method '" + m
                + "' is supposed to be public: " + exc, exc);
      } catch (final IllegalArgumentException exc) {
        throw new IllegalStateException("Unexpected IllegalArgumentException when invoking method '" + m
                + "' with String parameter '" + value + "': " + exc, exc);
      } catch (final InvocationTargetException exc) {
        throw new IllegalStateException("Invocation of method '" + m + "' with String parameter '" + value
                + "' failed: " + exc.getCause(), exc);
      }
    }
    extension.setName(name);
    return extension;
  }
}
