/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.util;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFileAttributeView;
import java.nio.file.attribute.PosixFilePermission;
import java.util.EnumSet;
import java.util.Set;

import ch.dals.buildtools.lib.util.CheckArg;


/**
 * TODO [javadoc]: type FileUtil
 *
 * @author zisch
 */
public class FileUtil {

  public static void makeExecutable (final Path path) throws IOException {
    mirrorPosixPermissions(path, PosixPermissionType.READ, PosixPermissionType.EXECUTE);
  }

  public static void mirrorPosixPermissions (final Path path, final PosixPermissionType from) throws IOException {
    mirrorPosixPermissions(path, from, null);
  }

  public static void mirrorPosixPermissions (final Path path, final PosixPermissionType from, final PosixPermissionType to)
          throws IOException {
    CheckArg.notNull("path", path);
    CheckArg.notNull("from", from);
    if (Files.getFileStore(path).supportsFileAttributeView(PosixFileAttributeView.class)) {
      final Set<PosixFilePermission> perms = EnumSet.copyOf(Files.getPosixFilePermissions(path));
      for (final PosixFilePermission fromPerm : from.getFilePermissions()) {
        if (perms.contains(fromPerm)) {
          final PosixPermissionClass c = PosixPermissionClass.forFilePermission(fromPerm);
          if (to == null) {
            switch (from) {
              case READ:
                perms.add(c.toFilePermission(PosixPermissionType.WRITE));
                perms.add(c.toFilePermission(PosixPermissionType.EXECUTE));
                break;

              case WRITE:
                perms.add(c.toFilePermission(PosixPermissionType.WRITE));
                perms.add(c.toFilePermission(PosixPermissionType.EXECUTE));
                break;

              case EXECUTE:
                perms.add(c.toFilePermission(PosixPermissionType.WRITE));
                perms.add(c.toFilePermission(PosixPermissionType.EXECUTE));
                break;

              default:
                throw new AssertionError("Unexpected " + PosixPermissionType.class.getSimpleName()
                        + " for 'from' argument: " + from);
            }
          } else {
            perms.add(c.toFilePermission(to));
          } /* if to == null */
        } /* if perms.contains(...) */
      } /* for */
      Files.setPosixFilePermissions(path, perms);
    } /* if PosixFileAttributeView */
  }

  private FileUtil () {
    throw new AssertionError("not allowed");
  }
}
