/*
 * This file is part of the DA Secure Communication Framework (SCF) project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA SCF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA SCF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA SCF.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.ide.eclipse

import org.gradle.api.Project

/**
 * Eclipse configuration for DA projects.
 *
 * @author zisch
 */
class EclipseConfigurator {
  static void configure (final Project project) {
    project.configure(project) {
      apply plugin: 'eclipse'

      configure(tasks['eclipse']) {
        dependsOn('cleanEclipse') // completely overwrite existing configurations

        doLast {
          // We replace everything in .eclipse-settings with our own templates:
          copy {
            from "${project.rootDir}/rsc/eclipse-settings"
            into '.settings'
            expand(javaVersion: targetCompatibility, cleanupOnSave: "true" /*String.valueOf(project.name != 'da-scf-core-silvertunnel')*/)
          }
          // TODO: Task(s) to sort Eclipse settings and copy them (with escapes of '$' and '\' and inserting '${javaVersion}' in the right places)
          // back to the templates directory (in case we manually edit the Eclipse configuration from Eclipse and want to (a) diff with the
          // previous version and (b) create new template(s) from the config)
        }
      }

      eclipse {
        classpath {
          defaultOutputDir = file('.eclipse-classes')
          file.whenMerged { cp ->
            // Fix the JRE container to contain the Java target version:
            def jreContainer = cp.entries.findAll { entry -> entry.kind == 'con' && entry.path == 'org.eclipse.jdt.launching.JRE_CONTAINER' }[0]
            jreContainer.path = "org.eclipse.jdt.launching.JRE_CONTAINER/org.eclipse.jdt.internal.debug.ui.launcher.StandardVMType/JavaSE-${targetCompatibility}"
          }
        }
      }
    }
  }
}

