/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.ext;


import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import ch.dals.buildtools.gradle.core.util.PropUtil;
import ch.dals.buildtools.lib.util.CheckArg;


/**
 * Base class for extensions which represent a node in the group tree.
 *
 * @author zisch
 */
public abstract class AbstractGroupNode extends AbstractNamedNode implements AbstractGroupNodeInfo {

  // ---- Fields

  private String mPackageName;

  private String mAcronym;

  private String mDescription;

  private URI mHomeUrl;

  // ---- Constructor

  /**
   * Default constructor.
   */
  protected AbstractGroupNode () {
    super();
  }

  // ---- Methods

  /**
   * {@inheritDoc}
   */
  @Override
  public String getPackageName () {
    return mPackageName; // FIXME: must be able to derive the package name from the name!
  }

  /**
   * Sets the group name of this node.
   *
   * @param packageName the package name of this node (a valid Java name, not {@code null}), for example
   *          {@code "ch.dals.scf.core"}
   */
  public void setPackageName (final CharSequence packageName) {
    mPackageName = checkPackageName(packageName);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getAcronym () {
    return mAcronym;
  }

  /**
   * Sets the acronym of this node.
   *
   * @param acronym an optional human readable acronym (or {@code null} if unspecified), for example {@code "SCF"}
   */
  public void setAcronym (final CharSequence acronym) {
    mAcronym = PropUtil.normalize(true, acronym, null);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getLabelWithAcronym () {
    return mAcronym == null ? getLabel() : getLabel() + " (" + mAcronym + ")";
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getAcronymOrLabel () {
    return mAcronym == null ? getLabel() : mAcronym;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getDescription () {
    return mDescription;
  }

  /**
   * Sets the description of this node.
   *
   * @param description an optional human readable description (or {@code null} if unspecified)
   */
  public void setDescription (final CharSequence description) {
    mDescription = PropUtil.normalize(true, description, null);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public URI getHomeUrl () {
    return mHomeUrl;
  }

  /**
   * Sets the home URL of this node.
   *
   * @param homeUrl an optional URL of the nodes WWW homepage (or {@code null} if unspecified)
   *
   * @throws URISyntaxException If the given string violates RFC 2396, as augmented by {@link URI} (see
   *           {@link URI#URI(String)})
   */
  public void setHomeUrl (final CharSequence homeUrl) throws URISyntaxException {
    mHomeUrl = homeUrl == null ? null : new URI(homeUrl.toString());
  }

  /**
   * Sets the home URL of this node.
   *
   * @param homeUrl an optional URL of the nodes WWW homepage (or {@code null} if unspecified)
   *
   * @throws URISyntaxException if this URL is not formatted strictly according to to RFC2396 and cannot be converted to
   *           a URI (see {@link URL#toURI()} and {@link URI})
   */
  public void setHomeUrl (final URL homeUrl) throws URISyntaxException {
    mHomeUrl = homeUrl == null ? null : homeUrl.toURI();
  }

  /**
   * Sets the home URL of this node.
   *
   * @param homeUrl an optional URL of the nodes WWW homepage (or {@code null} if unspecified)
   */
  public void setHomeUrl (final URI homeUrl) {
    mHomeUrl = homeUrl;
  }

  // ---- Private Helper Methods

  /**
   * Normalizes the specified {@code packageName} and checks if it is a valid Java name.
   *
   * @param packageName the package name to check (may be {@code null})
   *
   * @return the normalized package name (possibly {@code null})
   *
   * @throws IllegalArgumentException if the normalized package name is not a valid Java name
   */
  public static String checkPackageName (final CharSequence packageName) {
    final String normalized = PropUtil.normalize(true, packageName, null);
    if (normalized != null) {
      CheckArg.isJavaName("group", normalized);
    }
    return normalized;
  }
}
