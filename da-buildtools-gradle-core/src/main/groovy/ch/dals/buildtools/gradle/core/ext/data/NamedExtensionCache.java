/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.ext.data;


import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.gradle.api.Project;

import ch.dals.buildtools.lib.util.CheckArg;
import ch.dals.buildtools.lib.util.properties.PropertyReader;


/**
 * Loads predefined data for different type of extensions ("data objects").
 *
 * @author zisch
 */
final class NamedExtensionCache {

  private static final String BASEPKG = NamedExtensionCache.class.getPackage().getName();

  private static final String RSC_PREFIX = BASEPKG.replace('.', '/') + '/';

  private static final String RSC_NAME = "da-named-extensions.properties";

  private static final String RSC_PATH = RSC_PREFIX + RSC_NAME;

  private static final String CLASS_PREFIX = BASEPKG + ".Da";

  private static final String CLASS_SUFFIX = "Extension";

  private final Project mProject;

  private final AtomicReference<Map<Class<?>, Map<String, Map<String, PropertyEntry>>>> mLoadedData = new AtomicReference<>();

  /**
   * Constructor.
   *
   * @param project the Gradle project for which the predefined data should be loaded
   */
  NamedExtensionCache (final Project project) {
    mProject = CheckArg.notNull("project", project);
  }

  Map<String, PropertyEntry> getData (final Class<?> extensionType, final CharSequence name) {
    CheckArg.notNull("extensionType", extensionType);
    CheckArg.notEmpty("name", name);
    Map<Class<?>, Map<String, Map<String, PropertyEntry>>> loadedData = mLoadedData.get();
    if (loadedData == null) {
      loadedData = loadData();
      if (!mLoadedData.compareAndSet(null, loadedData)) {
        loadedData = mLoadedData.get();
      }
    }
    Map<String, PropertyEntry> data = null;
    final Map<String, Map<String, PropertyEntry>> typeData = loadedData.get(extensionType);
    if (typeData != null) {
      data = typeData.get(name.toString());
    }
    if (data == null) {
      throw new IllegalArgumentException("Cannot find predefined named extension of type '" + extensionType.getName()
              + "' with name '" + name + "'.");
    }
    return data;
  }

  private Map<Class<?>, Map<String, Map<String, PropertyEntry>>> loadData () {
    try {
      final Map<Class<?>, Map<String, Map<String, PropertyEntry>>> result = new HashMap<>();
      final ClassLoader cl = NamedExtensionCache.class.getClassLoader();
      final Enumeration<URL> resources = cl.getResources(RSC_PATH);
      while (resources.hasMoreElements()) {
        loadData(result, resources.nextElement());
      }
      loadProjectCustomData(result);
      return Collections.unmodifiableMap(result);
    } catch (final IOException exc) {
      throw new IllegalStateException("Failed to load predefined data: " + exc, exc);
    }
  }

  private void loadData (final Map<Class<?>, Map<String, Map<String, PropertyEntry>>> result, final URL resource)
          throws IOException {
    try (final InputStream is = resource.openStream()) {
      final Map<String, String> values = (new PropertyReader(is))
              .readRemainingProperties(new TreeMap<String, String>());
      for (final Map.Entry<String, String> v : values.entrySet()) {
        try {
          processProperty(resource.toURI(), result, v.getKey(), v.getValue());
        } catch (final IOException | URISyntaxException exc) {
          throw new IOException("Failed to process property '" + v.getKey() + "' in data file '" + resource + "': "
                  + exc, exc);
        }
      }
    } catch (final IOException exc) {
      throw new IOException("Failed to load data from resource file '" + resource + "': " + exc, exc);
    }
  }

  private void loadProjectCustomData (final Map<Class<?>, Map<String, Map<String, PropertyEntry>>> result)
          throws IOException {
    final Path propertiesFile = mProject.file(RSC_NAME).toPath();
    if (Files.isReadable(propertiesFile)) {
      final Map<String, String> values = PropertyReader.readProperties(propertiesFile);
      for (final Map.Entry<String, String> v : values.entrySet()) {
        try {
          processProperty(propertiesFile.toUri(), result, v.getKey(), v.getValue());
        } catch (final IOException exc) {
          throw new IOException("Failed to process property '" + v.getKey() + "' in data file '" + propertiesFile
                  + "': " + exc, exc);
        }
      }
    }
  }

  private static final String FILE_PREFIX = "@file:";

  private static final Pattern sKeyPattern = Pattern.compile("([0-9A-Za-z]+)\\.([0-9A-Za-z_$-]+)\\.([0-9A-Za-z]+)");

  private void processProperty (final URI fileUrl, final Map<Class<?>, Map<String, Map<String, PropertyEntry>>> result,
          final String key, final String value) throws IOException {
    final Matcher m = sKeyPattern.matcher(key);
    if (!m.matches()) {
      throw new IOException("Invalid key which does not match pattern '" + sKeyPattern + "': '" + key + "'");
    }
    final String typeName = m.group(1);
    final String dataId = m.group(2);
    final String propertyName = m.group(3);
    final Class<?> type = classForName(fileUrl, typeName);
    String resolvedValue = value;
    if (resolvedValue != null && resolvedValue.startsWith(FILE_PREFIX)) {
      final String relUrl = resolvedValue.substring(FILE_PREFIX.length());
      final URI contentFile = fileUrl.resolve(relUrl);
      resolvedValue = loadContentFile(fileUrl, key, contentFile);
    }

    Map<String, Map<String, PropertyEntry>> typeMap = result.get(type);
    if (typeMap == null) {
      typeMap = new TreeMap<>();
      result.put(type, typeMap);
    }
    Map<String, PropertyEntry> idMap = typeMap.get(dataId);
    if (idMap == null) {
      idMap = new TreeMap<>();
      typeMap.put(dataId, idMap);
    }
    idMap.put(propertyName, new PropertyEntry(fileUrl, key, resolvedValue));
  }

  private String loadContentFile (final URI fileUrl, final String key, final URI contentFile) throws IOException {
    final StringBuilder sb = new StringBuilder(1024);
    final char[] cbuf = new char[8 * 1024];
    try (final Reader r = new InputStreamReader(contentFile.toURL().openStream(), "UTF-8")) {
      for (int cnt = r.read(cbuf); cnt > 0; cnt = r.read(cbuf)) {
        sb.append(cbuf, 0, cnt);
      }
    } catch (final IOException exc) {
      throw new IOException("Failed to read content file '" + contentFile + "' referenced by '" + fileUrl + "#" + key
              + "'.");
    }
    return sb.toString();
  }

  private Class<?> classForName (final URI fileUrl, final String name) throws IOException {
    final String cn = CLASS_PREFIX + name.substring(0, 1).toUpperCase(Locale.US) + name.substring(1) + CLASS_SUFFIX;
    try {
      return Class.forName(cn);
    } catch (final ClassNotFoundException exc) {
      throw new IOException("Invalid type name '" + name + "' in '" + fileUrl + "'");
    }
  }

  static final class PropertyEntry {
    private final URI mSourceFile;

    private final String mSourceKey;

    private final String mValue;

    private PropertyEntry (final URI sourceFile, final String sourceKey, final String value) {
      mSourceFile = sourceFile;
      mSourceKey = sourceKey;
      mValue = value;
    }

    String getValue () {
      return mValue;
    }

    @Override
    public String toString () {
      return mSourceFile + "#" + mSourceKey;
    }
  }
}
