/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.plugin.java;


import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import ch.dals.buildtools.lib.idxrsc.IndexedResources;
import ch.dals.buildtools.lib.util.CheckArg;


/**
 * TODO [javadoc]: type IndexedResourcesDirectory
 *
 * @author zisch
 */
public class IndexedResourcesDirectory {
  private final List<Path> mRootDirectories;

  private final boolean mIncludeSubdirectories;

  private final Path mIndexFile;

  /**
   * TODO [javadoc]: constructor IndexedResourcesDirectory
   *
   * @param rootDirectories TODO [javadoc]
   * @param includeSubdirectories TODO [javadoc]
   * @param indexFile TODO [javadoc]
   */
  public IndexedResourcesDirectory (final Collection<? extends Path> rootDirectories,
          final boolean includeSubdirectories, final Path indexFile) {
    CheckArg.noNulls("rootDirectories", rootDirectories);
    mRootDirectories = Collections.unmodifiableList(new ArrayList<>(rootDirectories));
    mIncludeSubdirectories = includeSubdirectories;
    mIndexFile = indexFile == null ? Paths.get(IndexedResources.DEFAULT_INDEX_NAME) : indexFile;
  }

  /**
   * @return the root directory
   */
  public List<Path> getRootDirectories () {
    return mRootDirectories;
  }

  /**
   * @return the includeSubdirectories flag
   */
  public boolean isIncludeSubdirectories () {
    return mIncludeSubdirectories;
  }

  /**
   * @return the index file name or path
   */
  public Path getIndexFile () {
    return mIndexFile;
  }
}
