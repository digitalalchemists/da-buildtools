/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.util

import java.nio.file.Files





/**
 * TODO [javadoc]: type RscUtil
 *
 * @author zisch
 */
public final class RscUtil {

  private static final InheritableThreadLocal<Map<?, ?>> sTokens = new InheritableThreadLocal<>();

  private  static void withTokens (final Map<?, ?> tokens, final Closure<?> closure) {
    if (sTokens.get() != null) {
      throw new IllegalStateException("withTokens(...) is not reentrant!");
    }
    sTokens.set(tokens);
    try {
      closure.call();
    } finally {
      sTokens.set(null);
    }
  }

  static Map<String, String> tokens () {
    final Map<String, String> toks = sTokens.get()
    if (toks == null) {
      throw new IllegalStateException("No tokens defined in current context!")
    }
    def result = new TreeMap<String, String>()
    sTokens.get().each { k, v ->
      if (v != null) {
        result[k.toString()] = v.toString()
      }
    }
    return result
  }

  public static String readTextResource (Class<?> loadingClass, def rscName) {
    assert loadingClass != null && rscName != null
    InputStream is = loadingClass.getResourceAsStream(rscName.toString())
    if (is == null) {
      return null
    }
    try {
      return is.getText("UTF-8")
    } finally {
      is.close()
    }
  }

  public static void unzipClasspathResource (Class<?> loadingClass, def rscName, def dest, Map tokens) {
    assert loadingClass != null && rscName != null
    def tempArch = Files.createTempFile("${RscUtil.class.simpleName}-", ".zip").toFile()
    try {
      InputStream is = loadingClass.getResourceAsStream(rscName.toString())
      assert is != null : "Failed to find resource '$rscName' for loading class '$loadingClass'."
      try {
        tempArch.bytes = is.bytes
      } finally {
        is.close()
      }
      unzipFile(tempArch, dest, tokens)
    } finally {
      tempArch.delete()
    }
  }

  public static void unzipFile (def src, def dest, Map tokens) {
    assert src != null && dest != null && tokens != null
    def ab = new AntBuilder()
    withTokens(tokens) {
      ab.copy(todir: "${dest}") {
        zipfileset(src: "${src}")
        mapper(classname: UnzipMapper.class.name)
        filterchain {
          filterreader(classname: UnzipFilter.class.name)
        }
      }
    }
  }

  private RscUtil () {
    throw new AssertionError('not allowed')
  }
}

