/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.util;


import java.io.Flushable;
import java.io.IOException;
import java.util.Locale;

import org.gradle.api.Project;
import org.gradle.logging.StyledTextOutput;
import org.gradle.logging.StyledTextOutput.Style;
import org.gradle.util.GUtil;

import ch.dals.buildtools.lib.util.CheckArg;


/**
 * TODO [javadoc]: type TextReportRenderer
 *
 * @author zisch
 *
 * @see org.gradle.api.tasks.diagnostics.internal.TextReportRenderer
 */
public class GradleReportRenderer {
  private static final String SEPARATOR = "------------------------------------------------------------";

  private static final String INDENT = "  ";

  private final StyledTextOutput mOutput;

  private boolean mAutoflush = true;

  private String mLinesep = System.getProperty("line.separator");

  private int mIndent = 0;

  private Style mStyle = Style.Normal;

  private boolean mStartLine = true;

  private boolean mSawCr = false;

  public GradleReportRenderer (final StyledTextOutput output) {
    mOutput = CheckArg.notNull("output", output);
    mOutput.style(mStyle);
  }

  public StyledTextOutput output () {
    return mOutput;
  }

  public boolean autoflush () {
    return mAutoflush;
  }

  public GradleReportRenderer autoflush (final boolean autoflush) {
    mAutoflush = autoflush;
    return this;
  }

  public String linesep () {
    return mLinesep;
  }

  public GradleReportRenderer linesep (final String linesep) {
    mLinesep = CheckArg.notNull("linesep", linesep);
    return this;
  }

  public int indent () {
    return mIndent;
  }

  public GradleReportRenderer indent (final int indent) {
    CheckArg.notNegative("indent", indent);
    mIndent = indent;
    return this;
  }

  public GradleReportRenderer incIndent () {
    return indent(indent() + 1);
  }

  public GradleReportRenderer decIndent () {
    return indent(indent() - 1);
  }

  public Style style () {
    return mStyle;
  }

  public GradleReportRenderer style (final Style style) {
    mStyle = CheckArg.notNull("style", style);
    mOutput.style(mStyle);
    return this;
  }

  public GradleReportRenderer printHeader (final Project project) {
    String heading;
    if (project.getRootProject() == project) {
      heading = "Root project";
    } else {
      heading = String.format("Project %s", project.getPath());
    }
    if (GUtil.isTrue(project.getDescription())) {
      heading = heading + " - " + project.getDescription();
    }
    printHeader(heading);
    return this;
  }

  public GradleReportRenderer printHeader (final CharSequence heading) {
    println();
    style(Style.Header);
    println(SEPARATOR);
    println(heading);
    println(SEPARATOR);
    style(Style.Normal);
    println();
    return this;
  }

  public GradleReportRenderer printSubheader (final CharSequence heading) {
    style(Style.Header);
    println(heading);
    for (int i = 0; i < heading.length(); i++) {
      print('-');
    }
    println();
    style(Style.Normal);
    return this;
  }

  public GradleReportRenderer printProperty (final CharSequence name, final Object value) {
    return style(Style.Identifier).print(name).print(':').style(Style.Normal).print(' ')
            .println(value == null ? "null" : value.toString());
  }

  public GradleReportRenderer printComplexPropertyStart (final CharSequence name) {
    return style(Style.Identifier).print(name).style(Style.Normal).println(" {").incIndent();
  }

  public GradleReportRenderer printComplexPropertyEnd () {
    return decIndent().println("}");
  }

  public GradleReportRenderer printFormatted (final Style style, final CharSequence pattern, final Object... args) {
    return printFormatted(style, Locale.US, pattern, args);
  }

  public GradleReportRenderer printFormatted (final Style style, final Locale locale, final CharSequence pattern,
          final Object... args) {
    CheckArg.notNull("style", style);
    final Style old = style();
    return style(style).printFormatted(locale, pattern, args).style(old);
  }

  public GradleReportRenderer printFormatted (final CharSequence pattern, final Object... args) {
    return printFormatted(Locale.US, pattern, args);
  }

  public GradleReportRenderer printFormatted (final Locale locale, final CharSequence pattern, final Object... args) {
    CheckArg.notNull("pattern", pattern);
    CheckArg.notNull("args", args);
    final Locale l = locale == null ? Locale.getDefault() : locale;
    final String text = String.format(l, pattern.toString(), args);
    return print(text);
  }

  public GradleReportRenderer println (final Style style, final CharSequence text) {
    CheckArg.notNull("style", style);
    final Style old = style();
    return style(style).println(text).style(old);
  }

  public GradleReportRenderer println (final CharSequence text) {
    return print(text).println();
  }

  public GradleReportRenderer println () {
    mOutput.append(mLinesep);
    if (mAutoflush) {
      try {
        flush();
      } catch (final IOException exc) {
        throw new IllegalStateException("Unexpected IOException: " + exc);
      }
    }
    mStartLine = true;
    return this;
  }

  public GradleReportRenderer print (final CharSequence text) {
    CheckArg.notNull("text", text);
    final int len = text.length();
    for (int i = 0; i < len; i++) {
      print(text.charAt(i));
    }
    return this;
  }

  public GradleReportRenderer print (final char c) {
    if (mSawCr) {
      println();
      switch (c) {
        case '\r':
          break;

        case '\n':
          mSawCr = false;
          break;

        default:
          mSawCr = false;
          printChar(c);
      }

    } else {
      switch (c) {
        case '\r':
          mSawCr = true;
          break;

        case '\n':
          println();
          break;

        default:
          printChar(c);
      }
    }
    return this;
  }

  private void printChar (final char c) {
    if (mStartLine && mIndent > 0) {
      for (int i = 0; i < mIndent; i++) {
        mOutput.append(INDENT);
      }
      mStartLine = false;
    }
    mOutput.append(c);
  }

  public GradleReportRenderer flush () throws IOException {
    if (mOutput instanceof Flushable) {
      ((Flushable) mOutput).flush();
    }
    return this;
  }
}
