/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.util;


import java.io.FilterReader;
import java.io.Reader;
import java.util.Map;

import org.apache.tools.ant.filters.ReplaceTokens;

import ch.dals.buildtools.lib.util.AtomicLazyFactory;


/**
 * TODO [javadoc]: type UnzipFilter
 *
 * @author zisch
 */
public class UnzipFilter extends FilterReader {
  private AtomicLazyFactory<Map<String, String>> mTokens = new AtomicLazyFactory<Map<String, String>>() {
    @Override
    protected Map<String, String> produce () {
      return RscUtil.tokens();
    }
  };

  /**
   * TODO [javadoc]: constructor UnzipFilter
   *
   * @param in TODO [javadoc]
   */
  public UnzipFilter (final Reader in) {
    super(new ReplaceTokens(in));
    final ReplaceTokens rt = (ReplaceTokens) this.in;
    for (final Map.Entry<String, String> e : mTokens.get().entrySet()) {
      final ReplaceTokens.Token token = new ReplaceTokens.Token();
      token.setKey(String.valueOf(e.getKey()));
      token.setValue(String.valueOf(e.getValue()));
      rt.addConfiguredToken(token);
    }
  }
}
