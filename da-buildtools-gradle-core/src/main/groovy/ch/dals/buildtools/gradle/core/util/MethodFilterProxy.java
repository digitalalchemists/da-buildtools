/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.util;


import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import ch.dals.buildtools.lib.util.CheckArg;
import ch.dals.buildtools.lib.util.HashUtil;


/**
 * TODO [javadoc]: type MethodFilterProxy
 *
 * @author zisch
 */
public class MethodFilterProxy implements InvocationHandler {

  /**
   * TODO [javadoc]: method wrap
   *
   * @param <I> TODO [javadoc]
   * @param targetObject TODO [javadoc]
   * @param mainInterface TODO [javadoc]
   * @param supplementalInterfaces TODO [javadoc]
   *
   * @return TODO [javadoc]
   */
  public static <I> I wrap (final Object targetObject, final Class<I> mainInterface,
          final Class<?>... supplementalInterfaces) {
    CheckArg.notNull("targetObject", targetObject);
    CheckArg.notNull("mainInterface", mainInterface);
    CheckArg.noNulls("supplementalInterfaces", supplementalInterfaces);
    final MethodFilterProxy h = new MethodFilterProxy(targetObject);
    final Class<?>[] interfaces = new Class<?>[supplementalInterfaces.length + 1];
    interfaces[0] = mainInterface;
    System.arraycopy(supplementalInterfaces, 0, interfaces, 1, supplementalInterfaces.length);
    return mainInterface.cast(Proxy.newProxyInstance(mainInterface.getClassLoader(), interfaces, h));
  }

  private final Object mTargetObject;

  private final Map<MethodKey, Method> mTargetMethods;

  private MethodFilterProxy (final Object targetObject, final Class<?>... interfaces) {
    mTargetObject = CheckArg.notNull("targetObject", targetObject);
    final Map<MethodKey, Method> targetMethods = new LinkedHashMap<>();
    targetMethods.put(new MethodKey(objectMethod(Object.class, "hashCode")),
            objectMethod(targetObject.getClass(), "hashCode"));
    targetMethods.put(new MethodKey(objectMethod(Object.class, "equals", Object.class)),
            objectMethod(targetObject.getClass(), "equals", Object.class));
    targetMethods.put(new MethodKey(objectMethod(Object.class, "toString")),
            objectMethod(targetObject.getClass(), "toString"));
    for (final Class<?> ifc : interfaces) {
      final Method[] methods = ifc.getMethods();
      for (final Method m : methods) {
        final MethodKey mk = new MethodKey(m);
        if (!targetMethods.containsKey(mk)) {
          Method tm;
          try {
            tm = targetObject.getClass().getMethod(mk.mMethodName, mk.mParameterTypes);
          } catch (final NoSuchMethodException exc) {
            throw new IllegalArgumentException("targetObject of type '" + targetObject.getClass().getName()
                    + "' does not implement method '" + mk + "' of interface '" + ifc.getName() + "'.", exc);
          }
          final Class<?> rt = tm.getReturnType();
          if (!mk.mReturnType.isAssignableFrom(rt)) {
            throw new IllegalArgumentException("Method '" + mk + "' of targetObject of type '"
                    + targetObject.getClass().getName() + "' has return type '" + rt.getName()
                    + "' which is not equal to or a subclass of '" + mk.mReturnType.getName()
                    + "' as declared in interface '" + ifc.getName() + "'.");
          }
          targetMethods.put(mk, tm);
        }
      }
    }
    mTargetMethods = Collections.unmodifiableMap(targetMethods);
  }

  private Method objectMethod (final Class<?> declaringClass, final String methodName, final Class<?>... parameterTypes) {
    try {
      return declaringClass.getMethod(methodName, parameterTypes);
    } catch (final NoSuchMethodException exc) {
      throw new IllegalStateException("This should not happen: Cannot find '"
              + MethodKey.toString(methodName, parameterTypes) + "' in class '" + declaringClass.getName() + "'.", exc);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object invoke (final Object proxy, final Method method, final Object[] args) throws Throwable {
    final MethodKey mk = new MethodKey(method);
    final Method m = mTargetMethods.get(mk);
    if (m == null) {
      // Since we checked that every
      throw new IllegalStateException("This should not happen: Cannot find method '" + mk.mReturnType.getName() + " "
              + mk + "' in ");
    }
    return m.invoke(mTargetObject, args);
  }

  private static final class MethodKey {
    private final String mMethodName;

    private final Class<?>[] mParameterTypes;

    private final Class<?> mReturnType;

    private MethodKey (final Method method) {
      mMethodName = method.getName();
      mParameterTypes = method.getParameterTypes();
      mReturnType = method.getReturnType();
    }

    @Override
    public boolean equals (final Object o) {
      if (o instanceof MethodKey) {
        final MethodKey mk = (MethodKey) o;
        return mMethodName.equals(mk.mMethodName) && Arrays.equals(mParameterTypes, mk.mParameterTypes)
                && mReturnType.equals(mk.mReturnType);
      } else {
        return false;
      }
    }

    @Override
    public int hashCode () {
      return HashUtil.hash(mMethodName.hashCode(), Arrays.hashCode(mParameterTypes), mReturnType.hashCode());
    }

    @Override
    public String toString () {
      return toString(mMethodName, mParameterTypes);
    }

    private static String toString (final String methodName, final Class<?>... parameterTypes) {
      final StringBuilder sb = new StringBuilder(methodName.length() + parameterTypes.length * 16);
      sb.append(methodName).append('(');
      String sep = "";
      for (final Class<?> pt : parameterTypes) {
        sb.append(sep).append(pt.getName());
        sep = ", ";
      }
      sb.append(')');
      return sb.toString();
    }
  }
}
