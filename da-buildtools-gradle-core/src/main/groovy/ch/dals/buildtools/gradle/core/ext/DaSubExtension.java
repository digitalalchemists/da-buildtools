/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.ext;


import org.gradle.api.Project;

import ch.dals.buildtools.gradle.core.ext.data.NamedExtensionLoader;
import ch.dals.buildtools.gradle.core.util.MethodFilterProxy;


/**
 * TODO [javadoc]: type DaSubExtension
 *
 * @author zisch
 */
public final class DaSubExtension extends AbstractDaExtension {

  private DaOrganizationInfo mOrganizationInfo = null;

  private DaProjectInfo mProjectInfo = null;

  /**
   * Constructor.
   *
   * @param gradleProject the Gradle project in which this extension is installed
   */
  public DaSubExtension (final Project gradleProject) {
    super(gradleProject);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public DaOrganizationInfo getOrganization () {
    if (mOrganizationInfo == null) {
      mOrganizationInfo = MethodFilterProxy.wrap(getRootExtension().getOrganization(), DaOrganizationInfo.class);
    }
    return mOrganizationInfo;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public DaProjectInfo getProject () {
    if (mProjectInfo == null) {
      mProjectInfo = MethodFilterProxy.wrap(getRootExtension().getProject(), DaProjectInfo.class);
    }
    return mProjectInfo;
  }

  @Override
  DaRootExtension getRootExtension () {
    return DaRootExtension.instance(getGradleProject());
  }

  @Override
  NamedExtensionLoader getLoader () {
    return getRootExtension().getLoader();
  }
}
