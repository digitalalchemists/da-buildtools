/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.util.testing.files;


import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import ch.dals.buildtools.lib.util.CheckArg;


/**
 * TODO [javadoc]: type FileTreeDiff
 *
 * @author zisch
 */
public final class FileTreeDiff {

  public enum Result {
    MATCH, LEFT_MISSING, RIGHT_MISSING, DIFFERENT_TYPES, DIFFERENT_FILES, DIFFERENT_LINKS
  }

  public static Map<Path, Result> diffRecursive (final Path leftSide, final Path rightSide) throws IOException {
    final Map<Path, Result> result = new TreeMap<>();
    internalDiffRecursive(result, leftSide, leftSide, rightSide, rightSide);
    return result;
  }

  public static void internalDiffRecursive (final Map<Path, Result> result, final Path leftRoot, final Path leftSide,
          final Path rightRoot, final Path rightSide) throws IOException {
    final Result r = diff(leftSide, rightSide);
    if (r == Result.MATCH) {
      if (Files.isDirectory(leftSide, LinkOption.NOFOLLOW_LINKS)) {
        final Set<String> allChildren = new TreeSet<>();
        childNames(allChildren, leftSide);
        childNames(allChildren, rightSide);
        for (final String cn : allChildren) {
          final Path leftChild = leftSide.resolve(cn);
          final Path rightChild = rightSide.resolve(cn);
          internalDiffRecursive(result, leftRoot, leftChild, rightRoot, rightChild);
        }
      }
    } else {
      final Path relPath = leftRoot.relativize(leftSide);
      result.put(relPath, r);
    }
  }

  private static void childNames (final Set<String> result, final Path dir) throws IOException {
    try (final DirectoryStream<Path> ds = Files.newDirectoryStream(dir)) {
      for (final Path p : ds) {
        result.add(p.getFileName().toString());
      }
    }
  }

  public static Result diff (final Path leftSide, final Path rightSide) throws IOException {
    CheckArg.notNull("leftSide", leftSide);
    CheckArg.notNull("rightSide", rightSide);
    final boolean leftExists = Files.exists(leftSide, LinkOption.NOFOLLOW_LINKS);
    final boolean rightExists = Files.exists(rightSide, LinkOption.NOFOLLOW_LINKS);
    if (leftExists && rightExists) {
      if (Files.isSymbolicLink(leftSide)) {
        if (Files.isSymbolicLink(rightSide)) {
          final Path leftLink = Files.readSymbolicLink(leftSide);
          final Path rightLink = Files.readSymbolicLink(rightSide);
          return leftLink.equals(rightLink) ? Result.MATCH : Result.DIFFERENT_LINKS;

        } else if (Files.isDirectory(rightSide, LinkOption.NOFOLLOW_LINKS)
                || Files.isRegularFile(rightSide, LinkOption.NOFOLLOW_LINKS)) {
          return Result.DIFFERENT_TYPES;

        } else {
          throw new IOException(
                  "Cannot process nodes which are neither symbolic links nor directories nor regular files: '"
                          + rightSide + "'");
        }

      } else if (Files.isDirectory(leftSide, LinkOption.NOFOLLOW_LINKS)) {
        if (Files.isDirectory(rightSide, LinkOption.NOFOLLOW_LINKS)) {
          return Result.MATCH; // Note that we do _not_ descend into directories in this method!

        } else if (Files.isSymbolicLink(rightSide) || Files.isRegularFile(rightSide, LinkOption.NOFOLLOW_LINKS)) {
          return Result.DIFFERENT_TYPES;

        } else {
          throw new IOException(
                  "Cannot process nodes which are neither symbolic links nor directories nor regular files: '"
                          + rightSide + "'");
        }

      } else if (Files.isRegularFile(leftSide, LinkOption.NOFOLLOW_LINKS)) {
        if (Files.isRegularFile(rightSide, LinkOption.NOFOLLOW_LINKS)) {
          final boolean eq = equalFiles(leftSide, rightSide);
          return eq ? Result.MATCH : Result.DIFFERENT_FILES;

        } else if (Files.isSymbolicLink(rightSide) || Files.isDirectory(rightSide, LinkOption.NOFOLLOW_LINKS)) {
          return Result.DIFFERENT_TYPES;

        } else {
          throw new IOException(
                  "Cannot process nodes which are neither symbolic links nor directories nor regular files: '"
                          + rightSide + "'");
        }

      } else {
        throw new IOException(
                "Cannot process nodes which are neither symbolic links nor directories nor regular files: '" + leftSide
                        + "'");
      }
    } else {
      return leftExists ? Result.RIGHT_MISSING : Result.LEFT_MISSING;
    }
  }

  private static boolean equalFiles (final Path leftFile, final Path rightFile) throws IOException {
    try (final InputStream left = new BufferedInputStream(Files.newInputStream(leftFile));
            final InputStream right = new BufferedInputStream(Files.newInputStream(rightFile))) {
      boolean result;
      while (true) {
        final int l = left.read();
        final int r = right.read();
        if (l < 0 && r < 0) {
          result = true;
          break;
        } else if (l != r) {
          result = false;
          break;
        }
      }
      return result;
    }
  }

  private FileTreeDiff () {
    throw new UnsupportedOperationException("not allowed");
  }
}
