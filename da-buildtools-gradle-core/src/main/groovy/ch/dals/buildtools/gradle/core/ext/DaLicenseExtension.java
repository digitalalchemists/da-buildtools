/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.ext;


import java.net.URI;
import java.net.URISyntaxException;

import ch.dals.buildtools.lib.util.CheckArg;


/**
 * Extension to configure the license applying to the project.
 *
 * @author zisch
 */
public class DaLicenseExtension extends AbstractNamedNode {
  private String mCopyrightStatement;

  private String mText;

  private URI mTextUrl;

  private boolean mOpenSource;

  /**
   * Default constructor.
   */
  public DaLicenseExtension () {
    super();
  }

  /**
   * Copy constructor which creates a copy with the same attributes as the specified {@code defaultInstance}.
   *
   * @param defaultInstance the instance to copy
   */
  public DaLicenseExtension (final DaLicenseExtension defaultInstance) {
    CheckArg.notNull("defaultInstance", defaultInstance);
  }

  /**
   * @return the copyrightStatement
   */
  public String getCopyrightStatement () {
    return mCopyrightStatement;
  }

  /**
   * @param copyrightStatement the copyrightStatement to set
   */
  public void setCopyrightStatement (final CharSequence copyrightStatement) {
    mCopyrightStatement = copyrightStatement == null ? null : copyrightStatement.toString();
  }

  /**
   * @return the text
   */
  public String getText () {
    return mText;
  }

  /**
   * @param text the text to set
   */
  public void setText (final CharSequence text) {
    mText = text == null || text.length() == 0 ? null : text.toString();
  }

  /**
   * @return the textUrl
   */
  public URI getTextUrl () {
    return mTextUrl;
  }

  /**
   * @param textUrl the URL where the orginal text of the license can be found
   */
  public void setTextUrl (final URI textUrl) {
    mTextUrl = textUrl;
  }

  /**
   * @param textUrl the URL where the orginal text of the license can be found
   *
   * @throws URISyntaxException if the specified {@code textUrl} is not a valid {@link URI}
   */
  public void setTextUrl (final CharSequence textUrl) throws URISyntaxException {
    mTextUrl = textUrl == null ? null : new URI(textUrl.toString());
  }

  /**
   * @return if this is an <a href="http://opensource.org/osd">Open Source</a> license
   */
  public boolean isOpenSource () {
    return mOpenSource;
  }

  /**
   * @param openSource if this is an <a href="http://opensource.org/osd">Open Source</a> license
   */
  public void setOpenSource (final boolean openSource) {
    mOpenSource = openSource;
  }

  /**
   * @param openSource {@code "true"} (case insensitive) if this is an <a href="http://opensource.org/osd">Open
   *          Source</a> license, any other value (including {@code null}) otherwise
   */
  public void setOpenSource (final CharSequence openSource) {
    mOpenSource = openSource != null && openSource.toString().equalsIgnoreCase("true");
  }
}
