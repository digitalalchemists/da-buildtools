/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.util;


import java.nio.file.attribute.PosixFilePermission;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

import ch.dals.buildtools.lib.util.CheckArg;


/**
 * Represents the three variants of Posix file permission types {@link #READ}, {@link #WRITE} and {@link #EXECUTE}.
 *
 * @author zisch
 *
 * @see PosixFilePermission
 * @see PosixPermissionClass
 */
public enum PosixPermissionType {
  /**
   * Read permission.
   */
  READ('r', PosixFilePermission.OWNER_READ, PosixFilePermission.GROUP_READ, PosixFilePermission.OTHERS_READ),

  /**
   * Write permission.
   */
  WRITE('w', PosixFilePermission.OWNER_WRITE, PosixFilePermission.GROUP_WRITE, PosixFilePermission.OTHERS_WRITE),

  /**
   * Execute permission.
   */
  EXECUTE('x', PosixFilePermission.OWNER_EXECUTE, PosixFilePermission.GROUP_EXECUTE, PosixFilePermission.OTHERS_EXECUTE);

  public static PosixPermissionType forFilePermission (final PosixFilePermission filePerm) {
    CheckArg.notNull("filePerm", filePerm);
    switch (filePerm) {
      case OWNER_READ:
      case GROUP_READ:
      case OTHERS_READ:
        return READ;

      case OWNER_WRITE:
      case GROUP_WRITE:
      case OTHERS_WRITE:
        return WRITE;

      case OWNER_EXECUTE:
      case GROUP_EXECUTE:
      case OTHERS_EXECUTE:
        return EXECUTE;
    }
    throw new AssertionError("Unexpected " + PosixFilePermission.class.getSimpleName() + ": " + filePerm);
  }

  private final char mSymbol;

  private final PosixFilePermission mOwnerFilePerm;

  private final PosixFilePermission mGroupFilePerm;

  private final PosixFilePermission mOthersFilePerm;

  private final Set<PosixFilePermission> mFilePermissions;

  private PosixPermissionType (final char symbol, final PosixFilePermission ownerFilePerm,
          final PosixFilePermission groupFilePerm, final PosixFilePermission othersFilePerm) {
    mSymbol = symbol;
    mOwnerFilePerm = ownerFilePerm;
    mGroupFilePerm = groupFilePerm;
    mOthersFilePerm = othersFilePerm;
    mFilePermissions = Collections.unmodifiableSet(EnumSet.of(ownerFilePerm, groupFilePerm, othersFilePerm));
  }

  /**
   * @return the symbol used in string representations of Posix file permissions ({@link #READ 'r'}, {@link #WRITE 'w'}
   *         or {@link #EXECUTE 'x'}).
   */
  public char getSymbol () {
    return mSymbol;
  }

  /**
   * @return the immutable set of the three {@link PosixFilePermission} elements corresponding to this
   *         {@link PosixPermissionType} element
   */
  public Set<PosixFilePermission> getFilePermissions () {
    return mFilePermissions;
  }

  public PosixFilePermission toFilePermission (final PosixPermissionClass permClass) {
    CheckArg.notNull("permClass", permClass);
    switch (permClass) {
      case OWNER:
        return mOwnerFilePerm;

      case GROUP:
        return mGroupFilePerm;

      case OTHERS:
        return mOthersFilePerm;
    }
    throw new AssertionError("Unexpected " + PosixPermissionClass.class.getSimpleName() + ": " + permClass);
  }
}
