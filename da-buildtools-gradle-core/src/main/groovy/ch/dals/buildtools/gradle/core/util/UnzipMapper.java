/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.util;


import java.util.Map;

import org.apache.tools.ant.util.FileNameMapper;

import ch.dals.buildtools.lib.util.AtomicLazyFactory;


/**
 * TODO [javadoc]: type UnzipMapper
 *
 * @author zisch
 */
public final class UnzipMapper implements FileNameMapper {
  private AtomicLazyFactory<Map<String, String>> mTokens = new AtomicLazyFactory<Map<String, String>>() {
    @Override
    protected Map<String, String> produce () {
      return RscUtil.tokens();
    }
  };

  /**
   * Constructor.
   */
  public UnzipMapper () {
    super();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String[] mapFileName (final String sourceFileName) {
    String targetFileName = sourceFileName;
    for (final Map.Entry<String, String> e : mTokens.get().entrySet()) {
      targetFileName = targetFileName.replace("__" + e.getKey() + "__", e.getValue());
    }
    return new String[] { targetFileName };
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setFrom (final String from) {
    // do nothing
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setTo (final String to) {
    // do nothing
  }

}
