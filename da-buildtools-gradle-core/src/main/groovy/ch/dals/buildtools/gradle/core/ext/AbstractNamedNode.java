/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.ext;


import ch.dals.buildtools.gradle.core.util.PropUtil;
import ch.dals.buildtools.lib.util.CheckArg;


/**
 * TODO [javadoc]: type AbstractGroupNode
 *
 * @author zisch
 */
public abstract class AbstractNamedNode implements AbstractNamedNodeInfo {

  // ---- Fields

  private String mName;

  private String mLabel;

  // ---- Constructor

  /**
   * Default constructor.
   */
  protected AbstractNamedNode () {
    super();
  }

  // ---- Methods

  /**
   * {@inheritDoc}
   */
  @Override
  public String getName () {
    return mName;
  }

  /**
   * Sets the name of this node.
   *
   * @param name the programmatical &ldquo;flat&rdquo; name of this node (a valid Java name, but with {@code '-'}
   *          instead of {@code '.'}; defaults to {@code group.replace('.', '-')} if {@code null} or empty), for example
   *          {@code "da-scf-core"}
   */
  public void setName (final CharSequence name) {
    mName = checkName(name);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getLabel () {
    return mLabel == null ? mName : mLabel;
  }

  /**
   * Sets the label of this node.
   *
   * @param label an optional human readable name (never {@code null} or empty), for example
   *          {@code "Secure Communication Framework"} (if no explicit label has been specified upon construction, the
   *          label will be equal to the programmatical {@link #getName() name})
   */
  public void setLabel (final CharSequence label) {
    mLabel = PropUtil.normalize(true, label, null);
  }

  // ---- Private Helper Methods

  /**
   * Normalizes the specified {@code name} and checks if it is a valid node name.
   *
   * @param name the node name to check (may be {@code null})
   *
   * @return the normalized name (possibly {@code null})
   *
   * @throws IllegalArgumentException if the normalized name contains any {@code '.'} characters or if it is not a valid
   *           Java name after replacing all {@code '-'} characters with {@code '.'}
   */
  public static String checkName (final CharSequence name) {
    final String normalized = PropUtil.normalize(true, name, null);
    if (normalized != null) {
      if (normalized.indexOf('.') >= 0) {
        throw new IllegalArgumentException("name must not contain any '.' characters: '" + name + "'");
      }
      CheckArg.isJavaName("name.replace('-', '.')", normalized.replace('-', '.'));
    }
    return normalized;
  }
}
