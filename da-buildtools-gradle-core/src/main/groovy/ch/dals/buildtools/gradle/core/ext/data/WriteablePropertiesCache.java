/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.ext.data;


import java.beans.Introspector;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import ch.dals.buildtools.lib.util.CheckArg;


/**
 * Loads predefined data for different type of extensions ("data objects").
 *
 * @author zisch
 */
final class WriteablePropertiesCache {

  private final ConcurrentMap<Class<?>, Map<String, Method>> mWriteableProperties = new ConcurrentHashMap<>();

  /**
   * Constructor.
   */
  WriteablePropertiesCache () {
    super();
  }

  Map<String, Method> getProperties (final Class<?> dataObjectType) {
    CheckArg.notNull("dataObjectType", dataObjectType);
    Map<String, Method> properties = mWriteableProperties.get(dataObjectType);
    if (properties == null) {
      properties = introspectWriteableProperties(dataObjectType);
      final Map<String, Method> existing = mWriteableProperties.putIfAbsent(dataObjectType, properties);
      properties = existing == null ? properties : existing;
    }
    return properties;
  }

  private Map<String, Method> introspectWriteableProperties (final Class<?> dataObjectType) {
    final Map<String, Method> result = new TreeMap<>();
    final Method[] methods = dataObjectType.getMethods();
    for (final Method m : methods) {
      if (!Modifier.isStatic(m.getModifiers()) && m.getName().startsWith("set") && m.getName().length() > 3
              && m.getParameterTypes().length == 1 && CharSequence.class.isAssignableFrom(m.getParameterTypes()[0])) {
        final String pn = Introspector.decapitalize(m.getName().substring(3));
        result.put(pn, m);
      }
    }
    return result.isEmpty() ? Collections.<String, Method>emptyMap() : Collections.unmodifiableMap(result);
  }
}
