/*
 * This file is part of the DA Secure Communication Framework (SCF) project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA SCF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA SCF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA SCF.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.plugin.java;


import org.gradle.api.plugins.JavaPlugin;

import ch.dals.buildtools.gradle.core.plugin.DaPlugin;
import ch.dals.buildtools.gradle.core.plugin.base.DaBasePlugin;


/**
 * Plugin for DA Java projects.
 *
 * @author zisch
 */
public final class DaJavaPlugin extends DaPlugin {
  /**
   * Default constructor.
   */
  public DaJavaPlugin () {
    super(false, true);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void configureProject () {
    getProject().getPlugins().apply(JavaPlugin.class);
    getProject().getPlugins().apply(DaBasePlugin.class);

    getProject().getTasks().add(new IndexedResourcesTask());
    // getProject().getTasks().getByName("");
  }
}
