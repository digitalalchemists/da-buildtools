/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.util;


import java.io.File;
import java.util.Set;

import org.gradle.api.Project;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.SourceSetContainer;

import ch.dals.buildtools.lib.util.CheckArg;


/**
 * TODO [javadoc]: type GradleUtil
 *
 * @author zisch
 */
public class GradleUtil {

  public static Set<File> resourceDirectories (final Project project, final String sourceSetName) {
    return sourceSet(project, sourceSetName).getResources().getSrcDirs();
  }

  public static SourceSet sourceSet (final Project project, final String sourceSetName) {
    CheckArg.notNull("project", project);
    CheckArg.notEmpty("sourceSetName", sourceSetName);
    return ((SourceSetContainer) project.property("sourceSets")).getByName(sourceSetName);
  }

  public static <T> T rootExtension (final Project project, final Class<T> type, final Object... constructionArguments) {
    CheckArg.notNull("project", project);
    return extension(project.getRootProject(), type, constructionArguments);
  }

  public static <T> T extension (final Project project, final Class<T> type, final Object... constructionArguments) {
    CheckArg.notNull("project", project);
    CheckArg.notNull("type", type);
    CheckArg.notNull("constructionArguments", constructionArguments);
    T ext = project.getExtensions().findByType(type);
    if (ext == null) {
      ext = project.getExtensions().create(type.getName(), type, constructionArguments);
    }
    return ext;
  }

  private GradleUtil () {
    throw new AssertionError("not allowed");
  }
}
