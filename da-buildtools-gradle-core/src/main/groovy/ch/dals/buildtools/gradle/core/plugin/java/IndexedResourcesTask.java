/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.plugin.java;


import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;

import ch.dals.buildtools.gradle.core.util.GradleUtil;
import ch.dals.buildtools.lib.idxrsc.IndexedResourcesGenerator;
import ch.dals.buildtools.lib.util.CheckArg;


/**
 * TODO [javadoc]: type IndexedResourcesTask
 *
 * @author zisch
 */
public class IndexedResourcesTask extends DefaultTask {

  private final List<IndexedResourcesDirectory> mDirectories = new ArrayList<>();

  /**
   * Default constructor.
   */
  public IndexedResourcesTask () {
    setGroup("Generation");
    setName("indexedResources");
  }

  /**
   * @return modifiable list of currently configured root directories containing indexed resources
   */
  public List<IndexedResourcesDirectory> getDirectories () {
    return mDirectories;
  }

  /**
   * Adds a root directory containing indexed resources in the resources of the {@code main} source set, indexing any
   * subdirectories. This is a shortcut for {@link #directory(Object, Object, boolean, Object) directory("main",
   * packageName, true, indexFile)}.
   *
   * @param packageName the name of the package containing the resources
   */
  public void mainDirectory (final Object packageName) {
    directory("main", packageName, true, null);
  }

  /**
   * Adds a root directory containing indexed resources in the resources of the {@code main} source set, indexing any
   * subdirectories. This is a shortcut for {@link #directory(Object, Object, boolean, Object) directory("main",
   * packageName, true, indexFile)}.
   *
   * @param packageName the name of the package containing the resources
   * @param indexFile the target index file to generate (an existing file will be overwritten); this may be {@code null}
   *          for the default index file in the test resource target directory
   */
  public void mainDirectory (final Object packageName, final Object indexFile) {
    directory("main", packageName, true, indexFile);
  }

  /**
   * Adds a root directory containing indexed resources in the resources of the {@code main} source set with the default
   * index file. This is a shortcut for {@link #directory(Object, Object, boolean, Object) directory("main",
   * packageName, includeSubdirectories, null)}.
   *
   * @param packageName the name of the package containing the resources
   * @param includeSubdirectories if the subdirectories (i.e. subpackages) of the root directory should also be indexed
   */
  public void mainDirectory (final Object packageName, final boolean includeSubdirectories) {
    directory("main", packageName, includeSubdirectories, null);
  }

  /**
   * Adds a root directory containing indexed resources in the resources of the {@code main} source set. This is a
   * shortcut for {@link #directory(Object, Object, boolean, Object) directory("main", packageName,
   * includeSubdirectories, indexFile)}.
   *
   * @param packageName the name of the package containing the resources
   * @param includeSubdirectories if the subdirectories (i.e. subpackages) of the root directory should also be indexed
   * @param indexFile the target index file to generate (an existing file will be overwritten); this may be {@code null}
   *          for the default index file in the test resource target directory
   */
  public void mainDirectory (final Object packageName, final boolean includeSubdirectories, final Object indexFile) {
    directory("main", packageName, includeSubdirectories, indexFile);
  }

  /**
   * Adds a root directory containing indexed resources in the resources of the {@code test} source set, indexing any
   * subdirectories. This is a shortcut for {@link #directory(Object, Object, boolean, Object) directory("test",
   * packageName, true, indexFile)}.
   *
   * @param packageName the name of the package containing the resources
   */
  public void testDirectory (final Object packageName) {
    directory("test", packageName, true, null);
  }

  /**
   * Adds a root directory containing indexed resources in the resources of the {@code test} source set, indexing any
   * subdirectories. This is a shortcut for {@link #directory(Object, Object, boolean, Object) directory("test",
   * packageName, true, indexFile)}.
   *
   * @param packageName the name of the package containing the resources
   * @param indexFile the target index file to generate (an existing file will be overwritten); this may be {@code null}
   *          for the default index file in the test resource target directory
   */
  public void testDirectory (final Object packageName, final Object indexFile) {
    directory("test", packageName, true, indexFile);
  }

  /**
   * Adds a root directory containing indexed resources in the resources of the {@code test} source set with the default
   * index file. This is a shortcut for {@link #directory(Object, Object, boolean, Object) directory("test",
   * packageName, includeSubdirectories, null)}.
   *
   * @param packageName the name of the package containing the resources
   * @param includeSubdirectories if the subdirectories (i.e. subpackages) of the root directory should also be indexed
   */
  public void testDirectory (final Object packageName, final boolean includeSubdirectories) {
    directory("test", packageName, includeSubdirectories, null);
  }

  /**
   * Adds a root directory containing indexed resources in the resources of the {@code test} source set. This is a
   * shortcut for {@link #directory(Object, Object, boolean, Object) directory("test", packageName,
   * includeSubdirectories, indexFile)}.
   *
   * @param packageName the name of the package containing the resources
   * @param includeSubdirectories if the subdirectories (i.e. subpackages) of the root directory should also be indexed
   * @param indexFile the target index file to generate (an existing file will be overwritten); this may be {@code null}
   *          for the default index file in the test resource target directory
   */
  public void testDirectory (final Object packageName, final boolean includeSubdirectories, final Object indexFile) {
    directory("test", packageName, includeSubdirectories, indexFile);
  }

  public void directory (final Object sourceSetName, final Object packageName) {
    directory(sourceSetName, packageName, true, null);
  }

  public void directory (final Object sourceSetName, final Object packageName, final Object indexFile) {
    directory(sourceSetName, packageName, true, indexFile);
  }

  public void directory (final Object sourceSetName, final Object packageName, final boolean includeSubdirectories) {
    directory(sourceSetName, packageName, includeSubdirectories, null);
  }

  public void directory (final Object sourceSetName, final Object packageName, final boolean includeSubdirectories,
          final Object indexFile) {
    CheckArg.notNull("sourceSetName", sourceSetName);
    CheckArg.notNull("packageName", packageName);
    final String pkgName = packageName.toString();
    CheckArg.isJavaName("packageName", pkgName);
    final String[] pkgComps = pkgName.split("\\.");
    final Set<File> resourceDirectories = GradleUtil.resourceDirectories(getProject(), sourceSetName.toString());
    final List<Path> rootDirectories = new ArrayList<>(resourceDirectories.size());
    Path firstDir = null;
    for (final File rd : resourceDirectories) {
      final Path rp = rd.toPath();
      if (firstDir == null) {
        firstDir = rp;
      }
      final Path rootDir = rp.resolve(pkgName.replace(".", rp.getFileSystem().getSeparator()));
      if (Files.isDirectory(rootDir)) {
        rootDirectories.add(rootDir);
      }
    }
    if (!rootDirectories.isEmpty()) {
      final Path indexFilePath = toPath(indexFile);
      final IndexedResourcesDirectory dir = new IndexedResourcesDirectory(rootDirectories, includeSubdirectories,
              indexFilePath);
      mDirectories.add(dir);
    }
  }

  private static Path toPath (final Object o) {
    if (o == null) {
      return null;
    } else if (o instanceof Path) {
      return (Path) o;
    } else if (o instanceof File) {
      return ((File) o).toPath();
    } else if (o instanceof URI) {
      return Paths.get((URI) o);
    } else if (o instanceof URL) {
      try {
        return Paths.get(((URL) o).toURI());
      } catch (final URISyntaxException exc) {
        throw new IllegalArgumentException("Invalid URL syntax: " + exc, exc);
      }
    } else {
      return Paths.get(o.toString());
    }
  }

  /**
   * The task action.
   *
   * @throws IOException in case of I/O errors
   */
  @TaskAction
  public void executeTask () throws IOException {
    for (final IndexedResourcesDirectory dir : mDirectories) {
      final IndexedResourcesGenerator gen = new IndexedResourcesGenerator(dir.getRootDirectories(),
              dir.isIncludeSubdirectories(), null, dir.getIndexFile());
      gen.generateIndex();
    }
  }
}
