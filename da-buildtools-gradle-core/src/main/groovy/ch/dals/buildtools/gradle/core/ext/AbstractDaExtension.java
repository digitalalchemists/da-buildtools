/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.ext;


import groovy.lang.Closure;

import org.gradle.api.Project;
import org.gradle.logging.StyledTextOutput.Style;
import org.gradle.util.ConfigureUtil;

import ch.dals.buildtools.gradle.core.ext.data.NamedExtensionLoader;
import ch.dals.buildtools.gradle.core.util.GradleReportRenderer;
import ch.dals.buildtools.lib.util.CheckArg;


/**
 * Common base class of {@link DaRootExtension} and {@link DaSubExtension}.
 *
 * @author zisch
 */
public abstract class AbstractDaExtension {

  // ---- Fields

  private final Project mGradleProject;

  private final DaBuildExtension mBuild;

  private final DaLicenseExtension mLicense;

  // ---- Constructors

  /**
   * Constructor.
   *
   * @param gradleProject the Gradle project in which this extension is installed
   */
  protected AbstractDaExtension (final Project gradleProject) {
    mGradleProject = CheckArg.notNull("gradleProject", gradleProject);
    if (gradleProject.getParent() == null) {
      mBuild = new DaBuildExtension();
      mLicense = new DaLicenseExtension();
    } else {
      final DaRootExtension rootExt = getRootExtension();
      mBuild = new DaBuildExtension(rootExt.getBuild());
      mLicense = new DaLicenseExtension(rootExt.getLicense());
    }
  }

  // ---- Methods

  /**
   * @return the {@link DaOrganizationInfo} of this project
   */
  public abstract DaOrganizationInfo getOrganization ();

  /**
   * @return the {@link DaProjectInfo} of this project
   */
  public abstract DaProjectInfo getProject ();

  /**
   * @return the {@link DaBuildExtension} extension
   */
  public DaBuildExtension getBuild () {
    return mBuild;
  }

  /**
   * Configures the {@linkplain #getBuild() build extension}.
   *
   * @param closure the configuration closure
   */
  public void build (final Closure<?> closure) {
    ConfigureUtil.configure(closure, mBuild);
  }

  /**
   * @return the {@link DaLicenseExtension} of this project
   */
  public DaLicenseExtension getLicense () {
    return mLicense;
  }

  /**
   * Sets the {@linkplain #getLicense() license} to the predefined license with the specified {@ocde name}.
   *
   * @param name the name of the predefined license to set
   *
   * @throws IllegalArgumentException if no predefined license with the specified {@code name} could be found
   *
   * @see NamedExtensionLoader
   */
  public void setLicense (final CharSequence name) {
    getLoader().loadNamedExtension(DaLicenseExtension.class, mLicense, name);
  }

  /**
   * Configures the {@linkplain #getLicense() license extension}.
   *
   * @param closure the configuration closure
   */
  public void license (final Closure<?> closure) {
    ConfigureUtil.configure(closure, mLicense);
  }

  /**
   * Sets the {@linkplain #getLicense() license} to the predefined license with the specified {@ocde name} and
   * applies the specified configuration {@code closure} to the {@linkplain #getLicense() license}.
   *
   * @param name the name of the predefined license to set
   * @param closure the configuration closure
   *
   * @throws IllegalArgumentException if no predefined license with the specified {@code name} could be found
   *
   * @see NamedExtensionLoader
   */
  public void license (final CharSequence name, final Closure<?> closure) {
    setLicense(name);
    ConfigureUtil.configure(closure, mLicense);
  }

  /**
   * Generates formatted output describing the extension configuration.
   *
   * @param renderer the target renderer for the generated output (not {@code null})
   */
  public void render (final GradleReportRenderer renderer) {
    CheckArg.notNull("renderer", renderer);
    renderer.style(Style.Description).println("NOT YET IMPLEMENTED!").style(Style.Normal);
    // FIXME!
  }

  // -- Internal Hooks and Helper Methods

  abstract DaRootExtension getRootExtension ();

  Project getGradleProject () {
    return mGradleProject;
  }

  abstract NamedExtensionLoader getLoader ();
}
