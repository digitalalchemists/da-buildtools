/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.util;


import java.nio.file.attribute.PosixFilePermission;

import ch.dals.buildtools.lib.util.CheckArg;


/**
 * Represents the Posix permission classes {@link #OWNER}, {@link #GROUP} and {@link #OTHERS}.
 *
 * @author zisch
 *
 * @see PosixPermissionType
 * @see PosixFilePermission
 */
public enum PosixPermissionClass {
  /**
   * Owner category.
   */
  OWNER,

  /**
   * Group category.
   */
  GROUP,

  /**
   * Other users category.
   */
  OTHERS;

  public static PosixPermissionClass forFilePermission (final PosixFilePermission perm) {
    CheckArg.notNull("perm", perm);
    switch (perm) {
      case OWNER_READ:
      case OWNER_WRITE:
      case OWNER_EXECUTE:
        return OWNER;

      case GROUP_READ:
      case GROUP_WRITE:
      case GROUP_EXECUTE:
        return GROUP;

      case OTHERS_READ:
      case OTHERS_WRITE:
      case OTHERS_EXECUTE:
        return OTHERS;
    }
    throw new AssertionError("Unexpected PosixFilePermission: " + perm);
  }

  public PosixFilePermission toFilePermission (final PosixPermissionType perm) {
    CheckArg.notNull("perm", perm);
    return perm.toFilePermission(this);
  }
}
