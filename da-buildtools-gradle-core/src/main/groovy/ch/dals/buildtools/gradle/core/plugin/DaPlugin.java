/*
 * This file is part of the DA Secure Communication Framework (SCF) project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA SCF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA SCF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA SCF.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.plugin;


import java.util.LinkedHashMap;
import java.util.Map;

import org.gradle.api.Action;
import org.gradle.api.Plugin;
import org.gradle.api.Project;

import ch.dals.buildtools.gradle.core.util.GradleUtil;
import ch.dals.buildtools.lib.util.CheckArg;


/**
 * Abstract superclass for DA plugin implementations.
 * <p>
 * TODO [javadoc]: more doc
 *
 * @author zisch
 */
public abstract class DaPlugin implements Plugin<Project> {

  /**
   * Enum for the phase a {@link DaPlugin} can be in.
   *
   * @author zisch
   */
  public enum Phase {
    /**
     * The plugin will be in this phase when the {@link DaPlugin#validateProject()} method is executed. This is the
     * first phase in the {@link DaPlugin#apply(Project)} execution.
     */
    VALIDATION,

    /**
     * The plugin will be in this phase when the {@link DaPlugin#configureProject()} method is executed. This is the
     * second phase in the {@link DaPlugin#apply(Project)} execution.
     */
    CONFIGURATION,

    /**
     * The plugin will be in this phase when the {@link DaPlugin#prepareProject()} method is executed. This is the first
     * phase <em>after</em> the {@link DaPlugin#apply(Project)} execution. In this phase the build script of the plugins
     * Gradle project will have been evaluated.
     */
    PREPARATION,

    /**
     * The plugin will be in this phase when the {@link DaPlugin#postPrepareProject()} method is executed. This is the
     * last phase before the build tasks are executed. In this phase the build scripts of all Gradle project in the
     * build will have been evaluated and the {@link DaPlugin#prepareProject()} methods of all {@link DaPlugin}
     * instances taking part in the build have been executed.
     */
    POST_PREPARATION,

    /**
     * The plugin will be in this phase when the build tasks are being executed. This is (currently) the last phase of a
     * build.
     * <p>
     * (Note that in the future there might be a &ldquo;cleanup&rdquo; phase and a corresponding {@link DaPlugin} hook,
     * which is supposed to be executed after all build tasks have been executed.)
     */
    EXECUTION;

    private Phase prev () {
      return ordinal() == 0 ? null : values()[ordinal() - 1];
    }
  }

  private final boolean mAllowRoot;

  private final boolean mAllowSubprojects;

  private Phase mPhase = null;

  private Project mProject = null;

  /**
   * Constructor. Note that {@code allowRoot} and {@code allowSubprojects} cannot both be {@code false}.
   *
   * @param allowRoot if application of the plugin to the Gradle {@linkplain Project#getRootProject() root project}
   *          should be allowed by the default {@link #validateProject()} implementation
   * @param allowSubprojects if application of the plugin to the Gradle {@linkplain Project#getSubprojects()
   *          subprojects} should be allowed by the default {@link #validateProject()} implementation
   */
  protected DaPlugin (final boolean allowRoot, final boolean allowSubprojects) {
    if (!allowRoot && !allowSubprojects) {
      throw new IllegalArgumentException("allowRoot and allowSubprojects cannot both be false");
    }
    mAllowRoot = allowRoot;
    mAllowSubprojects = allowSubprojects;
  }

  /**
   * @return the Gradle project this plugin has been applied to or {@code null} if it has not (yet) been
   *         {@linkplain #apply(Project) applied} to a project
   */
  public Project getProject () {
    return mProject;
  }

  /**
   * @return the current phase of this plugin
   */
  public Phase getPhase () {
    return mPhase;
  }

  /**
   * @return {@code true} if {@linkplain #getProject() the Gradle project} this plugin has been
   *         {@linkplain #apply(Project) applied} to is the root project, {@code false} if it is a child project
   *
   * @throws IllegalStateException if this plugin has not (yet) been {@linkplain #apply(Project) applied} to project
   */
  public boolean hasRootProject () {
    if (mProject == null) {
      throw new IllegalStateException("This plugin (" + getClass().getName()
              + ") has not (yet) been applied to a project.");
    }
    return mProject.getParent() == null;
  }

  /**
   * Implementation of the plugin application method. Subclasses will normally not override this method but instead
   * implement any one of the hooks corresponding to certain build phases.
   *
   * @see #validateProject()
   * @see #configureProject()
   * @see #prepareProject()
   * @see #postPrepareProject()
   */
  @Override
  public void apply (final Project project) {
    CheckArg.notNull("project", project);
    if (mProject != null) {
      throw new IllegalStateException("Cannot apply this plugin (" + getClass().getName() + ") to project '"
              + project.getPath() + "' because it has already been applied to '" + mProject.getPath() + "'!");
    }
    project.getLogger().debug("Entering {}.apply(Project '{}') ...", getClass().getName(), project.getPath());
    mProject = project;
    setPhase(Phase.VALIDATION);
    validateProject();
    setPhase(Phase.CONFIGURATION);
    configureProject();
    getProject().afterEvaluate(new AfterEvaluateAction());
    setPhase(Phase.PREPARATION);
    project.getLogger().debug("Exiting {}.apply(Project '{}').", getClass().getName(), project.getPath());
  }

  /**
   * Validates if this plugin can be applied to the specified project. This method is called first in the
   * {@link #apply(Project)} phase.
   * <p>
   * The default implementation asserts that the specified project confirms to the {@code allowRoot} and
   * {@code allowSubprojects} flags defined in the {@linkplain #DaPlugin(boolean, boolean) constructor}.
   * <p>
   * Subclasses only need to override this method if they need to further restrict application of the plugin. An
   * overriding subclass should call the superclass implementation of the method unless the application of the plugin
   * represented by the subclass is allowed for the root projects as well as subprojects.
   * <p>
   * This hook is executed in {@link Phase#VALIDATION}; after execution of the method the plugin will change to
   * {@link Phase#CONFIGURATION}.
   *
   * @see #getPhase()
   */
  protected void validateProject () {
    if (!mAllowRoot && hasRootProject()) {
      throw new IllegalStateException("'" + getClass().getSimpleName() + "' can only be applied to subprojects!");
    }
    if (!mAllowSubprojects && !hasRootProject()) {
      throw new IllegalStateException("'" + getClass().getSimpleName() + "' can only be applied to the root project!");
    }
  }

  /**
   * Hook to configure the project. This method is called during the {@link #apply(Project)} phase (after the
   * {@link #validateProject()} hook).
   * <p>
   * The default implementation does nothing.
   * <p>
   * Use this hook to apply and configure other plugins, install extensions and generally configure the project.
   * Everything you would normally write directly into the Gradle build script should be placed into this method.
   * <p>
   * This hook is executed in {@link Phase#CONFIGURATION}; after execution of the method the plugin will change to
   * {@link Phase#PREPARATION}.
   *
   * @see #getPhase()
   */
  protected void configureProject () {
    // do nothing
  }

  /**
   * Hook to prepare the project in the {@link Project#afterEvaluate(Action)} phase. This method is called after the
   * build script for the Gradle project on which this plugin has been applied has been completely evaluated, but before
   * the {@link #postPrepareProject()} hook or any tasks are executed.
   * <p>
   * The default implementation does nothing.
   * <p>
   * Use this hook for actions you would normally place into an {@link Project#afterEvaluate(groovy.lang.Closure)
   * afterEvaluate} closure (on the Gradle project being configured) in a Gradle build script.
   * <p>
   * This hook is executed in {@link Phase#PREPARATION}; after execution of the method the plugin will change to
   * {@link Phase#POST_PREPARATION}.
   *
   * @see #getPhase()
   */
  protected void prepareProject () {
    // do nothing
  }

  /**
   * Hook for configurations for which all projects need to be prepared. This method is called after the
   * {@link #prepareProject()} hook has been executed for all {@link DaPlugin} instances in all Gradle projects taking
   * part in the build. At this point the build scripts of all Gradle projects have been completely evaluated and every
   * {@link DaPlugin} has {@linkplain #prepareProject() prepared} its own Gradle project.
   * <p>
   * The default implementatio does nothing.
   * <p>
   * Use this hook specifically for actions for which you would execute an
   * {@link Project#afterEvaluate(groovy.lang.Closure) afterEvaluate} closure on some child project and/or instead of
   * resorting to the {@link Project#evaluationDependsOn(String)} and {@link Project#evaluationDependsOnChildren()}
   * hacks.
   * <p>
   * This hook is executed in {@link Phase#POST_PREPARATION}; after execution of the method the plugin will change to
   * {@link Phase#EXECUTION}.
   *
   * @see #getPhase()
   */
  protected void postPrepareProject () {
    // do nothing
  }

  private final class AfterEvaluateAction implements Action<Project> {
    @Override
    public void execute (final Project project) {
      project.getLogger().debug("Entering {}.execute(Project '{}') ...", getClass().getName(), project.getPath());
      if (project != getProject()) {
        throw new IllegalStateException("Uups, something's wrong: DaPlugin.getProject() (" + getProject().getPath()
                + ") and the project argument (" + project.getPath()
                + ") of the Actin.execute(Project) method are different objects.");
      }
      prepareProject();
      setPhase(Phase.POST_PREPARATION);
      project.getLogger().debug("Exiting {}.execute(Project '{}').", getClass().getName(), project.getPath());
    }
  }

  private void setPhase (final Phase phase) {
    if (phase.prev() != mPhase) {
      throw new IllegalStateException("This plugin is in phase '" + mPhase + "' but to change to '" + phase
              + "' it should be in '" + phase.prev() + "'.");
    }
    getProject().getLogger().debug("{}: plugin {}", getClass().getName(), phase);
    mPhase = phase;
    GradleUtil.rootExtension(getProject(), Registry.class).registerPhase(this, phase);
  }

  /**
   * Internal class used by {@link DaPlugin}. This class is used by {@link DaPlugin} to keep track of all active plugin
   * instances and their {@linkplain DaPlugin#getPhase()}. Specifically this is necessary to know when the
   * {@link DaPlugin#prepareProject()} methods of all active instances have been executed so that the
   * {@link DaPlugin#postPrepareProject()} methods can be triggered.
   * <p>
   * This class is {@code public} because the Gradle framework needs to create instances. It should not be used
   * otherwise outside of the {@link DaPlugin} implementation.
   *
   * @author zisch
   */
  public static final class Registry {
    private final Map<DaPlugin, Phase> mPlugins = new LinkedHashMap<>();

    /**
     * Default constructor.
     * <p>
     * This constructor is {@code public} because the Gradle framework needs to create instances. It should not be used
     * otherwise outside of the {@link DaPlugin} implementation.
     */
    public Registry () {
      super();
    }

    private void registerPhase (final DaPlugin plugin, final Phase phase) {
      final Phase prev = mPlugins.put(plugin, phase);
      if (phase.prev() != prev) {
        throw new IllegalStateException("This plugin is registered to be in phase '" + prev + "' but to change to '"
                + phase + "' it should be in '" + phase.prev() + "'.");
      }
      final Phase[] pr = getPhaseRange();
      if (pr[0] == Phase.POST_PREPARATION && pr[1] == Phase.POST_PREPARATION) {
        triggerPostPrepared();
      } else if (pr[0].compareTo(Phase.POST_PREPARATION) < 0 && pr[1].compareTo(Phase.POST_PREPARATION) > 0) {
        throw new IllegalStateException("Internal error: Some plugin(s) reached a phase after POST_PREPARATION (max. "
                + pr[1] + ") while others are still in PREPRATION or before (min. " + pr[0] + ").");
      }
    }

    private void triggerPostPrepared () {
      for (final DaPlugin plugin : mPlugins.keySet()) {
        plugin.postPrepareProject();
        plugin.setPhase(Phase.EXECUTION);
      }
    }

    private Phase[] getPhaseRange () {
      Phase min = Phase.EXECUTION;
      Phase max = Phase.VALIDATION;
      for (final Phase p : mPlugins.values()) {
        if (p.ordinal() < min.ordinal()) {
          min = p;
        }
        if (p.ordinal() > max.ordinal()) {
          max = p;
        }
      }
      return new Phase[] { min, max };
    }
  }
}
