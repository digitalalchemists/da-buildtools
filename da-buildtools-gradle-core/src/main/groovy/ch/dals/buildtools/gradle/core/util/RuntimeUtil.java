/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.util;


import groovy.lang.GroovySystem;

import org.gradle.util.GradleVersion;


/**
 * TODO [javadoc]: type RuntimeUtil
 *
 * @author zisch
 */
public class RuntimeUtil {
  private static final SoftwareVersion JAVA_VERSION = determineJavaVersion();

  private static SoftwareVersion determineJavaVersion () {
    return SoftwareVersion.fromString(System.getProperty("java.version"));
  }

  public static SoftwareVersion javaVersion () {
    return JAVA_VERSION;
  }

  private static final SoftwareVersion GROOVY_VERSION = determineGroovyVersion();

  private static SoftwareVersion determineGroovyVersion () {
    return SoftwareVersion.fromString(GroovySystem.getVersion());
  }

  public static SoftwareVersion groovyVersion () {
    return GROOVY_VERSION;
  }

  private static final SoftwareVersion GRADLE_VERSION = determineGradleVersion();

  private static SoftwareVersion determineGradleVersion () {
    return SoftwareVersion.fromString(GradleVersion.current().toString());
  }

  public static SoftwareVersion gradleVersion () {
    return GRADLE_VERSION;
  }

  private RuntimeUtil () {
    throw new AssertionError("not allowed");
  }
}
