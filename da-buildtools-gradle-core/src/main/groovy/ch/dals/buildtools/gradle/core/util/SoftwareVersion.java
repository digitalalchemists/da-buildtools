/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.util;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ch.dals.buildtools.lib.util.CheckArg;
import ch.dals.buildtools.lib.util.Compare;
import ch.dals.buildtools.lib.util.HashUtil;


/**
 * TODO [javadoc]: type SoftwareVersion
 *
 * @author matthias
 */
public final class SoftwareVersion implements Serializable, Comparable<SoftwareVersion> {
  private static final long serialVersionUID = -7436068902713534032L;

  private static final Pattern sQualifierPattern = Pattern.compile("[a-zA-Z0-9_-]+");

  private static final Pattern sVersionPattern = Pattern.compile("([0-9]+)(?:\\.([0-9]+))*([-_.]" + sQualifierPattern
          + ")?");

  public static SoftwareVersion fromString (final String version) {
    final Matcher m = CheckArg.matcher(sVersionPattern, "version", version);
    int cnt = m.groupCount();
    String qualifier = null;
    if (m.group(cnt).startsWith("-")) {
      qualifier = m.group(cnt).substring(1);
      cnt--;
    }
    final List<Integer> components = new ArrayList<>(cnt);
    for (int i = 1; i <= cnt; i++) {
      components.add(new Integer(m.group(i)));
    }
    return new SoftwareVersion(components, qualifier);
  }

  private final List<Integer> mComponents;

  private final String mQualifier;

  private String mStringRep;

  /**
   * Constructor.
   *
   * @param components the components of this version
   * @param qualifier
   */
  public SoftwareVersion (final Collection<? extends Number> components, final String qualifier) {
    CheckArg.notNull("components", components);
    if (components.isEmpty()) {
      mComponents = Collections.singletonList(Integer.valueOf(0));
    } else {
      final Integer[] arr = new Integer[components.size()];
      int i = 0;
      for (final Number c : components) {
        if (c == null) {
          throw new IllegalArgumentException("components[" + i + "] must not be null");
        }
        final int v = c.intValue();
        if (v < 0) {
          throw new IllegalArgumentException("components[" + i + "] must not be negative: " + v);
        }
        arr[i++] = v;
      }
      mComponents = Collections.unmodifiableList(Arrays.asList(arr));
    }
    mQualifier = qualifier;
  }

  public int getMajor () {
    return getComponent(0);
  }

  public int getMinor () {
    return getComponent(1);
  }

  public int getMicro () {
    return getComponent(2);
  }

  /**
   * @return an unmodifiable list with all components
   */
  public List<Integer> getComponents () {
    return mComponents;
  }

  /**
   * Returns the component with the specified index, or 0 if the index is bigger than the number of defined components.
   *
   * @param idx the index of the component to get (not negative)
   *
   * @return the component with the specified index, or 0 if the index is bigger than the number of defined components
   */
  public int getComponent (final int idx) {
    CheckArg.notNegative("idx", idx);
    return idx < mComponents.size() ? mComponents.get(idx) : 0;
  }

  /**
   * @return the qualifier of this version (or {@code null} if none)
   */
  public String getQualifier () {
    return mQualifier;
  }

  /**
   * Two {@link SoftwareVersion} instances are first compared by their components (where missing components in one
   * instance are substituted with 0) and then by their qualifier.
   */
  @Override
  public int compareTo (final SoftwareVersion o) {
    int result = 0;
    final int compCnt = Math.max(mComponents.size(), o.mComponents.size());
    for (int i = 0; result == 0 && i < compCnt; i++) {
      result = Compare.compare(getComponent(i), o.getComponent(i));
    }
    if (result == 0) {
      if (mQualifier == null) {
        result = o.mQualifier == null ? 0 : -1;
      } else {
        result = o.mQualifier == null ? 1 : mQualifier.compareTo(o.mQualifier);
      }
    }
    return result;
  }

  /**
   * Equality is consistent with {@link #compareTo(SoftwareVersion)}.
   */
  @Override
  public boolean equals (final Object o) {
    if (o instanceof SoftwareVersion) {
      final SoftwareVersion sv = (SoftwareVersion) o;
      return compareTo(sv) == 0;
    } else {
      return false;
    }
  }

  /**
   * Hash code is consistent with {@link #compareTo(SoftwareVersion)}.
   */
  @Override
  public int hashCode () {
    return HashUtil.hash(mComponents.hashCode(), HashUtil.hash(mQualifier));
  }

  /**
   * TODO [javadoc]
   */
  @Override
  public String toString () {
    if (mStringRep == null) {
      final StringBuilder sb = new StringBuilder();
      for (final Integer c : mComponents) {
        if (sb.length() > 0) {
          sb.append('.');
        }
        sb.append(c);
      }
      if (mQualifier != null) {
        sb.append('-').append(mQualifier);
      }
    }
    return mStringRep;
  }
}
