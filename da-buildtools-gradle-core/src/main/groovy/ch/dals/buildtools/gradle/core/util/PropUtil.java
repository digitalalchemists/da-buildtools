/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.util;


import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.util.Locale;


/**
 * Some utility functions to process and parse property values.
 *
 * @author zisch
 */
public final class PropUtil {

  /**
   * Tries to convert the specified {@code value} to an enum element of the specified {@code enumType}.
   *
   * @param <E> the type of the enum element
   * @param enumType the type of the enum element
   * @param value the value to convert
   * @param defaultValue the default to value to return if {@code value} is {@code null} or empty
   *
   * @return the {@code value} converted to an instance of {@code enumType}
   */
  public static <E extends Enum<E>> E toEnum (final Class<E> enumType, final Object value, final E defaultValue) {
    if (value == null) {
      return defaultValue;
    } else if (enumType.isInstance(value)) {
      return enumType.cast(value);
    } else {
      final String s = normalize(true, value, null);
      if (s == null) {
        return defaultValue;
      } else {
        return Enum.valueOf(enumType, s.toUpperCase(Locale.US));
      }
    }
  }

  /**
   * Tries to convert the specified {@code value} to a {@link java.net.URI} object.
   *
   * @param label the label of the value (for error messages)
   * @param value the value to convert
   * @param defaultValue the default value to return if {@code value} is {@code null} or empty
   *
   * @return the {@code value} converted to a {@link java.net.URI}
   */
  public static URI toUri (final String label, final Object value, final URI defaultValue) {
    if (value == null) {
      return defaultValue;
    } else if (value instanceof URI) {
      return (URI) value;
    } else if (value instanceof URL) {
      try {
        return ((URL) value).toURI();
      } catch (final URISyntaxException exc) {
        throw new IllegalArgumentException("Invalid URI '" + value + "' (as java.net.URL) for " + label + ": "
                + exc.getMessage(), exc);
      }
    } else if (value instanceof File) {
      return ((File) value).toURI();
    } else if (value instanceof Path) {
      return ((Path) value).toUri();
    } else {
      final String s = normalize(true, value, null);
      if (s == null) {
        return null;
      } else {
        try {
          return new URI(s);
        } catch (final URISyntaxException exc) {
          throw new IllegalArgumentException("Invalid URI '" + s + "' (as string) for " + label + ": "
                  + exc.getMessage(), exc);
        }
      }
    }
  }

  /**
   * Normalizes the specified value. If the value is {@code null}, the specified {@code defaultValue} is returned.
   * Otherwise the {@linkplain Object#toString() string representation} of the value is {@linkplain String#trim()
   * trimmed} if {@code trim} is {@code true}. If the (possibly trimmed) string representation is
   * {@linkplain String#isEmpty() empty}, the {@code defaultValue} is returned, otherwise the trimmed string
   * representation is returned.
   *
   * @param trim if the string representation should be trimmed
   * @param value value to normalize
   * @param defaultValue the default value to return for an undefined or empty {@code value}
   *
   * @return the normalized string representation of the specified value
   */
  public static String normalize (final boolean trim, final Object value, final String defaultValue) {
    if (value == null) {
      return defaultValue;
    } else {
      final String s = trim ? value.toString().trim() : value.toString();
      return s.isEmpty() ? defaultValue : s;
    }
  }

  private PropUtil () {
    throw new AssertionError("not allowed");
  }
}
