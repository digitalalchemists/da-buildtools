/*
 * This file is part of the DA Secure Communication Framework (SCF) project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA SCF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA SCF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA SCF.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.plugin.base;


import org.gradle.api.Action;
import org.gradle.api.Task;
import org.gradle.api.plugins.ExtensionContainer;
import org.gradle.internal.service.ServiceRegistry;
import org.gradle.logging.StyledTextOutput;
import org.gradle.logging.StyledTextOutputFactory;

import ch.dals.buildtools.gradle.core.ext.AbstractDaExtension;
import ch.dals.buildtools.gradle.core.ext.DaRootExtension;
import ch.dals.buildtools.gradle.core.ext.DaSubExtension;
import ch.dals.buildtools.gradle.core.ide.eclipse.EclipseConfigurator;
import ch.dals.buildtools.gradle.core.plugin.DaPlugin;
import ch.dals.buildtools.gradle.core.util.GradleReportRenderer;


/**
 * Base plugin for configurations common to all DA projects.
 *
 * @author zisch
 */
public final class DaBasePlugin extends DaPlugin {

  /**
   * The name {@value} of the {@link DaRootExtension} and {@link DaSubExtension} objects.
   */
  public static final String DA_EXTENSION_NAME = "da";

  /**
   * Task name {@value} .
   */
  public static final String DA_EXTENSION_REPORT_TASK = "daExtensionReport";

  private Class<? extends AbstractDaExtension> mDaExtensionClass = null;

  private AbstractDaExtension mDaExtension = null;

  /**
   * Default constructor.
   */
  public DaBasePlugin () {
    super(true, true);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void configureProject () {
    createDaExtension();
    EclipseConfigurator.configure(getProject());

    final Task daPrintExtensionTask = getProject().task(DA_EXTENSION_REPORT_TASK);
    daPrintExtensionTask.setGroup("Help");
    daPrintExtensionTask.setDescription("Displays the configuration of the '" + DA_EXTENSION_NAME + "' extension.");
    daPrintExtensionTask.doLast(DisplayExtensionAction.INSTANCE);
  }

  private void createDaExtension () {
    if (mDaExtension != null) {
      throw new IllegalStateException("'" + DA_EXTENSION_NAME + "' extension already initialized");
    }
    if (hasRootProject()) {
      mDaExtensionClass = DaRootExtension.class;
    } else {
      mDaExtensionClass = DaSubExtension.class;
    }
    mDaExtension = getProject().getExtensions().create(DA_EXTENSION_NAME, mDaExtensionClass, getProject());
  }

  private static final class DisplayExtensionAction implements Action<Task> {
    private static final DisplayExtensionAction INSTANCE = new DisplayExtensionAction();

    @Override
    public void execute (final Task t) {
      final ExtensionContainer exts = t.getProject().getExtensions();
      final AbstractDaExtension daExtension = (AbstractDaExtension) exts.getByName(DA_EXTENSION_NAME);
      final ServiceRegistry sr = (ServiceRegistry) t.getProject().getProperties().get("services");
      final StyledTextOutputFactory of = sr.get(StyledTextOutputFactory.class);
      final StyledTextOutput output = of.create(DaBasePlugin.class);
      final GradleReportRenderer renderer = new GradleReportRenderer(output);
      daExtension.render(renderer);
    }
  }
}
