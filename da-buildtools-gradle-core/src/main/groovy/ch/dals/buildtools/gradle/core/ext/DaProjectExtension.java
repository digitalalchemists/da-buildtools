/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.ext;


import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;


/**
 * Configuration extension for the (root) project.
 *
 * @author zisch
 */
public class DaProjectExtension extends AbstractGroupNode implements DaProjectInfo {

  private int mInceptionYear;

  /**
   * Default constructor.
   */
  public DaProjectExtension () {
    super();
  }

  /**
   * @return the inception year of the project
   */
  @Override
  public int getInceptionYear () {
    return mInceptionYear;
  }

  /**
   * @param inceptionYear the inception year of the project
   */
  public void setInceptionYear (int inceptionYear) {
    mInceptionYear = inceptionYear;
  }

  /**
   * @return a formatted year range from the projects {@linkplain #getInceptionYear() inception year} to the current
   *         year
   */
  @Override
  public String getCopyrightYears () {
    final int currYear = currentYear();
    if (currYear == mInceptionYear) {
      return String.valueOf(mInceptionYear);
    } else if (currYear == mInceptionYear + 1) {
      return mInceptionYear + ", " + currYear;
    } else if (currYear > mInceptionYear) {
      return mInceptionYear + " - " + currYear;
    } else {
      throw new IllegalArgumentException("Inception year '" + mInceptionYear + "' cannot be after the current year '"
              + currYear + "'!");
    }
  }

  private static int currentYear () {
    return (new GregorianCalendar(Locale.US)).get(Calendar.YEAR);
  }
}
