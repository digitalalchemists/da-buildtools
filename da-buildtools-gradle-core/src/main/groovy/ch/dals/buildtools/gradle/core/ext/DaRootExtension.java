/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.ext;


import groovy.lang.Closure;

import org.gradle.api.Project;
import org.gradle.util.ConfigureUtil;

import ch.dals.buildtools.gradle.core.ext.data.NamedExtensionLoader;
import ch.dals.buildtools.gradle.core.plugin.base.DaBasePlugin;
import ch.dals.buildtools.lib.util.CheckArg;


/**
 * Extension object which is installed by the {@link DaBasePlugin} for {@linkplain Project#getRootProject() Gradle root
 * projects} with the name {@link DaBasePlugin#DA_EXTENSION_NAME "da"}.
 *
 * @author zisch
 */
public class DaRootExtension extends AbstractDaExtension {

  /**
   * Returns the {@link DaRootExtension} of the specified projects root project.
   *
   * @param project the Gradle project from which to obtain the extension instance
   *
   * @return the {@link DaRootExtension} of the specified projects root project
   *
   * @throw UnknownDomainObjectException if the {@link DaRootExtension} could not be found in the root project
   */
  public static DaRootExtension instance (final Project project) {
    CheckArg.notNull("project", project);
    return project.getRootProject().getExtensions().getByType(DaRootExtension.class);
  }

  private final NamedExtensionLoader mLoader;

  private final DaOrganizationExtension mOrganization = new DaOrganizationExtension();

  private final DaProjectExtension mProject = new DaProjectExtension();

  /**
   * Constructor.
   *
   * @param gradleProject the Gradle project in which this extension is installed
   */
  public DaRootExtension (final Project gradleProject) {
    super(gradleProject);
    mLoader = new NamedExtensionLoader(gradleProject);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public DaOrganizationExtension getOrganization () {
    return mOrganization;
  }

  /**
   * Sets the {@linkplain #getOrganization() organization} to the predefined organization with the specified {@ocde
   *  name}.
   *
   * @param name the name of the predefined organization to set
   *
   * @throws IllegalArgumentException if no {@link DaOrganizationExtension} of the specified {@code name} could be found
   *
   * @see NamedExtensionLoader
   */
  public void setOrganization (final CharSequence name) {
    mLoader.loadNamedExtension(DaOrganizationExtension.class, mOrganization, name);
  }

  /**
   * Applies the specified configuration {@code closure} to the {@linkplain #getOrganization() organization}.
   *
   * @param closure the configuration closure
   */
  public void organization (final Closure<?> closure) {
    ConfigureUtil.configure(closure, mOrganization);
  }

  /**
   * Sets the {@linkplain #getOrganization() organization} to the predefined organization with the specified {@ocde
   *  name} and applies the specified configuration {@code closure} to the {@linkplain #getOrganization()
   * organization}.
   *
   * @param name the name of the predefined organization to set
   * @param closure the configuration closure
   *
   * @throws IllegalArgumentException if no predefined organization with the specified {@code name} could be found
   *
   * @see NamedExtensionLoader
   */
  public void organization (final CharSequence name, final Closure<?> closure) {
    setOrganization(name);
    ConfigureUtil.configure(closure, mOrganization);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public DaProjectExtension getProject () {
    return mProject;
  }

  /**
   * Sets the {@linkplain #getProject() project} to the predefined project with the specified {@ocde name}.
   *
   * @param name the name of the predefined project to set
   *
   * @throws IllegalArgumentException if no predefined project with the specified {@code name} could be found
   *
   * @see NamedExtensionLoader
   */
  public void setProject (final CharSequence name) {
    mLoader.loadNamedExtension(DaProjectExtension.class, mProject, name);
  }

  /**
   * Applies the specified configuration {@code closure} to the {@linkplain #getProject() project}.
   *
   * @param closure the configuration closure
   */
  public void project (final Closure<?> closure) {
    ConfigureUtil.configure(closure, mProject);
  }

  /**
   * Sets the {@linkplain #getProject() project} to the predefined organization with the specified {@ocde name}
   * and applies the specified configuration {@code closure} to the {@linkplain #getProject() project}.
   *
   * @param name the name of the predefined organization to set
   * @param closure the configuration closure
   *
   * @throws IllegalArgumentException if no {@link DaProjectExtension} of the specified {@code name} could be found
   *
   * @see NamedExtensionLoader
   */
  public void project (final CharSequence name, final Closure<?> closure) {
    setProject(name);
    ConfigureUtil.configure(closure, mProject);
  }

  @Override
  DaRootExtension getRootExtension () {
    return this;
  }

  @Override
  NamedExtensionLoader getLoader () {
    return mLoader;
  }
}
