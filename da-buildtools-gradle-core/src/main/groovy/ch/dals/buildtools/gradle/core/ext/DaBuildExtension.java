/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.ext;


import groovy.lang.Closure;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import org.gradle.util.ConfigureUtil;

import ch.dals.buildtools.gradle.core.util.RuntimeUtil;
import ch.dals.buildtools.lib.util.CheckArg;


/**
 * TODO [javadoc]: type DaBuildExtension
 *
 * @author zisch
 */
public class DaBuildExtension {

  private final long mBuildTimeMillis;

  private final int mBuildTimeFormattedYear;

  private final String mBuildTimeFormattedDate;

  private final String mBuildTimeFormattedFull;

  private final String mBuildTimeStampDate;

  private final String mBuildTimeStampFull;

  private final DaLibraryExtension mJava;

  private final DaLibraryExtension mGroovy;

  private final DaLibraryExtension mGradle;

  /**
   * Default constructor.
   */
  public DaBuildExtension () {
    final long btm = System.currentTimeMillis();
    final Date btd = new Date(btm);
    final Calendar cal = new GregorianCalendar(Locale.US);
    cal.setTimeInMillis(btm);
    mBuildTimeMillis = btm;
    mBuildTimeFormattedYear = cal.get(Calendar.YEAR);
    mBuildTimeFormattedDate = timeFormatted(false, "yyyy-MM-dd", btd);
    mBuildTimeFormattedFull = timeFormatted(false, "yyyy-MM-dd HH:mm:ss Z", btd);
    mBuildTimeStampDate = timeFormatted(true, "yyyyMMdd", btd);
    mBuildTimeStampFull = timeFormatted(true, "yyMMddHHmmss", btd);
    mJava = new DaLibraryExtension(RuntimeUtil.javaVersion());
    mGroovy = new DaLibraryExtension(RuntimeUtil.groovyVersion());
    mGradle = new DaLibraryExtension(RuntimeUtil.gradleVersion());
  }

  /**
   * Copy constructor which creates a copy with the same attributes as the specified {@code defaultInstance}.
   *
   * @param defaultInstance the instance to copy
   */
  public DaBuildExtension (final DaBuildExtension defaultInstance) {
    CheckArg.notNull("defaultInstance", defaultInstance);
    mBuildTimeMillis = defaultInstance.mBuildTimeMillis;
    mBuildTimeFormattedYear = defaultInstance.mBuildTimeFormattedYear;
    mBuildTimeFormattedDate = defaultInstance.mBuildTimeFormattedDate;
    mBuildTimeFormattedFull = defaultInstance.mBuildTimeFormattedFull;
    mBuildTimeStampDate = defaultInstance.mBuildTimeStampDate;
    mBuildTimeStampFull = defaultInstance.mBuildTimeStampFull;
    mJava = new DaLibraryExtension(defaultInstance.mJava);
    mGroovy = new DaLibraryExtension(defaultInstance.mGroovy);
    mGradle = new DaLibraryExtension(defaultInstance.mGradle);
  }

  private static String timeFormatted (final boolean utc, String fmt, Date d) {
    final DateFormat df = new SimpleDateFormat(fmt, Locale.US);
    if (utc) {
      df.setTimeZone(TimeZone.getTimeZone("UTC"));
    }
    return df.format(d);
  }

  /**
   * @return the time in milliseconds (as in {@link System#currentTimeMillis()}) when the build has been started
   */
  public long getBuildTimeMillis () {
    return mBuildTimeMillis;
  }

  /**
   * @return the {@linkplain #getBuildTimeMillis() build time} formatted for {@linkplain TimeZone#getDefault() the hosts
   *         default timezone} with the {@linkplain SimpleDateFormat date format pattern} {@code "yyyy"}
   */
  public int getBuildTimeFormattedYear () {
    return mBuildTimeFormattedYear;
  }

  /**
   * @return the {@linkplain #getBuildTimeMillis() build time} formatted for {@linkplain TimeZone#getDefault() the hosts
   *         default timezone} with the {@linkplain SimpleDateFormat date format pattern} {@code "yyyy-MM-dd"}
   */
  public String getBuildTimeFormattedDate () {
    return mBuildTimeFormattedDate;
  }

  /**
   * @return the {@linkplain #getBuildTimeMillis() build time} formatted for {@linkplain TimeZone#getDefault() the hosts
   *         default timezone} with the {@linkplain SimpleDateFormat date format pattern}
   *         {@code "yyyy-MM-dd HH:mm:ss Z"}
   */
  public String getBuildTimeFormattedFull () {
    return mBuildTimeFormattedFull;
  }

  /**
   * @return the {@linkplain #getBuildTimeMillis() build time} formatted for UTC with the {@linkplain SimpleDateFormat
   *         date format pattern} {@code "yyyyMMdd"}
   */
  public String getBuildTimeStampDate () {
    return mBuildTimeStampDate;
  }

  /**
   * @return the {@linkplain #getBuildTimeMillis() build time} formatted for UTC with the {@linkplain SimpleDateFormat
   *         date format pattern} {@code "yyyyMMddHHmmss"}
   */
  public String getBuildTimeStampFull () {
    return mBuildTimeStampFull;
  }

  /**
   * @return the {@code java} configuration extension
   */
  public DaLibraryExtension getJava () {
    return mJava;
  }

  /**
   * Configures the {@code java} configuration extension.
   *
   * @param closure the configuration closure
   */
  public void java (final Closure<?> closure) {
    ConfigureUtil.configure(closure, mJava);
  }

  /**
   * @return the {@code groovy} configuration extension
   */
  public DaLibraryExtension getGroovy () {
    return mGroovy;
  }

  /**
   * Configures the {@code groovy} configuration extension.
   *
   * @param closure the configuration closure
   */
  public void groovy (final Closure<?> closure) {
    ConfigureUtil.configure(closure, mGroovy);
  }

  /**
   * @return the {@code gradle} configuration extension
   */
  public DaLibraryExtension getGradle () {
    return mGradle;
  }

  /**
   * Configures the {@code gradle} configuration extension.
   *
   * @param closure the configuration closure
   */
  public void gradle (final Closure<?> closure) {
    ConfigureUtil.configure(closure, mGradle);
  }
}
