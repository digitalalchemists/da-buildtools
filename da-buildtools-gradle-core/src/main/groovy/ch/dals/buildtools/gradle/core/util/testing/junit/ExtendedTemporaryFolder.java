/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.util.testing.junit;


import java.io.File;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.rules.ExternalResource;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;


/**
 * {@link TemporaryFolder} extension which optionally can be configured <em>not</em> to be deleted if the corresponding
 * test failed.
 *
 * @author zisch
 */
public class ExtendedTemporaryFolder extends TemporaryFolder {
  private volatile boolean mKeepFolderOnFailure = false;

  private volatile boolean mTestFailed = false;

  private final AtomicBoolean mInTest = new AtomicBoolean();

  /**
   * Default constructor.
   *
   * @see TemporaryFolder#TemporaryFolder()
   */
  public ExtendedTemporaryFolder () {
    super();
  }

  /**
   * Constructor.
   *
   * @param parentFolder parent folder where the temporary folder should be created
   *
   * @see TemporaryFolder#TemporaryFolder(File)
   */
  public ExtendedTemporaryFolder (final File parentFolder) {
    super(parentFolder);
  }

  /**
   * @return {@code true} if the temporary folder will be kept if the test fails, {@code false} (the default) if the
   *         folder should always be deleted after test execution
   */
  public boolean getKeepFolderOnFailure () {
    return mKeepFolderOnFailure;
  }

  /**
   * Sets the {@link #getKeepFolderOnFailure() keepFolderOnFailure} property/
   *
   * @param value the value to set
   *
   * @return this instance (for invocation chaining)
   */
  public ExtendedTemporaryFolder keepFolderOnFailure (final boolean value) {
    mKeepFolderOnFailure = value;
    return this;
  }

  /**
   * Injects a catch clause to handle test failures before the {@link ExternalResource} {@code after()} hook is called.
   */
  @Override
  public Statement apply (final Statement base, final Description description) {
    return super.apply(statement(base), description);
  }

  private Statement statement (final Statement base) {
    return new Statement() {
      @Override
      public void evaluate () throws Throwable {
        try {
          base.evaluate();
        } catch (final Throwable t) {
          try {
            onFailure(t);
          } catch (final Throwable tt) {
            t.addSuppressed(tt);
          }
          throw t;
        }
      }
    };
  }

  /**
   * Prevent reentrant execution and reset the {@code testFailed} flag upon test begin.
   */
  @Override
  protected void before () throws Throwable {
    if (!mInTest.compareAndSet(false, true)) {
      throw new IllegalStateException("Already executing a test!");
    }
    mTestFailed = false;
    super.before();
  }

  /**
   * Reset reentrant state.
   */
  @Override
  protected void after () {
    final boolean keep = mKeepFolderOnFailure && mTestFailed;
    if (!keep) {
      super.after();
    }
    mInTest.set(false);
  }

  /**
   * Implements the failure handling.
   *
   * @param t the test failure
   */
  protected void onFailure (final Throwable t) {
    if (!mInTest.get()) {
      throw new IllegalStateException("Not currently executing a test!");
    }
    mTestFailed = true;
  }
}
