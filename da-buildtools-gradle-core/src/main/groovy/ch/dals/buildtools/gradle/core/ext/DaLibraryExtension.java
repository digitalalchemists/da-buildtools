/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.ext;


import ch.dals.buildtools.gradle.core.util.SoftwareVersion;
import ch.dals.buildtools.lib.util.CheckArg;


/**
 * Extension to configure versions and other properties of libraries used or targetted by the build.
 * <p>
 * TODO: more doc!
 *
 * @author matthias
 */
public final class DaLibraryExtension {

  private final SoftwareVersion mBuildVersion;

  private SoftwareVersion mSourceVersion;

  private SoftwareVersion mTargetVersion;

  /**
   * Copy constructor which creates a copy with the same attributes as the specified {@code defaultInstance}.
   *
   * @param defaultInstance the instance to copy
   */
  public DaLibraryExtension (final DaLibraryExtension defaultInstance) {
    this(CheckArg.notNull("defaultInstance", defaultInstance).mBuildVersion, defaultInstance.mSourceVersion,
            defaultInstance.mTargetVersion);
  }

  /**
   * Constructor which initializes all versions to the specified {@code version}.
   *
   * @param version the {@linkplain #getBuildVersion() build}, {@linkplain #getSourceVersion() source} and
   *          {@link #getTargetVersion() target} version for this language (not {@code null})
   */
  public DaLibraryExtension (final SoftwareVersion version) {
    this(CheckArg.notNull("version", version), null, null);
  }

  /**
   * Constructor which initializes the {@linkplain #getBuildVersion() build version} to {@code buildVersion} and the
   * {@linkplain #getSourceVersion() source} and {@linkplain #getTargetVersion() target} versions to
   * {@code sourceVersion}.
   *
   * @param buildVersion the {@linkplain #getBuildVersion() build version} to use (not {@code null})
   * @param sourceVersion the {@linkplain #getSourceVersion() source} and {@linkplain #getTargetVersion() target}
   *          versions (not {@code null})
   */
  public DaLibraryExtension (final SoftwareVersion buildVersion, final SoftwareVersion sourceVersion) {
    this(buildVersion, sourceVersion, null);
  }

  /**
   * Constructor.
   *
   * @param buildVersion the {@linkplain #getBuildVersion() build version} to use (not {@code null})
   * @param sourceVersion the {@linkplain #getSourceVersion() source version} (not {@code null})
   * @param targetVersion the {@linkplain #getTargetVersion() target version} (not {@code null})
   */
  public DaLibraryExtension (final SoftwareVersion buildVersion, final SoftwareVersion sourceVersion,
          final SoftwareVersion targetVersion) {
    mBuildVersion = CheckArg.notNull("buildVersion", buildVersion);
    mSourceVersion = sourceVersion;
    mTargetVersion = targetVersion;
  }

  /**
   * @return the version active in the currently running build script (read only)
   */
  public SoftwareVersion getBuildVersion () {
    return mBuildVersion;
  }

  /**
   * @return the (minimal) target version for which the sources are written
   */
  public SoftwareVersion getSourceVersion () {
    return mSourceVersion == null ? mBuildVersion : mSourceVersion;
  }

  /**
   * Sets the source version.
   *
   * @param sourceVersion the (minimal) target version for which the sources are written; may be {@code null} in which
   *          case the target version will be set equal to the {@linkplain #getBuildVersion() build version}
   *
   * @throws IllegalArgumentException if the specified {@code CharSequence} does not represent a valid version according
   *           to {@link SoftwareVersion#fromString(String)}
   */
  public void setSourceVersion (final CharSequence sourceVersion) {
    if (sourceVersion == null || sourceVersion.length() == 0) {
      mSourceVersion = null;
    } else {
      mSourceVersion = SoftwareVersion.fromString(sourceVersion.toString());
    }
  }

  /**
   * Sets the source version.
   *
   * @param sourceVersion the (minimal) target version for which the sources are written; may be {@code null} in which
   *          case the target version will be set equal to the {@linkplain #getBuildVersion() build version}
   */
  public void setSourceVersion (final SoftwareVersion sourceVersion) {
    mSourceVersion = sourceVersion;
  }

  /**
   * @return the (minimal) target version on which the product of the build should run
   */
  public SoftwareVersion getTargetVersion () {
    return mTargetVersion == null ? getSourceVersion() : mTargetVersion;
  }

  /**
   * Sets the target version.
   *
   * @param targetVersion the (minimal) target version on which the product of the build should run; may be {@code null}
   *          in which case the target version will be set equal to the {@linkplain #getSourceVersion() source version}
   *
   * @throws IllegalArgumentException if the specified {@code CharSequence} does not represent a valid version according
   *           to {@link SoftwareVersion#fromString(String)}
   */
  public void setTargetVersion (final CharSequence targetVersion) {
    if (targetVersion == null || targetVersion.length() == 0) {
      mTargetVersion = null;
    } else {
      mTargetVersion = SoftwareVersion.fromString(targetVersion.toString());
    }
  }

  /**
   * Sets the target version.
   *
   * @param targetVersion the (minimal) target version on which the product of the build should run; may be {@code null}
   *          in which case the target version will be set equal to the {@linkplain #getSourceVersion() source version}
   */
  public void setTargetVersion (final SoftwareVersion targetVersion) {
    mTargetVersion = targetVersion;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString () {
    return mBuildVersion + "/" + getSourceVersion() + "/" + getTargetVersion();
  }
}
