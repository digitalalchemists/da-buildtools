/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.ext;


import java.net.URI;


/**
 * TODO [javadoc]: type DaProjectInfo
 *
 * @author zisch
 */
public interface AbstractGroupNodeInfo extends AbstractNamedNodeInfo {

  /**
   * @return the package name of this node (a valid Java name, never {@code null}), for example
   *         {@code "ch.dals.scf.core"}
   */
  public String getPackageName ();

  /**
   * @return an optional human readable acronym (or {@code null} if unspecified), for example {@code "SCF"}
   */
  public String getAcronym ();

  /**
   * @return the value <code>"${label} (${acronym})"</code> if the {@link #getAcronym() acronym} is not {@code null},
   *         otherwise just the {@link #getLabel() label} itself (read only)
   */
  public String getLabelWithAcronym ();

  /**
   * @return the {@link #getAcronym() acronym} if it is not {@code null}, otherwise the {@link #getLabel() label} (read
   *         only)
   */
  public String getAcronymOrLabel ();

  /**
   * @return an optional human readable description (or {@code null} if unspecified)
   */
  public String getDescription ();

  /**
   * @return an optional URL of the nodes WWW homepage (or {@code null} if unspecified)
   */
  public URI getHomeUrl ();

}
