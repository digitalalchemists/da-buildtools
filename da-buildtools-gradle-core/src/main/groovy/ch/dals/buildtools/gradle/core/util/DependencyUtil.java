/*
 * This file is part of the DA Buildtools project.
 *
 * Copyright 2014 Digital Alchemists GmbH, Switzerland
 * http://www.digital-alchemists.ch/
 * mailto:info@digital-alchemists.ch
 *
 * DA Buildtools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 *
 * DA Buildtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DA Buildtools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.dals.buildtools.gradle.core.util;


import java.io.File;
import java.util.Set;
import java.util.regex.Pattern;

import org.gradle.api.Project;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.artifacts.ConfigurationContainer;
import org.gradle.api.artifacts.Dependency;
import org.gradle.api.artifacts.dsl.DependencyHandler;

import ch.dals.buildtools.lib.util.CheckArg;


/**
 * TODO [javadoc]: type DependencyUtil
 *
 * @author zisch
 */
public final class DependencyUtil {
  private static final Pattern sNameElementPattern = Pattern.compile("[0-9a-zA-Z_$.-]+");

  public static File resolveArtifact (final Project resolvingProject, final String group, final String name,
          final String version) {
    return resolveArtifact(resolvingProject, group, name, version, null, null);
  }

  public static File resolveSourcesArtifact (final Project resolvingProject, final String group, final String name,
          final String version) {
    return resolveArtifact(resolvingProject, group, name, version, "sources", null);
  }

  public static File resolveJavadocArtifact (final Project resolvingProject, final String group, final String name,
          final String version) {
    return resolveArtifact(resolvingProject, group, name, version, "javadoc", null);
  }

  public static File resolveArtifact (final Project resolvingProject, final String group, final String name,
          final String version, final String classifier) {
    return resolveArtifact(resolvingProject, group, name, version, classifier, null);
  }

  public static File resolveArtifact (final Project resolvingProject, final String group, final String name,
          final String version, final String classifier, final String ext) {
    CheckArg.notNull("resolvingProject", resolvingProject);
    return resolveArtifact(resolvingProject.getConfigurations(), resolvingProject.getDependencies(), group, name,
            version, classifier, ext);
  }

  public static File resolveArtifact (final ConfigurationContainer configContainer, final DependencyHandler depHandler,
          final String group, final String name, final String version, final String classifier, final String ext) {
    CheckArg.matches(sNameElementPattern, "group", group);
    CheckArg.matches(sNameElementPattern, "name", name);
    CheckArg.matches(sNameElementPattern, "version", version);
    if (classifier != null) {
      CheckArg.matches(sNameElementPattern, "classifier", classifier);
    }
    if (ext != null) {
      CheckArg.matches(sNameElementPattern, "ext", ext);
    }

    final StringBuilder sb = new StringBuilder(128);
    sb.append(group).append(':').append(name).append(':').append(version);
    if (classifier != null) {
      sb.append(':').append(classifier);
    }
    if (ext == null) {
      sb.append("@jar");
    } else {
      sb.append('@').append(ext);
    }
    final Dependency dep = depHandler.create(sb.toString());

    final Configuration config = configContainer.detachedConfiguration(dep);
    config.setTransitive(false);
    config.setVisible(false);
    final Set<File> resolvedFiles = config.resolve();
    if (resolvedFiles.isEmpty()) {
      // FIXME: error/warning!?
      return null;
    } else if (resolvedFiles.size() == 1) {
      return resolvedFiles.iterator().next();
    } else {
      // FIXME: error/warning!?
      return null;
    }
  }

  private DependencyUtil () {
    throw new AssertionError("not allowed");
  }
}
