This file is part of the ${da.organization.acronymOrLabel} ${da.info.project.labelWithAcronym}.
Copyright (C) ${da.project.copyrightYears} ${da.organization.label}

${da.info.project.acronymOrLabel} is free software: you can redistribute it and/or modify it under the terms of the GNU Library General Public License version 2 as published by the Free Software Foundation.

${da.info.project.acronymOrLabel} is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ${da.info.project.acronymOrLabel}. If not, see <http://www.gnu.org/licenses/>.
